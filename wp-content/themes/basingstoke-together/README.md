# Base WordPress Build

This is a base static WP build.

## Prerequisites

1. Node.js
2. Ruby
3. Sass
4. Gulp
5. Bower

## Node Installation

* Visit [Node.js](http://nodejs.org/)
* Download the latest version of Node

## Sass Installation

* sudo gem install sass

## Gulp Installation

* npm install -g gulp (globally)
* cd to your project root
* npm install
* run gulp by typing 'gulp'
* The style.css file will be missing until you save a .scss file.

## Bower Installation

* npm install -g bower (globally)
* Keep in mind that modified bower installations will need to be manually copied to the correct location and modified again if applicable

### Bower Packages

##### Update all Bower packages
* bower update

##### Update one particular package
* bower update 'package name'

## BEM

We use [BEM](http://bem.info/) for all new projects.

### Quick Intro

.block {}
.block__element {}
.block--modifier {}

* block The block is the root or container.
* block__element is a piece of the block. The element helps form the block.
* block--modifier a modified version.

#### Quick Exmple

.site-nav {} /* Block */
.site-nav__sub {} /* Element */
.site-nav--full {} /* Modifier */

## Scss Styleguide

Before making ANY changes to Scss files, please read the styleguide first.

## Plugin update for ACF disabled

So, you may be wondering why the acf plugin is currently unable to update.

Basically, old ACF doesn't play well with google maps API; it fails to pass in an API key. so we had to update
wp-content/plugins/advanced-custom-fields/core/fields/google-map.php
line 293 to include 'key' => 'AIzaSyB4L4QybNh3Di47HgGU9hMuV9Wccd182Bc'

If you NEED to update ACF, then comment out the line in functions.php, update the plugin, then add in the key above and then please for the love of glob uncomment the line in function.php again!
