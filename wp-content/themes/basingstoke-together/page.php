<?php
/**
 * The template for displaying generic pages.
 */

class GenericView extends BasingstokeBaseView {

	/*
	 * Override this method in an extended class to add additional custom data to the context, or modify the context itself.
	 */ 
	public function post_process(){
		parent::post_process();

		/**
		 * Get files field data, so that we can then append size information too.
		 */
		$filesRows = get_field('downloads', $this->context['page']->ID);

		if ( is_array($filesRows) ) {

			//loop over each item in a group
			foreach ($filesRows as $rowIdx => $filesRow) {
				
				// work out file extension
				$ext = substr(strrchr($filesRow['file']['url'],'.'),1);
							
				$file = get_attached_file( $filesRow['file']['id'] );

				// work out file size!
				$size = (file_exists($file)) ? filesize( $file ) : 0;

				//set our keys for access in twig!
				$filesRows[$rowIdx]['file']['extension'] = $ext;
				$filesRows[$rowIdx]['file']['size'] = $size;
				$filesRows[$rowIdx]['file']['size_formatted'] = size_format($size, 1);
			}

			/**
			 * Remove the extra 'layer' in the array by using array_map.
			 */
			$this->context['downloads'] = array_map( function( $a ){ return $a['file']; }, $filesRows );

		}


 	}

}

$view = new GenericView( array( 'generic.twig' ) );
$view->render();