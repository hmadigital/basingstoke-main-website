<?php

class NewsDetailView extends BasingstokeBaseView {

	public function post_process(){
		
		parent::post_process();

		/*
		 * Always ensure 'Send a story' banner and latest tweet are shown.
		 */
		$this->context['rightcol']['story'] = true;
		$this->context['rightcol']['latest_tweet'] = true;
	}

}

$view = new NewsDetailView( array( 'news-detail.twig' ) );
$view->render();