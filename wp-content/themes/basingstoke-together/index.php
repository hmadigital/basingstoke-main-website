<?php
/**
 * The template for displaying anything not explicitly defined!.
 */

$view = new TimberView( array('generic-list.twig') );
$view->render();