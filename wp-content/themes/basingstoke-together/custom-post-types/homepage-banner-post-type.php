<?php

/**
 * Homepage Banner - Custom Post Type.
 */
class HomepageBannerPostType extends PostType
{
	/**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {

		/**
	     * Add action hook to register banner sections taxonomy.
		 */
		add_action( 'init', array( $this, 'register_banner_section_taxonomy' ) );

		/**
	     * Add action hook to hide the standard taxonomy UI
		 */
		add_action( 'do_meta_boxes', array( $this, 'hide_meta_boxes' ) );		

		/**
	     * Add save_post action hook to automagically assign 'Business Section' taxonomy on save.
		 */
		add_action('save_post', array( $this, 'set_business_section_taxonomy_term' ) );

	}	

	/**
     * Everytime we save a banner, update the 'Business Section' taxonomy automatically.
	 */
	public function set_business_section_taxonomy_term( $post_id ) {

		// If this isn't a 'business' post type, don't update it.
	    if ( !array_key_exists('post_type', $_POST) || $_POST['post_type'] != 'homepage_banner' ) {
	        return;
	    }
	    
	    $sectionFieldKey = 'field_53c67e8a78ca8';

	    if ( array_key_exists('fields', $_POST) && array_key_exists($sectionFieldKey, $_POST['fields']) ) {
	    	
    		$section = strtolower( sanitize_text_field( $_POST['fields'][$sectionFieldKey] ) );

	    	// check if aterm already exists for this letter
	    	$existingTerm = get_term_by( 'slug', $section, 'banner_section' );

	    	$termID = 0;

	    	if ($existingTerm !== false) {
    			$termID = (isset( $existingTerm->term_id )) ? intval($existingTerm->term_id) : 0;
	    	}

	    	if ($termID > 0) {
    			wp_set_object_terms( $post_id, $termID, 'banner_section');
    		}	    	
	    }
	}

	/**
     * Register our custom event focus taxonomy.
	 */
	public function register_banner_section_taxonomy() {

		$labels = array(
			'name'              => _x( 'Banner Section', 'taxonomy general name' ),
			'singular_name'     => _x( 'Banner Section', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Banner Sections' ),
			'all_items'         => __( 'All Banner Sections' ),
			'parent_item'       => __( 'Parent Banner Section' ),
			'parent_item_colon' => __( 'Parent Banner Section:' ),
			'edit_item'         => __( 'Edit Banner Section' ),
			'update_item'       => __( 'Update Banner Section' ),
			'add_new_item'      => __( 'Add New Banner Section' ),
			'new_item_name'     => __( 'New Banner Section Name' ),
			'menu_name'         => __( 'Banner Sections' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'rewrite'           => array( 'slug' => 'banner-section' ),
		);
	
		register_taxonomy( 'banner_section', 'homepage_banner', $args );
	}

	/**
     * Hide meta boxes for taxonomy.
	 */
	public function hide_meta_boxes() {
		remove_meta_box( 'banner_sectiondiv', 'homepage_banner', 'side' );
	}
	
}

new HomepageBannerPostType( array(
	'menu_position' => 9, 
	'publicly_queryable'  => true,
	'rewrite' => false,
), 'Homepage Banner' );

class HomepageBannerPost extends TimberPost {
	private $_link_url;

	public function link(){

		if (!$this->_link_url){

			if ($this->call_to_action_link_type == 'internal') {
				$this->_link_url = get_permalink( $this->call_to_action_internal_link );
			} else {
				$this->_link_url = 'http://' . $this->call_to_action_external_link;
			}
			
		}

		return $this->_link_url;
	}

}