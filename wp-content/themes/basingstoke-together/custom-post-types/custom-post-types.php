<?php
	
/**
 * Include PostType class so that we can use  or extend it below.
 */
require( get_template_directory() . '/core/class-post-type.php' );

/**
 * Businesses - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/business-post-type.php' );

/**
 * Contacts - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/contact-post-type.php' );

/**
 * Download Groups - Custom Post Type.
 */
//require( get_template_directory() . '/custom-post-types/download-group-post-type.php' );

/**
 * Events - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/event-post-type.php' );

/**
 * Homepage Banners - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/homepage-banner-post-type.php' );

/**
 * Initiative - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/initiative-post-type.php' );

/**
 * * Job Vacancy - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/job-vacancy-post-type.php' );

/**
 * News Articles - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/news-article-post-type.php' );

/**
 * Promotion - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/promotion-post-type.php' );

/**
 * Gallery Detail - Custom Post Type.
 */
//require( get_template_directory() . '/custom-post-types/gallery-post-type.php' );

/**
 * Gallery Detail - Custom Post Type.
 */
require( get_template_directory() . '/custom-post-types/form-submission-post-type.php' );