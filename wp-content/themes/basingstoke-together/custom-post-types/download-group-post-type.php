<?php

/**
 * Download Group - Custom Post Type.
 */
class DownloadGroupPostType extends PostType
{
	/**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {

		
	}
}

$contact = new DownloadGroupPostType( array(
	'menu_position' => 7,
	'rewrite' => false,
), 'Download Group' );