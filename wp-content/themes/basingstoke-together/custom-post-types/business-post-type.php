<?php

/**
 * News Article - Custom Post Type.
 */
class BusinessPostType extends PostType
{
	/**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {

		/**
	     * Add action hook to register 'A-Z' letter taxonomy.
		 */
		add_action( 'init', array( $this, 'register_letter_taxonomy' ) );

		/**
	     * Add action hook to register 'Business Category' letter taxonomy.
		 */
		add_action( 'init', array( $this, 'register_category_taxonomy' ) );

		/**
	     * Add action hook to hide the standard taxonomy UI
		 */
		add_action( 'do_meta_boxes', array( $this, 'hide_meta_boxes' ) );

		/**
	     * Add save_post action hook to automagically assign 'A-Z' letter taxonomy on save,
	     * and send data to remote API for rewards section
		 */
		add_action('save_post', array( $this, 'post_save_hook' ) );

		/**
	     * Add action to update custom rewrite rules.
		 */
		add_filter('rewrite_rules_array', array( $this, 'add_rewrite_rules' ) );

		/**
	     * Custom query vars hook (to allow custom newsyear variable).
		 */
		add_filter( 'query_vars', array( $this, 'add_query_vars') );

		/**
	     * Ensure that when viewing trying to access the post types default 'list' url, we redirect to our proper page.
		 */
		Timber::add_route('businesses/', function($params){
			$url = site_url('in-basingstoke/find-a-business/');
		    wp_redirect( $url, 301 );
			exit;
		});

		/**
	     * Ensure that when viewing a 'single' post using the post types rewrite slug that we redirect to our 'proper' url structure.
		 */
		Timber::add_route('businesses/:name', function($params){
			$url = site_url('in-basingstoke/find-a-business/' . $params['name'] . '/');
		    wp_redirect( $url, 301 );
			exit;
		});
	}

	/**
     * Hide meta boxes for taxonomy.
	 */
	public function hide_meta_boxes() {
		remove_meta_box( 'tagsdiv-business_letter', 'business', 'side' );
		remove_meta_box( 'business_categorydiv', 'business', 'side' );
	}

	/**
     * Return an array of redirect rules used to match URL requests.
	 *
	 * @param array $rules
	 * @return array
	 */
	public function add_rewrite_rules($rules) {
		$newRules = array(
			// business list by letter
			'in-basingstoke/find-a-business/by-letter/([^/]+)/?$' => 'index.php?pagename=in-basingstoke/find-a-business&by_letter=$matches[1]',

			// business by year and page
			'in-basingstoke/find-a-business/by-letter/([^/]+)/page/([0-9]{1,})/?$' => 'index.php?pagename=in-basingstoke/find-a-business&by_letter=$matches[1]&paged=$matches[2]',

			// business by page (all years)
			'in-basingstoke/find-a-business/page/([0-9]{1,})/?$' => 'index.php?pagename=in-basingstoke/find-a-business&by_letter=all&paged=$matches[1]',

			// specific article detail by slug
			'in-basingstoke/find-a-business/([^/]+)/?$' => 'index.php?business=$matches[1]',
		);

		$rules = $newRules + $rules;
		return $rules;
	}

	/**
     * Return an array of query vars allowed by WordPress.
	 *
	 * @param array $vars
	 * @return array
	 */
	function add_query_vars($vars) {
		array_push( $vars, 'by_letter' );
		return $vars;
	}

	/**
     * Register our custom letter taxonomy.
	 */
	public function register_letter_taxonomy() {

		$labels = array(
			'name'              => _x( 'Business Letters', 'taxonomy general name' ),
			'singular_name'     => _x( 'Business Letter', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Business Letters' ),
			'all_items'         => __( 'All Business Letters' ),
			'parent_item'       => __( 'Parent Business Letter' ),
			'parent_item_colon' => __( 'Parent Business Letter:' ),
			'edit_item'         => __( 'Edit Business Letter' ),
			'update_item'       => __( 'Update Business Letter' ),
			'add_new_item'      => __( 'Add New Business Letter' ),
			'new_item_name'     => __( 'New Business Letter' ),
			'menu_name'         => __( 'Business Letters' ),
		);

		$args = array(
			'hierarchical'      => false,
			'labels'            => $labels,
			'show_ui'           => false,
			'show_admin_column' => false,
			'rewrite'           => false,
		);

		register_taxonomy( 'business_letter', 'business', $args );
	}

	/**
     * Register our custom category taxonomy.
	 */
	public function register_category_taxonomy() {

		$labels = array(
			'name'              => _x( 'Business Categories', 'taxonomy general name' ),
			'singular_name'     => _x( 'Business Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Business Categories' ),
			'all_items'         => __( 'All Business Categories' ),
			'parent_item'       => __( 'Parent Business Category' ),
			'parent_item_colon' => __( 'Parent Business Category:' ),
			'edit_item'         => __( 'Edit Business Category' ),
			'update_item'       => __( 'Update Business Category' ),
			'add_new_item'      => __( 'Add New Business Category' ),
			'new_item_name'     => __( 'New Business Category' ),
			'menu_name'         => __( 'Business Categories' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'rewrite'           => false,
		);

		register_taxonomy( 'business_category', 'business', $args );
	}


	/**
	 * update the 'A-Z' taxonomy
	 */

	public function set_letter_taxonomy_term( $post_id ) {

		$firstLetter = strtolower( $_POST['post_title'][0] );

		if ( is_numeric( $firstLetter ) ) {
			$firstLetter = '0-9';
		}

		// check if aterm already exists for this letter
		$existingTerm = get_term_by( 'slug', $firstLetter, 'business_letter' );

		$termID = 0;

		if ($existingTerm === false) {
			$newTerm = wp_insert_term( strtoupper( $firstLetter ), 'business_letter' );
			$termID = intval($newTerm['term_id']);
		} else {
			$termID = (isset( $existingTerm->term_id )) ? intval($existingTerm->term_id) : 0;
		}

		if ($termID > 0) {
			wp_set_object_terms( $post_id, $termID, 'business_letter');
		}

	}

	/**
     * Everytime we save a business
	 */
	public function post_save_hook( $post_id ) {

		// If this isn't a 'business' post type, don't update it.
	    if ( !array_key_exists('post_type', $_POST) || $_POST['post_type'] != 'business' ) {
	        return;
		}

		if ( array_key_exists('post_title', $_POST) && strlen($_POST['post_title']) > 0 ) {

			$this->set_letter_taxonomy_term( $post_id );

			//$this->save_to_remote( $post_id );

	    }

	}

}

class BusinessPost extends TimberPost {
	private $_link;
	private $_logoImg;

	public function description($withBRs = true){
		return get_field('description', $this->ID, $withBRs);
	}

	public function link($baseUrl = '/'){

		if (!$this->_link){
			$this->_link = $baseUrl . $this->post_name . '/';
		}

		return $this->_link;
	}

	public function logo(){

		if (!$this->_logoImg){
			$this->_logoImg = new TimberImage($this->logo);
		}

		return $this->_logoImg;
	}
}

new BusinessPostType( array(
	'menu_position' => 5,
	'publicly_queryable'  => true,
	'rewrite' => array(
		'slug' => 'businesses',
		'with_front' => false,
	),
), 'Business', 'Businesses' );