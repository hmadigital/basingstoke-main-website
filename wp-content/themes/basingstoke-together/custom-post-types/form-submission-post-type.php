<?php

/**
 * Form Submission - Custom Post Type.
 */
class FormSubmissionPostType extends PostType
{
    private $page_data = [];

    private $forms = [];

    /**
     * Used by classes that extend this one to add extra hooks etc.
     */
    public function additional_actions() {

        // create admin menu
        add_action('admin_init', [ $this, 'admin_init' ] );
        add_action('admin_menu', [ $this, 'add_form_submission_menu' ] );

    }

    public function add_form_submission_menu() {
        add_menu_page( __( 'Form Submissions', 'form_submissions' ), __( 'Form Submissions', 'form_submissions' ), 'manage_options', 'submissions_list_page', [ $this, 'submissions_list_page' ], 'div', '2');

    }

    public function submissions_list_page() {

        if ( !empty( $this->page_data['columns'] ) && !empty( $this->page_data['rows'] ) ) {

            $this->page_data['form'] = $_GET['form'];
            $this->page_data['form_name'] = $this->forms[ $_GET['form'] ]['name'];

            Timber::render( 'admin/form-submissions-detail.twig', $this->page_data );

            return;

        }

        $this->page_data = array_merge( $this->page_data, [ 'lists' => $this->forms ] );

        Timber::render( 'admin/form-submissions-list.twig', $this->page_data );

    }

    public function admin_init() {

        $this->forms['newsletter_signup'] = [
            'name' => 'Newsletter Signups',
            'columns' => [
                'post_title' => 'Name',
                'email_address' => 'Email Address',
            ],
        ];

        $this->forms['together_card'] = [
            'name' => 'Together Card',
            'columns' => [
                'first_name' => 'First Name',
                'last_name' => 'Last Name',
                'email' => 'Email',
                'business_name' => 'Business Name',
                'user_type' => 'User Type',
                'stripe_status' => 'Stripe Status',
                'stripe_transaction_id' => 'Stripe Transaction ID',
                'payment_intent_id' => 'Stripe Payment Intent ID',
                'email_opt_in' => 'Email Opt In?',
                'interests' => 'Interests',
                'address' => 'Address',
            ],
        ];

        if ( !empty( $_GET['form'] ) && is_string ( $_GET['form'] ) ) {

            if ($_GET['form'] == 'together_card') {
                $query = new WP_Query([
                    'post_type' => 'form_submission',
                    'meta_query' => [
                        'relation' => 'AND',
                        [
                            'key' => 'form_name',
                            'value' => $_GET['form'],
                        ],
                        [
                            'relation' => 'OR',
                            [
                                'relation' => 'OR',
                                [
                                    'key' => 'stripe_status',
                                    'value' => 'CONFIRMED'
                                ],
                                [
                                    'key' => 'stripe_status',
                                    'value' => 'UNCONFIRMED'
                                ]
                            ],
                            [
                                'key' => 'user_type',
                                'value' => 'user_business'
                            ]
                        ]

                    ],
                    'nopaging' => true,
                ] );
            } else {
                $query = new WP_Query([
                    'post_type' => 'form_submission',
                    'meta_query' => [
                        [
                            'key' => 'form_name',
                            'value' => $_GET['form'],
                        ]
                    ],
                    'nopaging' => true,
                ] );
            }

            $form_submission_posts = Timber::get_posts($query);

            if ( is_array( $form_submission_posts ) && !empty( $form_submission_posts ) ) {

                $columns = $this->forms[ $_GET['form'] ]['columns'];
                $rows = [];

                foreach ( $form_submission_posts as $submission ) {

                        $rows[$submission->ID] = [];

                        foreach ( $columns as $column => $value ) {

                            $rows[$submission->ID][$column] = $submission->{$column};

                        }
                }

                $this->page_data['columns'] = $columns;
                $this->page_data['rows'] = $rows;

                if ( !empty( $_GET['download'] ) && $_GET['download'] === 'true' ) {

                    /**
                     * Set appropriate header data before exiting!.
                     */
                    header("Content-type: text/csv");
                    header("Content-Disposition: attachment; filename=" . $_GET['form'] . ".csv");
                    header("Pragma: no-cache");
                    header("Expires: 0");

                    Timber::render( 'admin/form-submissions-csv.twig', $this->page_data );
                    exit;

                }

            } else {

                $this->page_data['error'] = 'No submissions for that form.';

            }

        }

    }}

new FormSubmissionPostType( [
    'publicly_queryable'  => false,
    'rewrite' => false,
    'show_ui' => false,
], 'Form Submission', 'Form Submissions' );
