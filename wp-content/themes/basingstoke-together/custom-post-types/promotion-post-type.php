<?php

/**
 * Promotion - Custom Post Type.
 */
class PromotionPostType extends PostType {
	/**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {

		/**
	     * Add action hook to register news category taxonomy.
		 */
		add_action( 'init', array( $this, 'register_promotion_type_taxonomy' ) );

		/**
	     * Add action hook to hide the standard taxonomy UI
		 */
		//add_action( 'do_meta_boxes', array( $this, 'hide_meta_boxes' ) );

		/**
	     * Hide move to trash link when adding or edting a post.
		 */
		//add_action( 'admin_head-post.php', array( $this, 'hide_trash_link_when_editing' ) );
		//add_action( 'admin_head-post-new.php', array( $this, 'hide_trash_link_when_editing' ) );

		/**
	     * Add action to get a specfic column's content.
		 */
		add_action( 'manage_' . $this->post_type_name . '_posts_custom_column', array( $this, 'custom_column_content' ), 10, 2 );

		/**
	     * Add filter to customise columns shown on list page.
		 */
		add_filter( 'manage_edit-' . $this->post_type_name . '_columns',  array( $this, 'custom_columns' ) );		

		/**
	     * Default sorting logic.
		 */
		add_filter( 'pre_get_posts', array( $this, 'default_sort_by_title' ) );

		/**
	     * Hide trash link on row hover in admin UI.
		 */
		add_filter( 'post_row_actions', array( $this, 'remove_trash_link' ) );

		/**
	     * Hide delete action from bulk options dropwdown.
		 */
		add_filter( 'bulk_actions-edit-promotion', array( $this, 'remove_bulk_delete' ) );
	}	

	/**
     * Modify the default sort to by by title, rather than publish date.
	 *
	 * @param WP_Query query
	 * @return WP_Query
	 */
	public function default_sort_by_title( $query ) {

		if($query->is_admin && !array_key_exists('orderby', $_GET)) {
 
	        if ($query->get('post_type') == 'promotion')
	        {
	          $query->set('orderby', 'title');
	          $query->set('order', 'ASC');
	        }
	  }

	    return $query;
	}

	/**
     * Output CSS to hide the trash link when adding or editing Initiatives.
	 */
	public function hide_trash_link_when_editing( $actions ) {

		global $post;
        if($post->post_type == 'promotion'){
            echo '
                <style type="text/css">
                	#misc-publishing-actions,
                    #minor-publishing-actions,
                    #delete-action{
                        display:none;
                    }
                </style>
            ';
        }
	}

	/**
     * Return an array of actions shown in the admin section on row hover.
	 *
	 * @param array $actions
	 * @return array
	 */
	public function remove_trash_link( $actions ) {

		if( get_post_type() === 'promotion' ) {
	        unset( $actions['trash'] );
	    }
	    return $actions;

	}

	/**
     * Return an array of actions shown in the admin section on row hover.
	 *
	 * @param array $actions
	 * @return array
	 */
	public function remove_bulk_delete( $actions ) {

		unset( $actions['trash'] );
        return $actions;
	}

	/**
     * Return an array of columns to be used on list pages.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function custom_columns( $columns ) {
	
		//$columns['signups'] = 'Signups';
		
		unset($columns['date']);
	
		return $columns;
	}

	/**
     * Modify the query params to do a meta sort if appropriate.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function custom_column_sort_start_date( $vars ){
		if ( isset( $vars['orderby'] ) && 'start_date' == $vars['orderby'] ) {
	        $vars = array_merge( $vars, array(
	            'meta_key' => 'start_date',
	            'orderby' => 'meta_value_num'
	        ) );
	    }
	 
	    return $vars;
	}

	/**
     * Generates the relevant content for a given custom column and post.
	 *
	 * @param string $column
	 *		This is the column identifier e.g. 'job_title'.
	 * @param int $post_id
	 *		The post ID for the current column row.
	 */
	public function custom_column_content( $column, $post_id ) {
		
		global $post;		

		switch( $column ) {

			case 'signups' :

				$user_query = new WP_User_Query( array(
					'meta_query' => array(
						array(
							'key'     => 'promotion_signups',
							'value'   => $post->post_name,
							'compare' => 'IN'
						)
					)
				 ) );

				$count = $user_query->total_users;

				echo ($count > 0) ? $count . ( ($count == 1) ? ' user' : ' users' ) : 'None';
				break;
		}

	}

	/**
     * Register our custom event focus taxonomy.
	 */
	public function register_promotion_type_taxonomy() {

		$labels = array(
			'name'              => _x( 'Promotion Types', 'taxonomy general name' ),
			'singular_name'     => _x( 'Promotion Type', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Promotion Types' ),
			'all_items'         => __( 'All Promotion Types' ),
			'parent_item'       => __( 'Parent Promotion Type' ),
			'parent_item_colon' => __( 'Parent Promotion Type:' ),
			'edit_item'         => __( 'Edit Promotion Type' ),
			'update_item'       => __( 'Update Promotion Type' ),
			'add_new_item'      => __( 'Add New Promotion Type' ),
			'new_item_name'     => __( 'New Promotion Type Name' ),
			'menu_name'         => __( 'Promotion Types' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'rewrite'           => false,
		);
	
		register_taxonomy( 'promotion_type', 'promotion', $args );
	}

	/**
     * Hide meta boxes for taxonomy.
	 */
	public function hide_meta_boxes() {
		remove_meta_box( 'promotion_typediv', 'promotion', 'side' );
	}
}

new PromotionPostType( array(
	'menu_position' => 10, 
	'publicly_queryable'  => false,
	'rewrite' => false,
), 'Promotion' );

class PromotionPost extends TimberPost {
	private $_listImage;
	private $_asideImage;
	private $_checked;
	private $_intro;
	private $_aside;
	private $_action_type;
	private $_cta_link;

	/**
     * Convenience function to get an Promotion intro text content.
	 *
	 * @return string
	 */
	public function intro(){		

		if (!$this->_intro){
			$this->_intro = get_field('introduction', $this->ID);
		}

		return $this->_intro;
	}
	
	/**
     * Convenience function to get an Promotion action 'type'.
	 *
	 * @return string 'notification'|'email'|'link'|'twitter'|'business'
	 */
	public function action_type(){		

		$allowedTypes = array(
			'email',
			'link',
		);

		/**
 		 * Only get the value once, cache for future access.
 		 */
		if (!$this->_action_type){

			$this->_action_type = get_field('button_action', $this->ID);

			/**
     		 * Ensure we got a 'valid' type, fallback to notification otherwise.
     		 */
			if ( !in_array($this->_action_type, $allowedTypes) ) {
				$this->_action_type = $allowedTypes[0];
			}

		}

		return $this->_action_type;
	}

	/**
     * Convenience function to get a promotion aside text content.
	 *
	 * @return string
	 */
	public function aside(){		

		if (!$this->_aside){
			$this->_aside = get_field('aside_text', $this->ID);
		}

		return $this->_aside;
	}

	/**
     * Convenience function to get a promotion calll to action link (varies by promotion CTA type).
	 *
	 * @return TimberImage
	 */
	public function cta_link( $baseUrl = ''){

		if (!isset( $this->_cta_link ) ){
			
			switch ( $this->action_type() ) {
				case 'email':	
					$address = get_field('email_recipient', $this->ID);
					$subject = rawurlencode( get_field('email_subject', $this->ID, false) );
					$body = rawurlencode( get_field('email_body', $this->ID, false) );

					$this->_cta_link = 'mailto:' . $address .'?subject=' . $subject . '&body=' . $body;
					break;
				
				case 'link':
					$this->_cta_link = 'http://' . get_field('button_link', $this->ID);
					break;
				
				default:
					$this->_cta_link = '#';
					break;
			}


		}

		return $this->_cta_link;
	}

	/**
     * Convenience function to get a promotion list image.
	 *
	 * @return TimberImage
	 */
	public function listImage(){

		if (!$this->_listImage){
			$this->_listImage = new TimberImage($this->list_image);
		}

		return $this->_listImage;
	}

	/**
     * Convenience function to get a promotion list image.
	 *
	 * @return TimberImage
	 */
	public function asideImage(){

		if (!$this->_asideImage){
			$this->_asideImage = new TimberImage($this->aside_image);
		}

		return $this->_asideImage;
	}
	
}
