<?php

/**
 * News Article - Custom Post Type.
 */
class NewsArticlePostType extends PostType
{
	/**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {

		/**
	     * Add action hook to register news category taxonomy.
		 */
		add_action( 'init', array( $this, 'register_story_focus_taxonomy' ) );

		/**
	     * Add action to update custom rewrite rules.
		 */
		add_filter('rewrite_rules_array', array( $this, 'add_rewrite_rules' ) );

		/**
	     * Custom query vars hook (to allow custom newsyear variable).
		 */
		add_filter( 'query_vars', array( $this, 'add_query_vars') );

		/**
	     * Add action hook to hide the standard taxonomy UI
		 */
		add_action( 'do_meta_boxes', array( $this, 'hide_meta_boxes' ) );

		/**
	     * Ensure that when viewing trying to access the post types default 'list' url, we redirect to our proper page.
		 */
		Timber::add_route('news-articles/', function($params){
			$url = site_url('basingstoke-together/news/');
		    wp_redirect( $url, 301 );
			exit;
		});

		/**
	     * Ensure that when viewing a 'single' post using the post types rewrite slug that we redirect to our 'proper' url structure.
		 */
		Timber::add_route('news-articles/:name', function($params){
			$url = site_url('basingstoke-together/news/' . $params['name'] . '/');
		    wp_redirect( $url, 301 );
			exit;
		});

		/**
		 * Make news only RSS linka bit nicer.
		 */
		Timber::add_route('rss/news/', function($params){
			$url = site_url('feed/?post_type=news_article');
		    wp_redirect( $url, 301 );
			exit;
		});
	}	

	/**
     * Return an array of redirect rules used to match URL requests.
	 *
	 * @param array $rules
	 * @return array
	 */
	public function add_rewrite_rules($rules) {
		$newRules = array(
			// article list by year
			'basingstoke-together/news/([0-9]{4})/?$' => 'index.php?pagename=basingstoke-together/news&newsyear=$matches[1]',
			
			// specific article detail by slug
			'basingstoke-together/news/([^/]+)/?$' => 'index.php?news_article=$matches[1]',
			
			// article list by year and page
			'basingstoke-together/news/([0-9]{4})/page/([0-9]{1,})/?$' => 'index.php?pagename=basingstoke-together/news&newsyear=$matches[1]&paged=$matches[2]',

			// article list by page (all years)
			'basingstoke-together/news/page/([0-9]{1,})/?$' => 'index.php?pagename=basingstoke-together/news&newsyear=all&paged=$matches[1]',
		);
		
		$rules = $newRules + $rules;
		
		return $rules;
	}

	/**
     * Register our custom story focus taxonomy.
	 */
	public function register_story_focus_taxonomy() {

		$labels = array(
			'name'              => _x( 'Story Foci', 'taxonomy general name' ),
			'singular_name'     => _x( 'Story Focus', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Story Foci' ),
			'all_items'         => __( 'All Story Foci' ),
			'parent_item'       => __( 'Parent Story Focus' ),
			'parent_item_colon' => __( 'Parent Story Focus:' ),
			'edit_item'         => __( 'Edit Story Focus' ),
			'update_item'       => __( 'Update Story Focus' ),
			'add_new_item'      => __( 'Add New Story Focus' ),
			'new_item_name'     => __( 'New Story Focus Name' ),
			'menu_name'         => __( 'Story Foci' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'rewrite'           => array( 'slug' => 'news-category' ),
		);
	
		register_taxonomy( 'story_focus', 'news_article', $args );
	}

	/**
     * Hide meta boxes for taxonomy.
	 */
	public function hide_meta_boxes() {
		remove_meta_box( 'story_focusdiv', 'news_article', 'side' );
	}

	/**
     * Return an array of query vars allowed by WordPress.
	 *
	 * @param array $vars
	 * @return array
	 */
	function add_query_vars($vars) {
		array_push( $vars, 'newsyear' );
		return $vars;
	}
	 
	
}

class NewsArticlePost extends TimberPost {
	private $_link;
	private $_listImage;

	public function link(){

		if (!$this->_link){
			$this->_link = '/basingstoke-together/news/' . $this->post_name . '/';
		}

		return $this->_link;
	}

	public function listImage(){

		if (!$this->_listImage){
			$this->_listImage = new TimberImage($this->list_image);
		}

		return $this->_listImage;
	}
}

new NewsArticlePostType( array(
	'menu_position' => 12, 
	'publicly_queryable'  => true,
	'rewrite' => array(
		'slug' => 'news-articles',
		'with_front' => false,
	),
), 'News Article' );