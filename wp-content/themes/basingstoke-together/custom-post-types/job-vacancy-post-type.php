<?php

/**
 * Job Vacancy - Custom Post Type.
 */
class JobVacancyPostType extends PostType
{
	/**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {

		/**
	     * Add action hook to register job category taxonomy.
		 */
		add_action( 'init', array( $this, 'register_vacancy_category_taxonomy' ) );

		/**
	     * Add action to update custom rewrite rules.
		 */
		add_filter('rewrite_rules_array', array( $this, 'add_rewrite_rules' ) );

		/**
	     * Custom query vars hook (to allow custom newsyear variable).
		 */
		add_filter( 'query_vars', array( $this, 'add_query_vars') );

		/**
	     * Add action hook to hide the standard taxonomy UI
		 */
		add_action( 'do_meta_boxes', array( $this, 'hide_meta_boxes' ) );		

		/**
	     * Ensure that when viewing trying to access the post types default 'list' url, we redirect to our proper page.
		 */
		Timber::add_route('job-vacancies/', function($params){
			$url = site_url('in-basingstoke/job-vacancies/');
		    wp_redirect( $url, 301 );
			exit;
		});

		/**
	     * Ensure that when viewing a 'single' post using the post types rewrite slug that we redirect to our 'proper' url structure.
		 */
		Timber::add_route('job-vacancies/:name', function($params){
			$url = site_url('in-basingstoke/job-vacancies/' . $params['name'] . '/');
		    wp_redirect( $url, 301 );
			exit;
		});

	}	
	
	/**
     * Return an array of redirect rules used to match URL requests.
	 *
	 * @param array $rules
	 * @return array
	 */
	public function add_rewrite_rules($rules) {
		$newRules = array(			

			// by category
			'in-basingstoke/job-vacancies/by-category/([^/]+)/?$' => 'index.php?pagename=in-basingstoke/job-vacancies&by_category=$matches[1]',

			// by category and page
			'in-basingstoke/job-vacancies/by-category/([^/]+)/page/([0-9]{1,})/?$' => 'index.php?pagename=in-basingstoke/job-vacancies&by_category=$matches[1]&paged=$matches[2]',

			// specific vacancy by slug
			'in-basingstoke/job-vacancies/([^/]+)/?$' => 'index.php?job_vacancy=$matches[1]',
			
			// vacancy list by page (all years)
			'in-basingstoke/job-vacancies/page/([0-9]{1,})/?$' => 'index.php?pagename=in-basingstoke/job-vacancies&paged=$matches[1]',
		);
		
		$rules = $newRules + $rules;
		
		return $rules;
	}

	/**
     * Register our custom event focus taxonomy.
	 */
	public function register_vacancy_category_taxonomy() {

		$labels = array(
			'name'              => _x( 'Job Category', 'taxonomy general name' ),
			'singular_name'     => _x( 'Job Category', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Job Categories' ),
			'all_items'         => __( 'All Job Categories' ),
			'parent_item'       => __( 'Parent Job Category' ),
			'parent_item_colon' => __( 'Parent Job Category:' ),
			'edit_item'         => __( 'Edit Job Category' ),
			'update_item'       => __( 'Update Job Category' ),
			'add_new_item'      => __( 'Add New Job Category' ),
			'new_item_name'     => __( 'New Job Category Name' ),
			'menu_name'         => __( 'Job Categories' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'rewrite'           => array( 'slug' => 'vacancy-category' ),
		);
	
		register_taxonomy( 'vacancy_category', 'job_vacancy', $args );
	}

	/**
     * Hide meta boxes for taxonomy.
	 */
	public function hide_meta_boxes() {
		remove_meta_box( 'vacancy_categorydiv', 'job_vacancy', 'side' );
	}

	/**
     * Return an array of query vars allowed by WordPress.
	 *
	 * @param array $vars
	 * @return array
	 */
	function add_query_vars($vars) {
		array_push( $vars, 'by_category' );
		return $vars;
	}
	 
	
}

new JobVacancyPostType( array(
	'menu_position' => 11, 
	'publicly_queryable'  => true,
	'rewrite' => array(
		'slug' => 'job-vacancies',
		'with_front' => false,
	),
), 'Job Vacancy', 'Job Vacancies' );

class JobVacancyPost extends TimberPost {
	private $_link;
	private $_listImage;

	public function link(){

		if (!$this->_link){
			$this->_link = '/in-basingstoke/job-vacancies/' . $this->post_name . '/';
		}

		return $this->_link;
	}

}