<?php

/**
 * Contact - Custom Post Type.
 */
class ContactPostType extends PostType
{
	/**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {

		/**
	     * Add filter to customise columns shown on list page.
		 */
		add_filter( 'manage_edit-' . $this->post_type_name . '_columns',  array( $this, 'custom_columns' ) );

		/**
	     * Add action to get a specfic column's content.
		 */
		add_action( 'manage_' . $this->post_type_name . '_posts_custom_column', array( $this, 'custom_column_content' ), 10, 2 );
	}	

	/**
     * Return an array of columns to be used on list pages.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function custom_columns( $columns ) {
	
		$columns = array(
			'cb' => '<input type="checkbox" />',
			'title' => __( 'Name' ),
			'job_title' => __( 'Job Title' ),
			'email' => __( 'Email Address' ),
			'telephone' => __( 'Telephone No.' ),
		);
	
		return $columns;
	}

	/**
     * Generates the relevant content for a given custom column and post.
	 *
	 * @param string $column
	 *		This is the column identifier e.g. 'job_title'.
	 * @param int $post_id
	 *		The post ID for the current column row.
	 */
	public function custom_column_content( $column, $post_id ) {
		
		switch( $column ) {
	
			case 'job_title' :
	
				$jobTitle = get_post_meta( $post_id, 'job_title', true );
	
				if ( empty( $jobTitle ) ) {
					echo 'Not specified';
				} else {
					echo $jobTitle;
				}

				break;

			case 'email' :
	
				$email = get_post_meta( $post_id, 'email_address', true );
	
				if ( empty( $email ) ) {
					echo 'Not specified';
				} else {
					echo '<a href="mailto:' . $email . '">' . $email . '</a>';
				}

				break;

			case 'telephone' :
	
				$phone = get_post_meta( $post_id, 'phone_number', true );
	
				if ( empty( $phone ) ) {
					echo 'Not specified';
				} else {
					echo $phone;
				}

				break;
	
			default :
				break;
		}
	}
}

$contact = new ContactPostType( array('menu_position' => 6), 'Contact' );