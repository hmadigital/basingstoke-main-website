<?php

/**
 * Event - Custom Post Type.
 */
class EventPostType extends PostType
{
	/**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {

		/**
	     * Add action hook to register news category taxonomy.
		 */
		add_action( 'init', array( $this, 'register_event_focus_taxonomy' ) );

		/**
	     * Add action to update custom rewrite rules.
		 */
		add_filter('rewrite_rules_array', array( $this, 'add_rewrite_rules' ) );

		/**
	     * Custom query vars hook (to allow custom newsyear variable).
		 */
		//add_filter( 'query_vars', array( $this, 'add_query_vars') );

		/**
	     * Add action hook to hide the standard taxonomy UI
		 */
		add_action( 'do_meta_boxes', array( $this, 'hide_meta_boxes' ) );

		/**
	     * Add filter to customise columns shown on list page.
		 */
		add_filter( 'manage_edit-' . $this->post_type_name . '_columns',  array( $this, 'custom_columns' ) );

		/**
	     * Add filter for custom column sorting on list page.
		 */
		add_filter( 'manage_edit-' . $this->post_type_name . '_sortable_columns',  array( $this, 'sortable_custom_columns' ) );

		/**
	     * Add action to get a specfic column's content.
		 */
		add_action( 'manage_' . $this->post_type_name . '_posts_custom_column', array( $this, 'custom_column_content' ), 10, 2 );

		/**
	     * Custom sorting logic.
		 */
		add_filter( 'request', array( $this, 'custom_column_sort_start_date' ) );

		/**
	     * Ensure that when viewing trying to access the post types default 'list' url, we redirect to our proper page.
		 */
		Timber::add_route('events-list/', function($params){
			$url = site_url('/basingstoke-together/events/');
			wp_redirect( $url, 301 );
			exit;
		});

		/**
	     * Ensure that when viewing a 'single' post using the post types rewrite slug that we redirect to our 'proper' url structure.
		 */
		Timber::add_route('events-list/:name', function($params){

			// get event by slug
			// check site_section meta, redirect accordingly

			$post = Timber::get_post( [
				'post_type' => 'event',
				'name' => $params['name'],
			] );
			
			$site_section = ( $post->event_section === 'basingstoketogether' ) ? 'basingstoke-together' : 'in-basingstoke';

			$url = site_url('/' . $site_section . '/events/' . $params['name'] . '/');
			wp_redirect( $url, 301 );
			exit;
			
		});

		/**
		 * Make events only RSS linka bit nicer.
		 */
		Timber::add_route('rss/events/', function($params){
			$url = site_url('feed/?post_type=event');
		    wp_redirect( $url, 301 );
			exit;
		});
	}



	/**
     * Return an array of columns to be used on list pages.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function custom_columns( $columns ) {

		$columns['start_date'] = 'Start Date';
		$columns['end_date'] = 'End Date';

		unset($columns['wpseo-seotitle']);
		unset($columns['wpseo-focuskw']);
		unset($columns['date']);

		return $columns;
	}

	/**
     * Return an array of columns to be used for sorting on list pages.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function sortable_custom_columns( $columns ) {
		$columns['start_date'] = 'start_date';

    	return $columns;
	}

	/**
     * Modify the query params to do a meta sort if appropriate.
	 *
	 * @param array $columns
	 * @return array
	 */
	public function custom_column_sort_start_date( $vars ){
		if ( isset( $vars['orderby'] ) && 'start_date' == $vars['orderby'] ) {
	        $vars = array_merge( $vars, array(
	            'meta_key' => 'start_date',
	            'orderby' => 'meta_value_num'
	        ) );
	    }

	    return $vars;
	}

	/**
     * Generates the relevant content for a given custom column and post.
	 *
	 * @param string $column
	 *		This is the column identifier e.g. 'job_title'.
	 * @param int $post_id
	 *		The post ID for the current column row.
	 */
	public function custom_column_content( $column, $post_id ) {

		global $post;

		switch( $column ) {

			case 'list_text' :
				echo get_field('list_text', $post->ID);
				break;

			case 'start_date' :
				$start = new DateTime( get_field('start_date', $post->ID) );
				echo date_format($start, 'd F Y');
				break;

			case 'end_date' :
				$hasEndDate = ( get_field('multiday_event', $post->ID) == 'yes');
				if ($hasEndDate === true) {
					$end = new DateTime( get_field('end_date', $post->ID) );
					echo date_format($end, 'd F Y');
				} else {
					echo 'N/A';
				}
				break;

		}

	}

	/**
     * Return an array of redirect rules used to match URL requests.
	 *
	 * @param array $rules
	 * @return array
	 */
	public function add_rewrite_rules($rules) {
		$newRules = array(

			// specific article detail by slug
			'basingstoke-together/events/([^/]+)/?$' => 'index.php?event=$matches[1]',

			// article list by page (all years)
			'basingstoke-together/events/page/([0-9]{1,})/?$' => 'index.php?pagename=basingstoke-together/events&paged=$matches[1]',

			// specific article detail by slug
			'in-basingstoke/events/([^/]+)/?$' => 'index.php?event=$matches[1]',

			// article list by page (all years)
			'in-basingstoke/events/page/([0-9]{1,})/?$' => 'index.php?pagename=in-basingstoke/events&paged=$matches[1]',
			
		);

		$rules = $newRules + $rules;

		return $rules;
	}

	/**
     * Register our custom event focus taxonomy.
	 */
	public function register_event_focus_taxonomy() {

		$labels = array(
			'name'              => _x( 'Event Foci', 'taxonomy general name' ),
			'singular_name'     => _x( 'Event Focus', 'taxonomy singular name' ),
			'search_items'      => __( 'Search Event Foci' ),
			'all_items'         => __( 'All Event Foci' ),
			'parent_item'       => __( 'Parent Event Focus' ),
			'parent_item_colon' => __( 'Parent Event Focus:' ),
			'edit_item'         => __( 'Edit Event Focus' ),
			'update_item'       => __( 'Update Event Focus' ),
			'add_new_item'      => __( 'Add New Event Focus' ),
			'new_item_name'     => __( 'New Event Focus Name' ),
			'menu_name'         => __( 'Event Foci' ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'rewrite'           => array( 'slug' => 'event-category' ),
		);

		register_taxonomy( 'event_focus', 'event', $args );
	}

	/**
     * Hide meta boxes for taxonomy.
	 */
	public function hide_meta_boxes() {
		remove_meta_box( 'event_focusdiv', 'event', 'side' );
	}

	/**
     * Return an array of query vars allowed by WordPress.
	 *
	 * @param array $vars
	 * @return array
	 */
	function add_query_vars($vars) {
		array_push( $vars, 'newsyear' );
		return $vars;
	}

}

new EventPostType( array(
	'menu_position' => 8,
	'publicly_queryable'  => true,
	'rewrite' => array(
		'slug' => 'events-list',
		'with_front' => false,
	),
), 'Event' );

class EventPost extends TimberPost {
	private $_link;
	private $_listImage;

	public function link(){

		if (!$this->_link){
			$site_section = ($this->event_section === 'inbasingstoke') ? 'in-basingstoke' : 'basingstoke-together';
			$this->_link = '/' . $site_section . '/events/' . $this->post_name . '/';
		}

		return $this->_link;
	}

	public function listImage(){

		if (!$this->_listImage){
			$this->_listImage = new TimberImage($this->list_image);
		}

		return $this->_listImage;
	}
}