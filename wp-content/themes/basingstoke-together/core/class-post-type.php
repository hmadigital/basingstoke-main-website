<?php

/**
 * Convenience class for quickly creating custom post types.
 */
class PostType {

	public $post_type_name = '';
	public $post_type_config = array();
	public $post_type_singular = '';
	public $post_type_plural = '';

	/**
	 * Constructor. Hooks post type register up to init action.
	 *
	 * @param array $config 
	 * @param string $singular 
	 * @param string $plural
	 */
    function __construct($config = array(), $singular, $plural = '') {
    	
    	$this->post_type_config = $config;
    	$this->post_type_singular = $singular;
    	$this->post_type_plural = ( empty( $plural ) ) ? $singular . 's' : $plural;

    	/**
		 * Set the 'key' for our custom post type based on the singular passed in.
		 */
    	$this->post_type_name = str_replace( ' ', '_', strtolower( $singular ) ); 

    	/**
		 * Add register to 'init' action.
		 */
		add_action( 'init', array( $this, 'register_post_type' ) );

		/**
		 * Used by classes that extend this one to add extra hooks etc.
		 */
		$this->additional_actions();
    }

    /**
     * Used by classes that extend this one to add extra hooks etc.
	 */
	public function additional_actions() {
	}

    /**
     * Get the required strings for a given post type.
	 *
	 * @param string $singular
	 * @param string $plural
	 * @return array
	 */
	public function register_post_type() {

		/**
		 * Default settings. Get merged with custom config passed into constructor.
		 */
		$defaults = array(            
            'hierarchical' 		  => false,
            'has_archive' 		  => true,
            'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'menu_position'       => 4,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => false,
			'capability_type'     => 'page',
			'rewrite'			  => false,
			'supports' 			  => array( 'title' ),
			'labels' 			  => $this->label_inflections($this->post_type_singular, $this->post_type_plural),
        );


		/**
		 * Actually register our post type.
		 */
    	register_post_type( $this->post_type_name, array_merge( $defaults, $this->post_type_config ) );
	}


    /**
     * Get the required strings for a given post type.
	 *
	 * @param string $singular
	 * @param string $plural
	 * @return array
	 */
	protected function label_inflections( $singular, $plural ) {
	 
	    return array(
	        'name' => $plural,
	        'singular_name' => $singular,
	        'search_items' => 'Search ' . $plural,
	        'all_items' => $plural,
	        'edit_item' => 'Edit ' . $plural,
	        'add_new_item' => 'Add New ' . $singular,
	        'menu_name' => $plural,
	        'new_item' => 'New ' . $singular,
	        'view_item' => 'View ' . $plural,
	        'not_found' => 'No ' . $plural . ' found',
	        'not_found_in_trash' => 'No ' . $plural . ' found in Trash',
	        'parent_item_colon' => '',
	    );
	}

}