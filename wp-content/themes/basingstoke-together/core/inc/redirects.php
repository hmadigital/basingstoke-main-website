<?php

$currentURL = strtolower( $_SERVER["REQUEST_URI"] );

if ($currentURL[0] == '/') {
	$currentURL = substr($currentURL, 1);
}

$customRedirects = array(
	'businesscompetition' => 'business-opportunities/business-competition/',
	'businesscompetition/' => 'business-opportunities/business-competition/',
);

foreach($customRedirects as $src => $dest) {
	
	if ($currentURL === strtolower( $src )) {
		wp_redirect( site_url( $dest ), 301 ); 
		exit;
	}

}