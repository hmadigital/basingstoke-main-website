<?php 
	
	function comments_custom_comment_field($args) {
		$args['comment_field'] = '<div class="comment-form-comment"><label for="comment">' . _x( 'Comment', 'noun' ) . '</label><textarea id="comment" name="comment" aria-required="true"></textarea></div>';
		
		$args['label_submit'] = 'Comment &gt;';
		$args['title_reply'] = 'Leave a Comment';
		$args['title_reply_to'] = 'Comment on %s';
		$args['comment_notes_before'] = '<p class="comment-notes">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent tempus vulputate lacus ac porta. </p>';
		$args['comment_notes_after'] = '<div class="recaptcha-wrap">' . recaptcha_get_html(RECAPTCHA_PUBLIC_KEY) . '</div><p class="denotes">*Denotes mandatory field</p>';
		
		return $args;
	}
	
	add_filter('comment_form_defaults', 'comments_custom_comment_field');

	
	function comments_custom_fields($fields) {

		$commenter = wp_get_current_commenter();
		$req = get_option( 'require_name_email' );
		$aria_req = ( $req ? " aria-required='true'" : '' );
		
		$nonce = wp_nonce_field('custom_comment_nonce', 'comment_nonce');

		$fields[ 'author' ] = '<div class="comment-form-author">'.
			'<label for="author">' . __( 'Name' ) .
			( $req ? '*' : '' ) .
			'</label>'.
			'<input id="author" name="author" type="text" value="'. esc_attr( $commenter['comment_author'] ) .
			'" size="30" tabindex="1"' . $aria_req . ' /></div>';

		$fields[ 'email' ] = '<div class="comment-form-email">'.
			'<label for="email">' . __( 'Email Address' ) .
			( $req ? '*' : '' ).
			'</label>'.
			'<input id="email" name="email" type="text" value="'. esc_attr( $commenter['comment_author_email'] ) .
			'" size="30"  tabindex="2"' . $aria_req . ' /></div>';
			
		$fields[ 'confirmemail' ] = '<div class="comment-form-email">'.
			'<label for="confirmemail">' . __( 'Confirm Email Address' ) . 
			( $req ? '*' : '' ).
			'</label>'.
			'<input id="confirmemail" name="confirmemail" type="text" value="'. esc_attr( $commenter['comment_author_email'] ) .
			'" size="30"  tabindex="3"' . $aria_req . ' /></div>';
		
		$fields[ 'url' ] = '';
		
		$fields[ 'nonce' ] = $nonce;
		
		return $fields;
	}
	
	add_filter('comment_form_default_fields', 'comments_custom_fields');
	
	
	function comments_custom_verify($commentdata) {
	
		$commenter = wp_get_current_commenter();
		
		$hasCommenter = ($commenter && strlen($commenter['comment_author_email'])>0);
		
		$errors = array();
	
		$email = ($hasCommenter) ? $commenter['comment_author_email'] : strip_tags($_POST['email']);
		$confirmEmail = ($hasCommenter) ? $commenter['comment_author_email'] :  strip_tags($_POST['confirmemail']);
		
		
		if(!$hasCommenter && (!isset( $_POST['comment_nonce'] ) || !wp_verify_nonce( $_POST['comment_nonce'], 'custom_comment_nonce' ))) {
        	$errors['nonce'] = 'Invalid nonce. Please try again.';
    	}
        	
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$errors['email'] = 'Please enter a valid email address.';
			
			if(!filter_var($confirmEmail, FILTER_VALIDATE_EMAIL)) {
				$errors['confirmemail'] = 'Please confirm your email address.';
			}
			
		} else {
			if(!filter_var($confirmEmail, FILTER_VALIDATE_EMAIL)) {
				$errors['confirmemail'] = 'Please confirm your email address.';
			} else if ($email !== $confirmEmail) {
				$errors['confirmemail'] = 'Please ensure your confirmation email address matches.';
			}
		}
		
		if ( ! isset( $_POST['recaptcha_challenge_field'] ) ) {
			$errors['recaptcha'] = 'You did not enter a captcha solution.';
		}
		
		$resp = recaptcha_check_answer(RECAPTCHA_PRIVATE_KEY, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
				
		if (!$resp->is_valid) {
			$errors['recaptcha'] = 'Please enter a valid captcha solution.';
		}
		
		if (count($errors)> 0) {
			$dieMessage = '<strong>Sorry, there are some errors with your submission:</strong><br/><br/>';
			
			foreach($errors as $error) {
				$dieMessage .= $error . '<br/>';
			}
			
			$dieMessage .= '<br/>Hit the Back button on your Web browser to try and resubmit your comment.';
			wp_die( __( $dieMessage ) );
		}
		
		return $commentdata;
	}	
	add_filter( 'preprocess_comment', 'comments_custom_verify' );

	if ( ! function_exists( 'catch_comment' ) ) :
		function catch_comment( $comment, $args, $depth ) {
			$GLOBALS['comment'] = $comment;
			switch ( $comment->comment_type ) :
				case 'pingback' :
				case 'trackback' :
				// Display trackbacks differently than normal comments.
			?>
			<li <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
				<p><?php _e( 'Pingback:', 'twentytwelve' ); ?> <?php comment_author_link(); ?> <?php edit_comment_link( __( '(Edit)', 'twentytwelve' ), '<span class="edit-link">', '</span>' ); ?></p>
			<?php
					break;
				default :
				// Proceed with normal comments.
				global $post;
			?>
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
				<article id="comment-<?php comment_ID(); ?>" class="comment">
					<div class="comment-meta comment-author vcard">
						<?php
							printf( 'by <cite class="fn">%1$s %2$s</cite> | ',
								get_comment_author_link(),
								// If current post author is also comment author, make it known visually.
								( $comment->user_id === $post->post_author ) ? '<span> (' . __( 'Post author', 'twentytwelve' ) . ')</span>' : ''
							);
							printf( ' <a href="%1$s"><time datetime="%2$s">%3$s</time></a> | ',
								esc_url( get_comment_link( $comment->comment_ID ) ),
								get_comment_time( 'c' ),
								/* translators: 1: date, 2: time */
								sprintf( __( '%1$s at %2$s', 'twentytwelve' ), get_comment_date(), get_comment_time() )
							);
						?>
						<span class="reply">
							<?php comment_reply_link( array_merge( $args, array( 'reply_text' => __( 'Reply', 'twentytwelve' ), 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
						</span>
					</div><!-- .comment-meta -->
		
					<?php if ( '0' == $comment->comment_approved ) : ?>
						<p class="comment-awaiting-moderation"><?php _e( 'Your comment is awaiting moderation.', 'twentytwelve' ); ?></p>
					<?php endif; ?>
		
					<section class="comment-content comment">
						<?php comment_text(); ?>
						<?php edit_comment_link( __( 'Edit', 'twentytwelve' ), '<p class="edit-link">', '</p>' ); ?>
					</section><!-- .comment-content -->
				</article><!-- #comment-## -->
			<?php
				break;
			endswitch; // end comment_type check
		}
	endif;
	
?>