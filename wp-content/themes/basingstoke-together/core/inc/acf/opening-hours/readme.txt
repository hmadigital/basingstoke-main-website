=== Advanced Custom Fields: Opening Hours Field ===
Contributors: AUTHOR_NAME
Tags: PLUGIN_TAGS
Requires at least: 3.5
Tested up to: 3.8.1
Stable tag: trunk
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

DESCRIPTION

== Description ==

EXTENDED_DESCRIPTION

= Compatibility =

This ACF field type is compatible with:
* ACF 5
* ACF 4

== Installation ==

1. Copy the `acf-opening_hours` folder into your `wp-content/plugins` folder
2. Activate the Opening Hours plugin via the plugins admin page
3. Create a new field via ACF and select the Opening Hours type
4. Please refer to the description for more info regarding the field type settings

== Changelog ==

= 1.0.0 =
* Initial Release.