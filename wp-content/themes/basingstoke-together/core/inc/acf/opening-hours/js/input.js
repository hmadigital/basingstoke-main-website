(function($){
	
	function checkLis(){

	}
	
	function initialize_field( $el ) {
		
		var _lis = $el.find('li'),
			_chks = $el.find('input[type="checkbox"]'),

			checkLis = function(){
				_lis.removeClass('checked');

				_chks.each(function(idx, chk){

					var _check = $(chk);

					if (_check.is(':checked')) {
						_check.parents('li').addClass('checked');
					} else {
						_check.parents('li').removeClass('checked');
					}

				});
			};

		checkLis();

		_chks.on('change', checkLis);
	}
	
	
	if( typeof acf.add_action !== 'undefined' ) {
	
		/*
		*  ready append (ACF5)
		*
		*  These are 2 events which are fired during the page load
		*  ready = on page load similar to $(document).ready()
		*  append = on new DOM elements appended via repeater field
		*
		*  @type	event
		*  @date	20/07/13
		*
		*  @param	$el (jQuery selection) the jQuery element which contains the ACF fields
		*  @return	n/a
		*/
		
		acf.add_action('ready append', function( $el ){
			
			// search $el for fields of type 'opening_hours'
			acf.get_fields({ type : 'opening_hours'}, $el).each(function(){
				
				initialize_field( $(this) );
				
			});
			
		});
		
		
	} else {
		
		
		/*
		*  acf/setup_fields (ACF4)
		*
		*  This event is triggered when ACF adds any new elements to the DOM. 
		*
		*  @type	function
		*  @since	1.0.0
		*  @date	01/01/12
		*
		*  @param	event		e: an event object. This can be ignored
		*  @param	Element		postbox: An element which contains the new HTML
		*
		*  @return	n/a
		*/
		
		$(document).live('acf/setup_fields', function(e, postbox){
			
			$(postbox).find('.field[data-field_type="opening_hours"]').each(function(){
				
				initialize_field( $(this) );
				
			});
		
		});
	
	
	}


})(jQuery);
