<?php

class acf_field_opening_hours extends acf_field {
	
	// vars
	var $settings, // will hold info such as dir / path
		$defaults; // will hold default field options
		
		
	/*
	*  __construct
	*
	*  Set name / label needed for actions / filters
	*
	*  @since	3.6
	*  @date	23/01/13
	*/
	
	function __construct()
	{
		// vars
		$this->name = 'opening_hours';
		$this->label = __('Opening Hours');
		$this->category = __("Choice",'acf'); // Basic, Content, Choice, etc
		$this->defaults = array(
			// add default here to merge into your field. 
			'days' => array(
				'mon' => 'Monday',
				'tues' => 'Tuesday',
				'weds' => 'Wednesday',
				'thurs' => 'Thursday',
				'fri' => 'Friday',
				'sat' => 'Saturday',
				'sun' => 'Sunday',
			),
			'times' => array(),
		);


		foreach (range(0,23) as $fullhour) {
		   $parthour = $fullhour > 12 ? $fullhour - 12 : $fullhour; 
		   $suffix = $fullhour > 11 ? ' pm' : ' am'; 
		   $this->defaults['times']["$fullhour:00"] = $parthour . ':00' . $suffix; 
		   $this->defaults['times']["$fullhour:15"] = $parthour . ':15' . $suffix; 
		   $this->defaults['times']["$fullhour:30"] = $parthour . ':30' . $suffix;
		   $this->defaults['times']["$fullhour:45"] = $parthour . ':45' . $suffix; 
		}		
		
		// do not delete!
    	parent::__construct();
    	
    	
    	// settings
		$this->settings = array(
			'path' => apply_filters('acf/helpers/get_path', __FILE__),
			'dir' => apply_filters('acf/helpers/get_dir', __FILE__),
			'version' => '1.0.0'
		);

	}
	
	
	/*
	*  create_options()
	*
	*  Create extra options for your field. This is rendered when editing a field.
	*  The value of $field['name'] can be used (like below) to save extra data to the $field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field	- an array holding all the field's data
	*/
	
	function create_options( $field )
	{
		// defaults?
		/*
		$field = array_merge($this->defaults, $field);
		*/
		
		// key is needed in the field names to correctly save the data
		$key = $field['name'];
		
	}
	
	
	/*
	*  create_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field - an array holding all the field's data
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/
	
	function create_field( $field )
	{
		// defaults?
		
		$field = array_merge($this->defaults, $field);
		

		// value must be array
		if( !is_array($field['value']) )
		{
			// perhaps this is a default value with new lines in it?
			if( strpos($field['value'], "\n") !== false )
			{
				// found multiple lines, explode it
				$field['value'] = explode("\n", $field['value']);
			}
			else
			{
				$field['value'] = array( $field['value'] );
			}
		}

		//var_dump($field);
		
		// vars
		$i = 0;
		$e = '<input type="hidden" name="' .  esc_attr($field['name']) . '" value="" />';
		$e .= '<ul class="acf-checkbox-list opening-hours-day-list">';
		
		
		// foreach choices
		foreach( $field['days'] as $key => $value )
		{
			// vars
			$i++;
			$atts = '';
			$altCls = '';
			$fromVal = '';
			$toVal = '';

			if( array_key_exists($key, $field['value']) && array_key_exists('open', $field['value'][$key]) && $field['value'][$key]['open'] == true)
			{
				$atts = 'checked="yes"';
				$fromVal = $field['value'][$key]['from'];
				$toVal = $field['value'][$key]['to'];
			}

			if ( $i % 2 == 0) {
				$altCls = 'alt';
			}

			$name = esc_attr($field['name']) . '[' . esc_attr($key) . ']';
			
			
			// each checkbox ID is generated with the $key, however, the first checkbox must not use $key so that it matches the field's label for attribute
			$id = $field['id'];
			
			if( $i > 1 )
			{
				$id .= '_' . $key;
			}


			
			
			$e .= '<li class="' . $altCls . '">';
			$e .= '		<label class="checkbox"><input id="' . esc_attr($id) . '" type="checkbox" class="' . esc_attr($field['class']) . '" name="' . $name . '[checked]" value="1" ' . $atts . ' />' . $value . '</label>';
			$e .= '		<div class="from-to-range">';
			$e .= '			<div class="from">';
			$e .= '			<label for="' . $name . '[from]">From:</label>';
			$e .= '				<select id="' . $name . '[from]" name="' . $name . '[from]">';

			foreach ($field['times'] as $key => $value) {
				$selected = ($fromVal == $key) ? 'selected' : '';
				$e .= '<option value="' . $key .'" ' . $selected . '>' . $value . '</option>';
			}

			$e .= '				</select>';
			$e .= '			</div>';
			$e .= '			<div class="to">';
			$e .= '			<label for="' . $name . '[to]">To:</label>';
			$e .= '				<select id="' . $name . '[to]" name="' . $name . '[to]">';
			
			foreach ($field['times'] as $key => $value) {
				$selected = ($toVal == $key) ? 'selected' : '';
				$e .= '<option value="' . $key .'" ' . $selected . '>' . $value . '</option>';
			}

			$e .= '				</select>';
			$e .= '			</div>';
			$e .= '		</div>';
			$e .= '</li>';
		}
		
		$e .= '</ul>';
		
		
		// return
		echo $e;
	}
	
	
	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your create_field() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function input_admin_enqueue_scripts()
	{
		// Note: This function can be removed if not used
		
		
		// register ACF scripts
		wp_register_script( 'acf-input-opening_hours', $this->settings['dir'] . 'js/input.js', array('acf-input'), $this->settings['version'] );
		wp_register_style( 'acf-input-opening_hours', $this->settings['dir'] . 'css/input.css', array('acf-input'), $this->settings['version'] ); 
		
		
		// scripts
		wp_enqueue_script(array(
			'acf-input-opening_hours',	
		));

		// styles
		wp_enqueue_style(array(
			'acf-input-opening_hours',	
		));
		
		
	}
	
	
	/*
	*  input_admin_head()
	*
	*  This action is called in the admin_head action on the edit screen where your field is created.
	*  Use this action to add CSS and JavaScript to assist your create_field() action.
	*
	*  @info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_head
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function input_admin_head()
	{
		// Note: This function can be removed if not used
	}
	
	
	/*
	*  field_group_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is edited.
	*  Use this action to add CSS + JavaScript to assist your create_field_options() action.
	*
	*  $info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_enqueue_scripts
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function field_group_admin_enqueue_scripts()
	{
		// Note: This function can be removed if not used
	}

	
	/*
	*  field_group_admin_head()
	*
	*  This action is called in the admin_head action on the edit screen where your field is edited.
	*  Use this action to add CSS and JavaScript to assist your create_field_options() action.
	*
	*  @info	http://codex.wordpress.org/Plugin_API/Action_Reference/admin_head
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*/

	function field_group_admin_head()
	{
		// Note: This function can be removed if not used
	}


	/*
	*  load_value()
	*
		*  This filter is applied to the $value after it is loaded from the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value - the value found in the database
	*  @param	$post_id - the $post_id from which the value was loaded
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$value - the value to be saved in the database
	*/
	
	function load_value( $value, $post_id, $field )
	{
		// Note: This function can be removed if not used
		return $value;
	}
	
	
	/*
	*  update_value()
	*
	*  This filter is applied to the $value before it is updated in the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value - the value which will be saved in the database
	*  @param	$post_id - the $post_id of which the value will be saved
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$value - the modified value
	*/
	
	function update_value( $value, $post_id, $field )
	{
		$results = array();

		// Note: This function can be removed if not used
		if ( is_array( $value ) )
		{	
			// loop over our days, so they are always defined for tpl usage etc.
			foreach ($this->defaults['days'] as $dayKey => $dayName) {

				$newDay = array(
						'key' => $dayKey,
						'day' => $dayName,
						'open' => false,
					);

				//check the 'checked' attribute is , umm, checked. if it is, we need to change open to true, and set the from/to times.
				if ( array_key_exists( $dayKey, $value ) && array_key_exists( 'checked', $value[$dayKey] ) ) {

					$newDay['open'] = true;
					$newDay['from'] = ( array_key_exists( 'from', $value[$dayKey] ) ) ? esc_attr( $value[$dayKey]['from'] ) : false;
					$newDay['to'] = ( array_key_exists( 'from', $value[$dayKey] ) ) ? esc_attr( $value[$dayKey]['to'] ) : false;
				}

				$results[$dayKey] = $newDay;				
			}

		} 

		return $results;
	}
	
	
	/*
	*  format_value()
	*
	*  This filter is applied to the $value after it is loaded from the db and before it is passed to the create_field action
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value	- the value which was loaded from the database
	*  @param	$post_id - the $post_id from which the value was loaded
	*  @param	$field	- the field array holding all the field options
	*
	*  @return	$value	- the modified value
	*/
	
	function format_value( $value, $post_id, $field )
	{
		// defaults?
		/*
		$field = array_merge($this->defaults, $field);
		*/
		
		// perhaps use $field['preview_size'] to alter the $value?
		
		
		// Note: This function can be removed if not used
		return $value;
	}
	
	
	/*
	*  format_value_for_api()
	*
	*  This filter is applied to the $value after it is loaded from the db and before it is passed back to the API functions such as the_field
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value	- the value which was loaded from the database
	*  @param	$post_id - the $post_id from which the value was loaded
	*  @param	$field	- the field array holding all the field options
	*
	*  @return	$value	- the modified value
	*/
	
	function format_value_for_api( $value, $post_id, $field )
	{
		// defaults?
		/*
		$field = array_merge($this->defaults, $field);
		*/
		
		// perhaps use $field['preview_size'] to alter the $value?
		
		
		// Note: This function can be removed if not used
		return $value;
	}
	
	
	/*
	*  load_field()
	*
	*  This filter is applied to the $field after it is loaded from the database
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field - the field array holding all the field options
	*
	*  @return	$field - the field array holding all the field options
	*/
	
	function load_field( $field )
	{
		// Note: This function can be removed if not used
		return $field;
	}
	
	
	/*
	*  update_field()
	*
	*  This filter is applied to the $field before it is saved to the database
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field - the field array holding all the field options
	*  @param	$post_id - the field group ID (post_type = acf)
	*
	*  @return	$field - the modified field
	*/

	function update_field( $field, $post_id )
	{
		// Note: This function can be removed if not used
		return $field;
	}

	
}


// create field
new acf_field_opening_hours();

?>
