<?php

class acf_field_s3_file_upload extends acf_field {
	
	
	/*
	*  __construct
	*
	*  This function will setup the field type data
	*
	*  @type	function
	*  @date	5/03/2014
	*  @since	5.0.0
	*
	*  @param	n/a
	*  @return	n/a
	*/
	
	function __construct() {
		
		/*
		*  name (string) Single word, no spaces. Underscores allowed
		*/
		
		$this->name = 's3_file_upload';
		
		
		/*
		*  label (string) Multiple words, can include spaces, visible when selecting a field type
		*/
		
		$this->label = __('S3 File Upload', 'acf-s3_file_upload');
		
		
		/*
		*  category (string) basic | content | choice | relational | jquery | layout | CUSTOM GROUP NAME
		*/
		
		$this->category = 'content';
		
		
		/*
		*  defaults (array) Array of default settings which are merged into the field object. These are used later in settings
		*/
		
		$this->defaults = array(
			's3_api_access_key_id' => '',
			's3_api_secret_access_key' => '',
			's3_api_bucket_name' => '',
			's3_api_bucket_location' => '',
		);
		
		
		/*
		*  l10n (array) Array of strings that are used in JavaScript. This allows JS strings to be translated in PHP and loaded via:
		*  var message = acf._e('s3_file_upload', 'error');
		*/
		
		$this->l10n = array(
			//'error'	=> __('Error! Please enter a higher value', 'acf-s3_file_upload'),
		);
		
				
		// do not delete!
    	parent::__construct();
    	
	}
	
	
	/*
	*  render_field_settings()
	*
	*  Create extra settings for your field. These are visible when editing a field
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field_settings( $field ) {
		
		/*
		*  acf_render_field_setting
		*
		*  This function will create a setting for your field. Simply pass the $field parameter and an array of field settings.
		*  The array of settings does not require a `value` or `prefix`; These settings are found from the $field array.
		*
		*  More than one setting can be added by copy/paste the above code.
		*  Please note that you must also have a matching $defaults value for the field name (key_name)
		*/
		
		acf_render_field_setting( $field, array(
			'label'			=> __('Access Key ID','acf-s3_file_upload'),
			'instructions'	=> __('Please enter the S3 Access Key ID for the bucket you wish to upload to.','acf-s3_file_upload'),
			'type'			=> 'text',
			'name'			=> 's3_api_access_key_id',
			'required'		=> true,
		));

		acf_render_field_setting( $field, array(
			'label'			=> __('Secret Access Key','acf-s3_file_upload'),
			'instructions'	=> __('Please enter the S3 Secret Access Key for the bucket you wish to upload to.','acf-s3_file_upload'),
			'type'			=> 'text',
			'name'			=> 's3_api_secret_access_key',
			'required'		=> true,
		));

		acf_render_field_setting( $field, array(
			'label'			=> __('Bucket Name','acf-s3_file_upload'),
			'instructions'	=> __('Please enter the bucket name you wish to upload to.','acf-s3_file_upload'),
			'type'			=> 'text',
			'name'			=> 's3_api_bucket_name',
			'required'		=> true,
		));

		acf_render_field_setting( $field, array(
			'label'			=> __('Bucket Location','acf-s3_file_upload'),
			'instructions'	=> __('Please enter the location relative to the bucket root that the files should be uploaded to (optional).','acf-s3_file_upload'),
			'type'			=> 'text',
			'name'			=> 's3_api_bucket_location',
			'required'		=> false,
		));

	}
	
	
	
	/*
	*  render_field()
	*
	*  Create the HTML interface for your field
	*
	*  @param	$field (array) the $field being rendered
	*
	*  @type	action
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$field (array) the $field being edited
	*  @return	n/a
	*/
	
	function render_field( $field ) {
		
		$has_value = ( is_array( $field['value'] ) && array_key_exists('original_filename', $field['value']) );

		?>

		<?php if ( $has_value ) { ?>
			<p>
				Current File: <strong><a href="<?php echo $field['value']['pre_signed_url']; ?>"><?php echo $field['value']['original_filename'];?></a> (<?php echo $field['value']['human_size']; ?>)</strong><br><br>
				Select another file to upload below to overwrite this one.
			</p>
		<?php } ?>

		<div class="acf-input-wrap">
			<input type="hidden" name="<?php echo esc_attr($field['name']); ?>" <?php if ( $has_value ) { ?> value="1" <?php } ?> />
			<input type="file" name="<?php echo esc_attr($field['key']); ?>_file-field" />
		</div>		

		<?php
	}
	
		
	/*
	*  input_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is created.
	*  Use this action to add CSS + JavaScript to assist your render_field() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/
	function input_admin_enqueue_scripts() {
		
		$dir = get_template_directory_uri() . '/core/inc/acf/s3-file-upload/';	
		
		// register & include JS
		wp_register_script( 'acf-input-s3_file_upload', "{$dir}js/input.js" );
		wp_enqueue_script('acf-input-s3_file_upload');
		
		
		// register & include CSS
		// wp_register_style( 'acf-input-s3_file_upload', "{$dir}css/input.css" ); 
		// wp_enqueue_style('acf-input-s3_file_upload');
	}
	
	
	
	
	/*
	*  input_admin_head()
	*
	*  This action is called in the admin_head action on the edit screen where your field is created.
	*  Use this action to add CSS and JavaScript to assist your render_field() action.
	*
	*  @type	action (admin_head)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/

	/*
		
	function input_admin_head() {
	
		
		
	}
	
	*/
	
	
	/*
   	*  input_form_data()
   	*
   	*  This function is called once on the 'input' page between the head and footer
   	*  There are 2 situations where ACF did not load during the 'acf/input_admin_enqueue_scripts' and 
   	*  'acf/input_admin_head' actions because ACF did not know it was going to be used. These situations are
   	*  seen on comments / user edit forms on the front end. This function will always be called, and includes
   	*  $args that related to the current screen such as $args['post_id']
   	*
   	*  @type	function
   	*  @date	6/03/2014
   	*  @since	5.0.0
   	*
   	*  @param	$args (array)
   	*  @return	n/a
   	*/
   	
   	/*
   	
   	function input_form_data( $args ) {
	   	
		
	
   	}
   	
   	*/
	
	
	/*
	*  input_admin_footer()
	*
	*  This action is called in the admin_footer action on the edit screen where your field is created.
	*  Use this action to add CSS and JavaScript to assist your render_field() action.
	*
	*  @type	action (admin_footer)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/

	/*
		
	function input_admin_footer() {
	
		
		
	}
	
	*/
	
	
	/*
	*  field_group_admin_enqueue_scripts()
	*
	*  This action is called in the admin_enqueue_scripts action on the edit screen where your field is edited.
	*  Use this action to add CSS + JavaScript to assist your render_field_options() action.
	*
	*  @type	action (admin_enqueue_scripts)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/

	/*
	
	function field_group_admin_enqueue_scripts() {
		
	}
	
	*/

	
	/*
	*  field_group_admin_head()
	*
	*  This action is called in the admin_head action on the edit screen where your field is edited.
	*  Use this action to add CSS and JavaScript to assist your render_field_options() action.
	*
	*  @type	action (admin_head)
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	n/a
	*  @return	n/a
	*/

	/*
	
	function field_group_admin_head() {
	
	}
	
	*/


	/*
	*  load_value()
	*
	*  This filter is applied to the $value after it is loaded from the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value found in the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*  @return	$value
	*/
	function load_value( $value, $post_id, $field ) {
		
		$has_value = ( is_array( $value ) && array_key_exists('original_filename', $value) );

		if ( $has_value ) {

			// we have a file, lets include the AWS SDK so that we can push it to our bucket
			require( get_template_directory() . '/core/inc/acf/s3-file-upload/inc/aws/aws-autoloader.php' );

			try {

				// initiliasea new S3 client
				$s3Client = \Aws\S3\S3Client::factory(array(
				    'credentials' => array(
				        'key'    => $field['s3_api_access_key_id'],
				        'secret' => $field['s3_api_secret_access_key'],
				    )
				));

				$value['pre_signed_url'] = $s3Client->getObjectUrl(
					$field['s3_api_bucket_name'], 
					$value['s3_file_key'], 
					'+60 minutes',
					array(
						'ResponseContentDisposition' => 'attachment; filename="' . $value['original_filename'] . '"'
					)
				);

			} catch ( S3Exception $e ) {

				// an error happened, clear the value in the DB.
				$value = null;
			}

			
		}

		return $value;
		
	}
	
	
	/*
	*  update_value()
	*
	*  This filter is applied to the $value before it is saved in the db
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value found in the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*  @return	$value
	*/	
	function update_value( $value, $post_id, $field ) {

		// the $value will default to '1' as this is what is set in the hidden field for JS validation to work. 
	
		$field_id = $field['key'];

		

		if ( strlen( $field_id ) > 0 ) {

			$uploaded_file = self::check_file_upload( $field_id . '_file-field' );	

			// we parsed the uploaded file, now pass it on to S3...
			if ( true === $uploaded_file['success'] ) {

				$value = self::put_object_s3( $uploaded_file, $field, array(
			        'CV_ID' => $post_id,
			        'Original_Filename' => $uploaded_file['file']['name'],
			    ) );

			} else {
				// no file selected - check db for existing values and revert to that
				$value = get_post_meta( $post_id, $field['name'], true );
			}

		}


		return $value;
		
	}
	
	
	/*
	*  format_value()
	*
	*  This filter is appied to the $value after it is loaded from the db and before it is returned to the template
	*
	*  @type	filter
	*  @since	3.6
	*  @date	23/01/13
	*
	*  @param	$value (mixed) the value which was loaded from the database
	*  @param	$post_id (mixed) the $post_id from which the value was loaded
	*  @param	$field (array) the field array holding all the field options
	*
	*  @return	$value (mixed) the modified value
	*/
		
	/*
	
	function format_value( $value, $post_id, $field ) {
		
		// bail early if no value
		if( empty($value) ) {
		
			return $value;
			
		}
		
		
		// apply setting
		if( $field['font_size'] > 12 ) { 
			
			// format the value
			// $value = 'something';
		
		}
		
		
		// return
		return $value;
	}
	
	*/
	
	
	/*
	*  validate_value()
	*
	*  This filter is used to perform validation on the value prior to saving.
	*  All values are validated regardless of the field's required setting. This allows you to validate and return
	*  messages to the user if the value is not correct
	*
	*  @type	filter
	*  @date	11/02/2014
	*  @since	5.0.0
	*
	*  @param	$valid (boolean) validation status based on the value and the field's required setting
	*  @param	$value (mixed) the $_POST value
	*  @param	$field (array) the field array holding all the field options
	*  @param	$input (string) the corresponding input name for $_POST value
	*  @return	$valid
	*/
	function validate_value( $valid, $value, $field, $input ){
		// return false;
		// // Basic usage
		// if( $value < $field['custom_minimum_setting'] )
		// {
		// 	$valid = false;
		// }
		
		
		// // Advanced usage
		// if( $value < $field['custom_minimum_setting'] )
		// {
		// 	$valid = __('The value is too little!','acf-s3_file_upload');
		// }
		
		
		// return
		return $valid;
		
	}
	
	
	/*
	*  delete_value()
	*
	*  This action is fired after a value has been deleted from the db.
	*  Please note that saving a blank value is treated as an update, not a delete
	*
	*  @type	action
	*  @date	6/03/2014
	*  @since	5.0.0
	*
	*  @param	$post_id (mixed) the $post_id from which the value was deleted
	*  @param	$key (string) the $meta_key which the value was deleted
	*  @return	n/a
	*/
	
	/*
	
	function delete_value( $post_id, $key ) {
		
		
		
	}
	
	*/
	
	
	/*
	*  load_field()
	*
	*  This filter is applied to the $field after it is loaded from the database
	*
	*  @type	filter
	*  @date	23/01/2013
	*  @since	3.6.0	
	*
	*  @param	$field (array) the field array holding all the field options
	*  @return	$field
	*/
	
	/*
	
	function load_field( $field ) {
		
		return $field;
		
	}	
	
	*/
	
	
	/*
	*  update_field()
	*
	*  This filter is applied to the $field before it is saved to the database
	*
	*  @type	filter
	*  @date	23/01/2013
	*  @since	3.6.0
	*
	*  @param	$field (array) the field array holding all the field options
	*  @return	$field
	*/
	
	/*
	
	function update_field( $field ) {
		
		return $field;
		
	}	
	
	*/
	
	
	/*
	*  delete_field()
	*
	*  This action is fired after a field is deleted from the database
	*
	*  @type	action
	*  @date	11/02/2014
	*  @since	5.0.0
	*
	*  @param	$field (array) the field array holding all the field options
	*  @return	n/a
	*/
	
	/*
	
	function delete_field( $field ) {
		
		
		
	}	
	
	*/

	/**
	 * Checks if a particular valid file has been uploaded. Doesn't actually save it, just ensures the file is safe.
	 * Returns array with three potential keys, success, file and message.
	 *
	 * @param array $uploaded_file an array of file properties return from check_file_upload
	 * @param array $field an array of field properties 
	 * @param array $meta_data an array of fadditional meta data to save against the file on S3
	 * @return mixed|array|null
	 */
	static function put_object_s3( $uploaded_file = array(), $field = array(), $meta_data = array() ){

		$value = null;

		if ( !is_array( $uploaded_file ) || empty( $uploaded_file ) ) {
			return $value;
		}



		// only proceed if we have a valid tmp file via $_FILES
		if ( true === $uploaded_file['success'] ) {

			// we have a file, lets include the AWS SDK so that we can push it to our bucket
			require( get_template_directory() . '/core/inc/acf/s3-file-upload/inc/aws/aws-autoloader.php' );
			
			// build up a unique filename but always persist file extension.
			$filename = substr( $uploaded_file['file']['name'], 0, ( strlen( $uploaded_file['file']['name'] ) ) - ( strlen( strrchr( $uploaded_file['file']['name'], '.' ) ) ) );
			$extension = self::get_file_extension( $uploaded_file['file']['name'] );
			$new_filename = $filename . '_' . sha1_file( $uploaded_file['file']['tmp_name'] ) . '.' . $extension;

			$s3_file_key = ( strlen( $field['s3_api_bucket_location'] ) > 0 ) ? $field['s3_api_bucket_location'] . $new_filename : $new_filename; 

			try {

				// initiliasea new S3 client
				$s3Client = \Aws\S3\S3Client::factory(array(
				    'credentials' => array(
				        'key'    => $field['s3_api_access_key_id'],
				        'secret' => $field['s3_api_secret_access_key'],
				    )
				));



				// Upload an object by streaming the contents of a file
				$result = $s3Client->putObject(array(
				    'Bucket'     => $field['s3_api_bucket_name'],
				    'Key'        => $s3_file_key,
				    'SourceFile' => $uploaded_file['file']['tmp_name'],
				    'Metadata'   => $meta_data,
				));

				if ( is_object( $result ) ) {

					$value = array(
						's3_file_key' => $s3_file_key,
						'original_filename' => $uploaded_file['file']['name'],
						'size' => $uploaded_file['file']['size'],
						'human_size' => self::friendly_filesize( $uploaded_file['file']['size'] ),
						'mime_type' => $uploaded_file['file']['type'],
						'extension' => $extension,
					);
				} else {
					$value = null;
				}


			} catch ( S3Exception $e ) {

				// an error happened, clear the value in the DB.
				$value = null;
			}
		}

		return $value;
	}

	/**
	 * Checks if a particular valid file has been uploaded. Doesn't actually save it, just ensures the file is safe.
	 * Returns array with three potential keys, success, file and message.
	 *
	 * @param string $field the file field name (corresponds to input 'name' field in html).
	 * @param int $size the max file size to be allowed. Defaults to 0. If not specified any size is allowed.
	 * @param array $allowed_mime_types (Optional) A specific set of mime types to allow. Otherwise allow any document.
	 * @return array
	 */
	public static function check_file_upload( $field = '', $size = 0, $allowed_mime_types = array() ){

		try {
    		
    		// Undefined | Multiple Files | $_FILES Corruption Attack
		    // If this request falls under any of them, treat it invalid.
		    if (
		        !isset($_FILES[$field]['error']) ||
		        is_array($_FILES[$field]['error'])
		    ) {
		        throw new RuntimeException('Invalid parameters.');
		    }

		    // Check $_FILES[$field]['error'] value.
		    switch ($_FILES[$field]['error']) {
		        case UPLOAD_ERR_OK:
		            break;
		        case UPLOAD_ERR_NO_FILE:
		            throw new RuntimeException('No file sent.');
		        case UPLOAD_ERR_INI_SIZE:
		        case UPLOAD_ERR_FORM_SIZE:
		            throw new RuntimeException('Exceeded filesize limit.');
		        default:
		            throw new RuntimeException('Unknown errors.');
		    }

		    // You should also check filesize here. 
		    if ( $size > 0 ) {

		    	if ($_FILES[$field]['size'] > $size) {
			        throw new RuntimeException('Exceeded filesize limit.');
			    }

		    }

		    // DO NOT TRUST $_FILES[$field]['mime'] VALUE !!
		    // Check MIME Type by yourself against allowed_mime_types array function parameter.
		    // array(
	        //     'jpg' => 'image/jpeg',
	        //     'png' => 'image/png',
	        //     'gif' => 'image/gif',
	        // )

		    if ( is_array( $allowed_mime_types ) && !empty( $allowed_mime_types ) ) {

		    	$finfo = new finfo(FILEINFO_MIME_TYPE);
		    
			    if (false === $ext = array_search(
			        $finfo->file($_FILES[$field]['tmp_name']),
			        $allowed_mime_types,
			        true
			    )) {
			        throw new RuntimeException('Invalid file format.');
			    }

		    }
		    
		    return array( 
		    	'file' => $_FILES[$field],
		    	'success' => true,
		    );

		} catch ( RuntimeException $e ) {

			return array( 
		    	'error' => $e->getMessage(),
		    	'success' => false,
		    );
		}

	}
	
	/**
	 * Return the extension for a given file name.
	 * 
	 * @param string $file_name
	 * @return string
	 */
	public static function get_file_extension( $file_name ) {
		return substr( strrchr( $file_name, '.' ), 1 );
	}	

	/**
	 * Return the human readable filesize for a given nuber of bytes.
	 * 
	 * @param int $bytes
	 * @param int $bytes
	 * @return string
	 */
	public static function friendly_filesize($bytes = 0, $dec_places = 2) 
	{
	    $size   = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
	    $factor = floor((strlen($bytes) - 1) / 3);

	    return sprintf("%.{$dec_places}f", $bytes / pow(1024, $factor)) . @$size[$factor];
	}
}

function include_field_types_s3_file_upload(){
	
	// create field
	new acf_field_s3_file_upload();
}





add_action('acf/include_field_types', 'include_field_types_s3_file_upload');	

?>
