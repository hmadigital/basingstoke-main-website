<?php

function get_pagination($query = false) {
	global $paged;
	global $wp_query;
	
	if (!$query) {
		$query = $wp_query;
	}
	
	// How many pages do we have?
	if ( !$max_page ) {
	    $max_page = $query->max_num_pages;
	}
	
	// We need the pagination only if there is more than 1 page
	if ( $max_page > 1 ) {
	    if ( !$paged ) $paged = 1;
	
	    echo '<div class="pagination"><ul>';
		
		
		echo ($paged > 1) ?  '<li class="prev"><a href="' . get_pagenum_link(($paged - 1)) .'">Prev</a></li>' : '<li class="prev"><span>Prev</span></li>';
		
        for ( $i = 1; $i <= $max_page; $i++ ) {
            echo ($i != $paged) ? '<li><a href="' . get_pagenum_link($i) .'">'.$i.'</a></li>' : '<li class="active"><span>'.$i.'</span></li>';
        }

		echo ($paged < $max_page) ?  '<li class="next"><a href="' . get_pagenum_link(($paged + 1)) .'">Next</a></li>' : '<li class="next"><span>Next</span></li>';

		
			
	    echo '</ul></div>';
	}
}

function get_custom_pagination($pageSize, $pageNumber, $total) {
	
	$pageCounter = 1;
	// How many pages do we have?
	while (($total - ($pageCounter * $pageSize)) > 0){
        $pageCounter++;
    }
	
	// We need the pagination only if there is more than 1 page
	if ( $pageCounter > 1 ) {
	    
	    echo '<div class="pagination"><ul>';
		
		
		echo ($pageNumber > 1) ?  '<li class="prev"><a href="' . get_pagenum_link(($pageNumber - 1)) .'">Prev</a></li>' : '<li class="prev"><span>Prev</span></li>';
		
        for ( $i = 1; $i <= $pageCounter; $i++ ) {
            echo ($i != $pageNumber) ? '<li><a href="' . get_pagenum_link($i) .'">'.$i.'</a></li>' : '<li class="active"><span>'.$i.'</span></li>';
        }

		echo ($pageNumber < $pageCounter) ?  '<li class="next"><a href="' . get_pagenum_link(($pageNumber + 1)) .'">Next</a></li>' : '<li class="next"><span>Next</span></li>';

		
			
	    echo '</ul></div>';
	}
}

?>