<?php 

class MandrillEmails {

	/**
	 * Constructor. Adds two filter hooks for Mandrill customisation.
	 */
    function __construct() {
    	add_filter( 'mandrill_payload', array( $this, 'filterEmail' ) );
    	add_filter( 'mandrill_nl2br', array( $this, 'filterNewLines' ), 10, 2 );
    }

    /**
	 * Whether to automatically replace new lines with <br> tags or not.
	 * @param bool $nl2br
	 *   Default nl2br value from plugin options.
	 * @param array $message
	 *   Associative array of message properties.
	 *
	 * @return bool
	 *   Boolean indicating whether to replace newlines with br tags.
	 */
	public function filterNewLines($nl2br, $message) {

		$textOnlyEmails = array(
			'wp_call_user_func_array',
			'wp_retrieve_password',
			'wp_wp_new_user_notification',
			'wp_wp_password_change_notificationView',
			'wp_forgotpasswordview->generate_reset_key',
			'wp_initiativepost->togglesignup',
			'wp_initiativepost->processtwitter',
			'wp_initiativepost->processbusiness',
		);

		foreach ($message['tags']['automatic'] as $tag) {

			if ( in_array( strtolower( $tag ), $textOnlyEmails ) ){
				$nl2br = true;
			}
		}

		return $nl2br;
	}

    /**
	 * Custom processing for mail messages.
	 * @param array $message
	 *   Associative array of message properties to be sent via Mandrills API.
	 *
	 * @return array
	 *   Modified associative array of message properties to be sent via Mandrills API.
	 */
	public function filterEmail($message) {


		// Detect when no template was set
		if(!isset($message['template'])) {
		    // Create the template parameter
		    $message['template'] = array();

		    // Set a template
		    $message['template']['name'] = 'Basingstoke Together';

		    // Fill in the main region with the normal content
		    $message['template']['content'][] = array(
		        'name' => 'main',
		        'content' => $message['html']
		    );

		    // Remove the normal content from its non-template location
		    unset($message['html']);        
		}

		/**
		 * Add base64 encoded blank gif and logo images.
		 */
		if(!isset($message['images'])) {

			// Create the template parameter
		    $message['images'] = array();

			$message['images'][] = array(
				'type' => 'image/gif',
				'name' => 'blank_1x1',
				'content' => 'R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7',
			);

			$message['images'][] = array(
				'type' => 'image/png',
				'name' => 'logo',
				'content' => ',iVBORw0KGgoAAAANSUhEUgAAAPsAAAA8CAYAAACgn8yqAAAaE0lEQVR4AeSdCXRcVRnHK5BJWsq+YN5MukCPzJtJQ0tUxEXroogiQktZUEBFEZKUsqAgKsQdIWkBRazCweWIWGQmpS7QVmJZFBUPiFSQxQVZJG2hpSKCWby/kwm8+fNm3n3LTBN859xz0pl779z33fv/9u920sv1Wbhw4bYNTm5uQ9o9rtHJndqYdk83//5IU6Z13s4z5uw86WX87DrrgB1Tmda2pubcm6a05F/T6LTtCz2Smr9p2uy9DT3fm8rkPmjoe3yq2T28IZ2fk8/nU+OdNvPmzdtuSstch7NB428+m/T/8jQ25w8yG/azBsf9dUD7lWk3pxz3R6nm7KdSzuxs3N82c33VzPlkQ3N2xLTN5u9vjIyMvCLOQTRzXMZczFmhDfG+jensId3d3dtUmuu427bsuWD1+r75qwfWL1g9cN3xa57eza9fb9eKQ3s7Cg/1dhY29nQW1i49rTgjytrN+EvMPI/0dhb7Ll68Yi/bcdCLd2l03G+nnOwjvu/sZP/NHpo+HY7TPiXs2ian2zJmry407bGKdHXc5zgbTen8W/3mmNLS+mpD9+9yjpJo5reuacpk3xK49sx+aUOfM8wa+xua3f/quvnMtLVmvk82zZgTau9S6fwR5p3WGTw81NjsnhaG6TQ053rNuL+aPbthspOfpn2aWtw3mzX9hH2L1rK3mXYJ7z+6AdPdZvPBs7x0pOZkb2ITIwL9aL85G9O5j0diWk5uEYc6zPoNoe/2IzTPgtUbTpm/amBkrBngf8yvHwDt6SiMvNiK9xnAh9IeLl3Ul/POYQB/itU7Z/LvNnS8M9yeuRtSTu5ztpLY7MfJBgxbwtHV/T0MwjsPgOK7JBtAq7TuHTP5Xc3+XhTqfDvu8wiL7We2WjFbM/8fPGOfyWQOnBw0pr29vcGMWyH0Knj77LJ3+05m3ZsSoZHjXvDCYYk7GQQyEx4WEpts/i8rAPCusHOhaVRhSP+hVSHGY4bzv0rnPGLN+p4ysK8ZuED7LO9elwKc2nq7ileHlOqHlI3vKFxUrT8aiaHfkpj7tnqPfH5qwGE+L+r8aBmYEGNzIW0SB7vjPuB7ttL5/cx3D8eY+4nJ6eyBQfumTLBpetvMoH1DI1GtC5PL24/fTopGSPeSNMwfWkY81Ipm94svaensl8ygS81Cl/uryO6/hJMHSaRZAYtst1YxM/nXopqrxMamnDpr7h5jqu7Ufdr2hCmZtlL7o15GAftV3f1NfmCnLenqO7YWYEcFNO93vT/d3H+izhuVcrHpcwxqO+o39KgA+JsrmU1GHX+bz8HZhKTAF7D9jPwrd5o2exf8A8b+/TBz+Uje67zzmT6rzGd/8zbO3FjzYxj08WujJmXu/S9Zt+O+g/Po867PIEGNBngWvpyGdPaEUXPUvRGB5SckwEeSYDf7cYUKysaM+x4/FV7WMwgDCtMMjR7nnMBw/cDOj68JOpioGNgbShw+s7d1cl+WQypS3r3cei4flUhtcX3gpBwuz5gfJgx2VPGnes/oa0ka7D4SHVD8A+ZW7b1xoiHNdWwluxe7WOh6/w5O++7aTxmEV6ICyFDanqwNEyuCz+ZJAe2zCKzd933DDpXGYc6adV9p+g/LGp7GNxUb7P77NoQpy3cBYIdp/imObyw82MUphMPD1n5ST7k5nI96CQqx4byezzbbOJJmzJjXhGPIu7F7tbVtb7OOkqR/kN9COiUCdm2dxf6R7pFtkgK77hcN9XiHdHY3+z13OznUCnY9/D6/s7/N/DAEs7/3lQ7zYfUCO+dK/Rf4J1CJQwiOYzhPeq7RpmKAnXnP9/FNncx34xXsMjbbJRx0vc04PMfecaido3a3+33v54R1Audqad1H1nBbKEkw051O4+8EwI40/50B7l1l6nxn4RNJgB2pzcETaftn7O7wPo7cuYxHzfM7yAZkb5RzsTFMlASQTk67r+PveoEd1VxV5MmZ3AERaPMBXQdmSlSw46X30cTO4buJAnZU8YVy8B609MIXvePgvGIjWquAONbigF2f+GAv3Lqk8/q8+fvZF6V74bneRSv3iwl26HaUHJph9W2EeVQ9VXVczYT4FE0e7OL4ekDWfF7Udajg4WzD7MKCndwONQ3wofDdBAO7+xlZ0E+DxhDSgON6NuRer2mA48U7J2AOSiARYg6hbm5NsI+Ct+9U+fwe+sYEe0EOTbFWoMOpo8CDKY9XsOOkVUciZ0P7hXQgD6kZYwN2EYZDflrshAI7hFTvqVHr32URCz9LXr5MxSX+G5YLmjXfIodkM7YX8cytBXYYV29H8cYydb6jeHFUsKNqaxSEZKhaAs/s71+Urni/ebdxBnYEz2flXF0Rdy1gQc7i2bZgb8zkDlYPP5EsNJAJBXbUP8M5fyEbs8zSAXKvN3OJ8I33e4iFpPaGkoLSGVOZ/IIK8cX1cFK81ByaeoKdZ+lpNzQbB92GFwFcHO7pXPHOKGBHXddsNU3gSPohzdiPrmSLEYolxIUzdDyAXYEJs9c+cXM38G/YgB2ganIXWXKSxLT1wU7YhCC8XyMWXcqCGvR6O8lcs3PatL5eCLDCr5+Pl/9wi0PyTYskj3sJf3BI4bC1BjvP0kWF+eKdf3TJ6TfsGhbs5KLL+9xRa5UaCa5hzSqppmenWtzWrQV2wFC219Pyubhr8Uk4+20Q2I3qfiTRJfUjSWQpNNihM8lfwS37aCn1ff9AsIdpOC3CeFyJY8p437AMXnhbX4AczHOw1WzTLNEIGJc82BXIxSvLAV+4NrRkT+c/JNL154H0zsyeTXzZ0PPiSo100lQmO796aqd7oXUasuPejnNvK0j2Dd5xJFMlwHjaVaMJAjuhXx0jhVehwB4Dm1cnB3aRMp60SN+H8JAQ5olK6jmqoXDHwVIyf+ADYY1d2W2bKkmMmnh7LcF+WUf/VCPRH5Rw3Akh1fiT5BD9OBDs1YpWbEJL4lglaYp9s03Fha51A7tIU/Li44Kd3AvVeoPATuKRZr4h1OoNdph8ENjpdA/xSt/muCeirqEyqwTl38Y+xh71fRgr/XvCaAF4/sMSDLWSSibSIXVTVMqjZtUK7DxLOwsHGgAPeoplnv5a1/UzbcGOFhLWtxJQ+ad2+LW2qj2psjBUQqMBNQf3k05bD7AjQct+e3qrGxfsaCha3BMs2XNzOU8q7QFvHLCT5wBmghqaGhV+ZLnaOOhW2ywG6UuOtGzOAF56m9RLODEOtEpNiYhnOI4XGC2CFFmz5u/4qaSoqsmDXUthi93ad/nykW1twE6Wm/ofbGxONADy0f2a11uMnReFrrNmHdyIP4EwoF/5KPHqOqnxt3vHwRzjgp0MNzWdgsDOekvlwA+rMERTqL+DLgbY9cGe9o6H42sfuCzfxW1N6dzbk7p8AQ1G0yrxltYS7P3d/dsZdf72cvu971wbsCMhJWY7HFdVRSVXsMd58N8wpzqWMAHqAPbLAqJDoR9CZUGqsZoPZGOOnXnOlFZX4q2fsGDH1tP6du2DahEf7FqsEu8BPGqKwHlrCXaeJR0rZpk+Wzygfr7n1GK7ZVLNnUKPo2LGkZ9KAOw65/4++3Z0rcFO/F+ZN6ZZrPMhUhsABkl2wO5lftR7aEkuvowJCXYfu+Yx9ebC7ZVDYsOb9lEcT1xWYVTBUyjHNK2TUB4xXkpqteQwljQLqJYjJFcTsCvgu4on6mUXSzqLR1qA/QLbdOKwacZxwS7r/Lt3bq4EqzXYMR+1rBXbNcbZOF9tZs5yGLCPJdeg3ajtTwXehAO7hslIghE1/3CtTw64D00ZxYB3PExA++DMkwoiqwctRNNB6wF2HpNgU1TAB4CdPIUWDk6YHATbcksFO6ErTDTu7As7t6qvMPF6FMLgd9H69SiOOpxsJC3ZOIgV7H7XWpHUpfnxJKVhNk4ksEPgvmqJB9yh5f2ezKuQgOzRSym83zOfJ8RhrdbiYBRv9RAHvF5g7zlz5e4m3v64d6xNiSuJTeoUxQcRYc8HfcEuvhhMHa4dCxMBUcByOUM9wM7+YUNrLkgY+qSm5fI411SqozlEBTsPWgbf+6fPjnOww5XIa/eO1Uojbu7UQ0WBQcDUgc49ih7Gvte0XRw1ANmiQkqZVH/y3vgAwHf1HUQKbRiwU7eul0qW7gZot80Kk8QPX7BzwL2puUZzOhONLLhewv2jaHpbuHOg1mDXSkzVNslsC6qFx5zEj6GVhQYj76NPFLDr5ar00fNad7BLnP14ruzxtlIG10nErc0Cv054zC8XnaQWb820EH1t8IqswnbLKnJMzy21EAzVl7AbDABGgxmAk0Q3FBW+XmCX7LpLw4CdB9XaJ749RP4DUtjPHCJ0J/njA+qgE8n+PaVrKRXzC8TZ8bATCiWeW5KGn1eTq6T+fjpMem4csCuotOHg5ExCC5LAcKCR8IL26C36kXc+P8lrqcx8V9n8hoIdjYVciNDNyX0F/4zmXMdqSAucdd6NQ4Xy9oFpRAT7ifrigPcFjuy43wpY33C176nEY666gV3HdxbXMVZCclWr/Tiw3miCSjLTfoPWU0o2GVRHJ3UKOIoE7Hq76U1x6ApzEbu01pJdqyuHYpzpQbkaOgbY9f5Ad2XArTVyeUjUJvkB2DORiCK569xvppwpqVAIwFanD5qG3ryi3tjAhnrn5CteCjl/1fozy8C+amCx9lm27I4Gz0UVgL0Q9v2WdhTncMFFGdi7iottas5Jjgm58Q+NhZDIn1awa8IMpaLiFLRpwzhNo+w3e+KdS6oiQz3kZXDVc4TzfI9tTodXSyRJyaaGnmpFw0hvVQbs9dDz3syXBNjRjMsyhZDCAMqibUTlAeCUAKIiVLqYn2qfEvEel1s6Qz84ecburcMu9LtlBWmEmu4po/VrQ+TyE94L2piFazfNNHfFrxu9M37g7qPXbmzxtb07+i43YB2inJV4eZT3I/RmVPrNAB1JL5VxNnfHX1MlPXaYveDGWRin97plVG/fwgnZS9R09jFIuzO0/wE+lRhhu7Nh2mMOrGQq99yjCLNWS5kuMbR+TFh8OmHKgJkXsFJYFEaA8X74Q6ArV2FpH3wInHkyPqM2TBe090n1eIiLQ/CkSi5ts7Hox+UObB6JP/wvJdiaUeqvj71lU2B+N+BEysd9v8vPunHPqPRCZcZRx+ai+fC/laAOcnlktfBmmIIVHFA4rKApDJOwGn/j80iqvh6HnuR2J/JAH+6kw04HSGR7juZ3/I+9OyQAAABhIBiILPSvg8TP7q7A7LvNZntfQGm1Vl1PAQAAAAAA1865BjdxXXF8JgQID5IO8KHD9NGETkomTB9poGn5kKZlApNpmSaNcB4SULJ3ZQySDLap9yG60AmBpsGySXi0ATOEDI2xDIR0mj4S0saWtH6YMqCG1lCglJCQgJG0Kxkb+/ZcmNp715K8q3UsJeyd+Y/G0r2rx/i355x7zzk3+2iVDo0/Wv7HCVZk5igtznIzFEZ4MoZ4X4zlJfKosPxT5Plh7RwrSbcmBeaBlN/tVHjEEcHfpaqIFiQl5gumrxeVxuDQynGmhQefaWNcNwrj8gnWVDM23efcdWzx9IMnlk7K9Xeriz4zeXd0UdasPilaN2ZTqG6cWdXB9850TU9Hx1hH6Ny4dCKvSRjneoRnjzZf/dcjnvr3Ip592Kpkb/37sjf4aEZQVkqT44hfH2O4UyCcUYg7DfM2xJevn5Lr91L9zLcVgfkdwH1F4RmcSarAtJMbAd7Ojs4K+aGyqT0N3nB30INzEayN9TR41vR/vs7ydcrl8rhyuQxbEVynV71c3oix1F+Es6PN+QYI72xzpWqPOE03gdzZtsi5o83Vff0a7a5XB0Heun1qZTgQrgxV4VzEhQOxynA1lSf/VEvLXY5G+bijMYKH0sKmyPmFjfLrRSF5kevoUaP5JPaIeOufo6C1rPq0zSESrLAixvCdBGbDQnwsxgil5iz5ysmqgOpUnukjMBuVyqNzST/7nUzX7Ql6/bmBTgEfJ94PsfIAqGIVdAr6WMX18traI0s+t7Pd2UdApWA1MXa0Ow/+fz2Bvvb0EqqarzJS7SfQWlRc6wkCvNsIyOYlX3A0NS+xSTYwiCUeTtibffVrKfjY7aPBSu/IAnUcLPlZeFQyzYH1tViShiw0uSqW3AOgn8wA9EVixVWeCcGc/4J6BwPPKF1r3Gk7B3cHvdutw+7pLy9WOsvfGTbQO8uuqSp/PXtwb/vSaQRSDeyvm7fsrre019jT4aHSrblIYJtV2Llw1TtUynaT/IZZ0Glr31xm02xgtJbVzwBInRFv0KVXs29flc5VP5RuHpFc2vBdfdweZ7idaQA+lWD45cligeqRn2LEO+NIKIc1FwYDL/wmq9surZgGwH5Au+ioB7SVuPT6+YpU8vmkgFaBRT9LQS+gj3CNZ+xQsBNLf22/12VUPQd88/DhAVebxNpK7OfzlM4yVzoBxMe1QCc7Kzzp5iWvVBQpSkV/EU1eYAdLz0WqXUZVGaqeL52uvS0b7EVNLTMdjS1f06ooEnkAXHcHzH0B5iR0wF97srn5XgsY2KPFF1yghZ24/UbXxhGH0ljpTdij31SiBy6RJsLa3YPWssLiTJtwAHWT3i1X/O5vDOn2b1w9Cdbu0K5NicydQ8GOg6X3fZK/O1j+P1PWm1juLCOfsPPhFy3/FoMsezSa1ZNbJMtTYM0Jyro3yrvygIgNO2Y33KGP0cGa/8LMe8dY/kUKdsR9iFlp/CCrzrOM3mVPCiVfNPNeAHztDcvO0I1HbNgLEnYyipqaZ+nj9zwgYsMOoEq0Zeb/YrbKjcT7McS36dx5qpMq2UUHK36atuqs6VJHsiegiuxPiAtf0LDbsFPVk46mSHwgbpd7yXM2tSMNO2y6aSFV3cL9Of3Tu8X5One+Vfs62VDTxenvWv3OhQ+7DbtmF/+f2nUSxrfY1I4g7IkS8Z5BgFoYYN3/rYn5e8l5fT8UAnpBCzs5M785YLdhZ1tbR4M17+5f0yR/bBM70rAjoVgXaz9v5TPEkfAydfNwi48MxNpMqxZ2sst+c8Buw+4IySt0MXuDTewIwx5j+HWDdtEtjATiPdT1GO6Zfth55rzm2OxD8txIwA5n5mpP0JPILu9FWPcqiSPzAzuo3XUJ4H3XjGDdFTOwc+GAyoUCiSF0EebRv0WOsDswHgWxuguUpNY0yT+wiR1p2BG/lYLTLfzYkpVDwtO64ztuAHbUPWDZUXTIs3iBqQb9lRzVpRNc4224VsXwJtX4ZuUNdksa/qQaIVQz2wjssNNesjAUWQ7JMl54bRW466shTfaXYL1fBsg7yBwa9EitTWt+LPsWGk7hUUsxu5t30p4CX5YWdoH5R1bYRWad0fTZ1Br2oeGAHVJkP8KHyHGhDTtY9o8hr358dtjNaWFjJAWbdDyJ321a82LZhbVaOEkMbwl2Riilc+YFlyZmv2DUjVdE98+Mwp4U2BVZYd/vWdZzYOXcrDpYOodUyeU1Zm9zngIFzMn1H3NufNWyynDN3GziwpvnkCq57DG7Kcgvg6V/paipdbpNaT436BiB1cXYOy1t0DH8K/TNg3tQA3u7FlL8rC9rV94ukZ2eFNBPk4K7SCtFYBdTsIvI9xnZoDvwadygA5B/Da77s/D4ki4u7ysKNz9WIOfpNuxxJN2t240/h0mP8hwGWRdH/PsDNw4+Bc/dNgA72qyzyI7c3sdze+HAbsP+dCTS/57w93O6XfeLT4SPfMWms1CSahj+DJ0qyz2e4078Ql0m3p+0rydF5nFdffofcqp/F92PFCbsNuwkUWaw5ZePLW08MakAELFhB1db1O2g/4sUuJiytqs3ToK1HdRNw809Rs3ZtHIcqVTTlKn2Jf3Fc0zDJaA9NuzpYScdb/IJOxnkb9hxf0/n6v++ADLmbNixR7odrPAlHfB1pELNaF58nOGDdOzPHyWx2lC77AB8B16/fIpxq45+RMphCxd2G3YyimT5bnDhOyngQ/Imm9K8wk4fmVFC/Jv6Onb9SLHSl+KIe0sX93epyD8rY5kqz5zS1aUf75KW3TUkVKJ7PoCukjWFCbsNOzUv1DyfFLxQZ/KNMmOTmjfYqYKYzXrgQQpAX5Ng+YfirDSVVJyRG4CC+Hlg/beRTTh6Pp01l24kheLZqsBc1cXvqaTI/krl2fv0jStuxOjobc0ZfSfpVGMU9mtB35ae/T7RsBq8q3CDd+ZnEXYuXL2FCwdE46paxbdUz8wFdjLAulfo4vfuJ8It388XIzbs1G66sJEAm4sI+KTrrJH3Sq1x/xBAjWfoMZcE+M+ka0BJXPgb1XMomgn2aw2ebZbbUgW9Cj4sTfy0w86HA1st96ALVylS9KWJucBOBgC+R5dBd8kRavuqTayF0ezd9z0a9uC63HfV+TPmQOcOq8X+b5l5n6tS8UxS5mqm0aQisg+TtQD7a5lgB+tcNgw96PrwwdXTDMF+uXyvFnYcl6YaWbf7qGsCaRI5ALvzt+Zhd742kFvvTLRiuvMusczWYQ/0Se2bpw3AG9mrPUsnbaOzF8GcGwfzQjTw8t9sYi0MXIdHyZ76N2Xvvl6A/XyLt2GmhR7uYxIs5ybHZyQGz2DJPyBJOAoS51r53KQRBYC8D8BPZAD9GLwm4g3sHf03Cj+6lzxPdveTopve9YeU1+4G7y7QSQD3jFnBuihx541+/q4r5XOhA+1ZaCbZB+Cb6hBb2+aqggKYLrDQJ3cdWTzHNOx/d80DyM8R0MEzWJumlfR4AH4XFwqc5EJVZ0wrHIiCRMpSh1vmgrWOkV5y8Fhj5HOSozeYH4R0WYW0obZd+eGCXjp867Ber/z5CQriv6mw4sNxJC6AmH12arnw5eHOjCKdZ65K7IyUHz1I4nRVcN+v8iy4xIU/brSdrhuVy9o6WGf9vaVbRvr7koo2q9f5HxLI/BeGqgoDAAAAAElFTkSuQmCC',
			);

		}


		// $message['template']['content'][] = array(
		//     'name' => 'header',
		//     'content' => '<h2>' . $message['subject'] . '</h2>'
		// );

    	return $message;
	}

}

new MandrillEmails();