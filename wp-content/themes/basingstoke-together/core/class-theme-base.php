<?php

/**
 * 'Base' theme implementation, not to be instantiated directly. .
 */
abstract class BaseTheme {

	/**
	 * Menu locations available to the theme. Slug/Label structure.
	 */
	public $menus = array(
		'primary' => 'Primary Menu',
	);

	/**
	 * Image Sizes to be used throughout the theme.
	 *
	 * Associative array of slug, width, height, and whether to force cropping.
	 */
	public $imageSizes = array(
		array(
			'slug' => 'example_image',
			'width' => 500,
			'height' => 250,
			'crop' => true,
		)
	);

	/**
	 * Constructor. Automatically calls init method.
	 */
    public function __construct() {
    	$this->init();
    }

    /**
	 * Shared constants get defined here.
	 */
    public function constants(){
    	define('RECAPTCHA_PUBLIC_KEY', '6LdTab8SAAAAANQZIUXl62bFkhJ8Kb0hUO6OO7Pr');
		define('RECAPTCHA_PRIVATE_KEY', '6LdTab8SAAAAAEPJ5o7WyqM9UCs6h_MZDbswFURi');
    }

    /**
	 * Automatically called by contructor.
	 *
	 * Responsible for ALL hook/function setup for the theme. Overide the method to add extra hooks/filters sepecific to your theme.
	 */
    protected function init() {

    	/**
		 * Declare theme-related constants first.
		 */
    	$this->constants();

    	/**
		 * Add all of our action hooks. Ensure callback functions are public to prevent scope errors.
		 */
    	add_action( 'after_setup_theme', array( $this, 'setup_theme' ) );
    	add_action( 'wp_enqueue_scripts', array( $this, 'theme_scripts_styles' ) );
    	add_action( 'login_head', array( $this, 'login_css' ) );
		add_action( 'admin_head', array( $this, 'admin_css' ) );
		add_action( 'admin_init', array( $this, 'admin_font_icons' ) );
		add_action( 'acf/register_fields', array( $this, 'register_acf_fields' ) );
		add_action( 'admin_bar_init', array( $this, 'admin_bar_init' ) );
		remove_action('wp_head', 'wp_generator');

		/**
		 * Add all of our filter hooks. Ensure callback functions are public to prevent scope errors.
		 */
		add_filter( 'timber_context', array( $this, 'setup_timber_context' ) );
		add_filter( 'body_class', array( $this, 'body_class_names' ) );
		add_filter( 'wp_headers', array( $this, 'ie_headers' ) );
		add_filter( 'post_thumbnail_html', array( $this, 'remove_width_attribute' ) );
		add_filter( 'image_send_to_editor', array( $this, 'remove_width_attribute' ) );

		/**
		* Ensure auto-update emails don't get sent to admin email address.
		*/
		add_filter( 'auto_core_update_send_email', '__return_false', 1 );

		/**
		 * Process custom image sizes.
		 */
		if ( function_exists( 'add_image_size' ) ) {

			foreach ($this->imageSizes as $imageSize) {
				/**
				 * Check we do actually have, at least, slug, width and height.
				 */
				if ( isset( $imageSize['slug'] ) && isset( $imageSize['width'] ) && isset( $imageSize['height'] ) ) {

					$crop = isset( $imageSize['crop'] ) ? $imageSize['crop'] : false;

					add_image_size( $imageSize['slug'], $imageSize['width'], $imageSize['height'], $crop );
				}
			}
		}

		$this->additional_hooks();
    }

    /**
	 * Removes any width or height attributes found in a given string.
	 * @param string $html
	 *   HTML markup to check.
	 *
	 * @return string
	 *   Resulting html string devoid of width/height attributes.
	 */
	public function remove_width_attribute( $html ) {
		$html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
		return $html;
	}

    /**
	 * Add specific headers to be sent by the web server.
	 * @param array $headers
	 *   Associative array of headers.
	 *
	 * @return array
	 *   Modified associative array of headers.
	 */
	public function ie_headers($headers) {

		if (isset($_SERVER['HTTP_USER_AGENT']) && (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE') !== false)) {
                $headers['X-UA-Compatible'] = 'IE=edge,chrome=1';
        }

        return $headers;
	}

    /**
	 * Common setup of Timber context. Add sitewide variables (e.g. menus) here to avoid repetition.
	 * @param array $data
	 *   Associative array of context data.
	 *
	 * @return array
	 *   Associative array of context data.
	 *
	 */
	public function setup_timber_context($data){

	    $data['menu'] = array();

	    foreach ($this->menus as $slug => $label) {
	    	$data['menu'][$slug] = new TimberMenu($slug);
	    }

	    return $data;
	}

    /**
	 * Theme setup hooks. Associated with 'after_setup_theme' action.
	 */
	public function setup_theme() {

		add_editor_style('css/editor-style.css');

		add_theme_support( 'automatic-feed-links' );

		/**
		 * Ensure each menu is registered properly!
		 */
		foreach ($this->menus as $slug => $label) {
			register_nav_menu( $slug, $label );
		}

		add_theme_support( 'post-thumbnails' );

		/**
		 * Unlimited height, soft crop
		 */
		set_post_thumbnail_size( 624, 9999 );

		if (!current_user_can('administrator') && !is_admin()) {
		  show_admin_bar(false);
		}
	}

	/**
	 * Theme CSS/JS include logic.
	 */
	public function theme_scripts_styles() {

		/**
		 * Only include commenting JS if neccessary.
		 */
		//if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			//wp_enqueue_script( 'comment-reply' );
		//}

		wp_enqueue_style( 'theme-style', get_stylesheet_uri() );
	}

	/**
	 * Custom login CSS.
	 */
	public function login_css() {
	  echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/css/login.css' . '">';
	}

	/**
	 * Custom admin CSS.
	 */
	public function admin_css() {
	  echo '<link rel="stylesheet" type="text/css" href="' . get_template_directory_uri() . '/css/admin.css' . '">';
	}

	/**
	 * Custom admin font icons.
	 */
	public function admin_font_icons() {
	  wp_enqueue_style('fontawesome', '//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.css', '', '4.1.0', 'all');
	}




	/**
	 * Register custom ACF fields.
	 */
	public function register_acf_fields()
	{
		include_once(get_template_directory() . '/core/inc/acf/repeater/repeater.php');
		include_once(get_template_directory() . '/core/inc/acf/range/range-v4.php');
		include_once(get_template_directory() . '/core/inc/acf/opening-hours/acf-opening_hours.php');
	}

	/**
	 * Add specific CSS classes to page body by filter.
	 * @param array $classes
	 *   Associative array of css classes as strings.
	 *
	 * @return array
	 *   Modified associative array of css classes as strings.
	 */
	public function body_class_names($classes) {

		/**
		 * Add 'iframe' class if exisits in querystring.
		 */
		if (isset ($_GET['iframe']) && $_GET['iframe'] === 'true'){
			$classes[] = 'iframe';
		}

		return $classes;
	}

	/**
	 * Remove admin bar inline CSS.
	 */
	public function admin_bar_init() {
	    //remove_action('wp_head', '_admin_bar_bump_cb');
	}

}