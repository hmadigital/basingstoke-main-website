<?php

/**
 * 'Base' Timber view class, encapsulating shared view functionality/Twig rendering.
 *
 * Can be extended to include custom data, just override the 'post_process' method.
 */
class TimberView {

	protected $context;
	protected $expiration;
	protected $templates;

	/**
	 * Constructor.
	 * @param array $tpls
	 *   Associative array of twig template names (Required).
	 *
	 * @param array $additionalParams
	 *   Associative array of additional context data. Defaults to an empty array.
	 *
	 * @param bool $isPage
	 *   Boolean indicating whether the current view represents a 'page'. Defaults to true.
	 */
	function __construct( $tpls = array(), $additionalParams = array(), $expiration = false ) {

		/**
		 * Check that we were actually provided with at least one twig template name.
		 */
		if ( empty($tpls) ) {
			echo 'No twig template specified.';
			die();
		}

		/**
		 * Ensure that the Timber Plugin is available otherwise we cannot continue.
		 */
		if (!class_exists('Timber')){
			echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
			die();
		}

		/* AJAX check  */
		if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {

			$ajaxTpls = array();

			foreach ($tpls as $tpl) {
				$ajaxTpls[] = 'ajax/' . $tpl;
				$ajaxTpls[] = $tpl;
			}

			$tpls = $ajaxTpls;
		}

		/**
		 * Ensure we have a reference to the provided twig tamplates.
		 */
		$this->templates = $tpls;

		$this->expiration = $expiration;

		/**
		 * Return an object with all of our common data, e.g. site details, theme etc.
		 */
		$this->context = Timber::get_context();

		/**
		 * Get current user details.
		 */
		$user = array(
			'id' => 0,
			'logged_in' => false,
		);

		if ( is_user_logged_in() ) {

			$current_user = wp_get_current_user();

			$user = array(
				'id' => $current_user->ID,
				'logged_in' => true,
				'username' => $current_user->data->user_login,
				'display_name' => $current_user->data->display_name,
				'email' => $current_user->data->user_email,
				'roles' => $current_user->roles,
				'is_admin' => in_array( 'administrator', $current_user->roles ),
			);
		}

		$this->context['user'] = $user;

		/**
		 * Populate any querystring data to access in Twig tls. Escape unsafe chars by default.
		 */
		$qsData = $this->sanitize_array( $_GET );

		$this->context['querystring'] = $qsData;

		/**
		 * Populate any form data to access in Twig tls. Escape unsafe chars by default.
		 */
		$formData = $this->sanitize_array( $_POST );

		$this->context['form_data'] = $formData;
		$this->context['form_errors'] = array();
		/**
		 * Always 'get' our current posts. This is the equivalant of the standard loop in PHP templates.
		 */
		$this->context['posts'] = Timber::get_posts();

		/**
		 * If additional data was provided lets merge it into the context too.
		 */
		if (!empty( $additionalParams )) {
			foreach ($additionalParams as $key => $value) {
				$this->context[$key] = $value;
			}
		}

		/**
		 * If single post is true (e.g. for a page), lets create a convenience 'page' object in the context.
		 */
		if (count( $this->context['posts'] ) > 0) {

			/**
			 * The 'page' is the first post in the result.
			 */
			$this->context['page'] = $this->context['posts'][0];

			/**
			 * Generate breadcrumb data (if applicable).
			 */
			$breadcrumbItems = $this->get_breadcrumb_data();

			/**
			 * If we did get breadcrumb data back ensure it is available via the context.
			 */
			if ( !(empty( $breadcrumbItems )) ) {
				$this->context['breadcrumbs'] = array_reverse( $breadcrumbItems );
			}
		}

		$this->context['ajax_url'] = admin_url('admin-ajax.php');

		/**
		 * Call post_process to add any custom data required for a custom template. Only used when overriden in an extended class.
		 */
		$this->post_process();
	}

	/**
	 * Generates breadcrumb associative array if the current page has parent items.
	 *
	 * @return array
	 *   An associative array containing numerous breadcrumb items. Each item has an 'id', 'title' and 'link' property.
	 */
	public function get_breadcrumb_data(){

		$breadcrumbItems = array();

		/**
		 * Get a neater reference to the current page.
		 */
		$page = $this->context['page'];

		/**
		 * Check if the current page has a parent. If so we know we need breadcrumb data!
		 */
		if ( isset( $this->context['page']->post_parent ) && $this->context['page']->post_parent > 0){

			/**
			 * Get an array of parent page IDs to use in our custom query.
			 */
			$parentIDs = get_post_ancestors($this->context['page']->ID);


			if ( isset($parentIDs) && !empty($parentIDs) ) {

				/**
				 * If we got some parent IDs lets query for those posts only.
				 */
				$parents = new WP_Query( array( 'post_type' => 'page', 'post__in' => $parentIDs ) );

				/**
				 * Check we got some results!
				 */
				if ( $parents->post_count > 0) {

					/**
					 * Always include the current page first.
					 */
					$breadcrumbItems[] = array(
						'id' => $page->ID,
						'title' => $page->title(),
						'link' => get_permalink($page->ID),
					);

					/**
					 * Iterate over query results, adding a breadcrumb 'item' for each one.
					 */
					foreach ($parents->posts as $parent) {
						$breadcrumbItems[] = array(
							'id' => $parent->ID,
							'title' => $parent->post_title,
							'link' => get_permalink($parent->ID),
						);
					}
				}
			}
		}

		return $breadcrumbItems;
	}

	/**
	 * Override this method in an extended class to add additional custom data to the context, or modify the context itself.
	 */
	public function post_process(){}

	/**
	 * Convenience debug method for viewing context contents.
	 */
	public function debug(){
		var_dump($this->context);
		die();
	}

	/**
	 * Renders the current view with it's associated context data.
	 */
	public function render(){

		Timber::render($this->templates, $this->context, $this->expiration);
	}


	/**
	 * Helper function to sanitize an array (used for $_GET and $_POST sanitization)
	 */

	private function sanitize_array( $data ) {

		if ( !is_array( $data ) ) {

			$data = sanitize_text_field( $data );

		} else {

			foreach ( $data as $key => $value ) {

				if ( !is_array( $value ) && !is_object( $value ) ) {

					$data[$key] = sanitize_text_field( $value );

				}

				if ( is_array($value) ) {

					$data[$key] = $this->sanitize_array( $value );

				}

			}

		}

		return $data;

	}
}
