<?php
require( get_template_directory() . '/vendor/autoload.php' );
/**
 * Wordpress Starter Pack functions and definitions.
 *
 * Sets up the theme and provides some helper functions, which are used
 * in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 */

define('RECAPTCHA_SECRET', '6LdvRBYUAAAAAEZTevWqLtRlQD2qHBvrgpD2jBnl');

// setup ACF key
add_filter( 'acf/fields/google_map/api', function ($api) {

    $api['key'] = 'AIzaSyB4L4QybNh3Di47HgGU9hMuV9Wccd182Bc';
    return $api;

} );

/**
 * Include TimberView class so that we can use it any page/template without additional includes.
 */
require( get_template_directory() . '/core/class-timber-view.php' );

/**
 * Custom logout url route.
 */
Timber::add_route('basingstoke-together/logout/', function($params){
    wp_logout();
    wp_redirect( site_url( 'basingstoke-together/home/' ), 302 );
    exit;
});

/**
 * Ensure that when viewing a 'single' post using the post types rewrite slug that we redirect to our 'proper' url structure.
 */
Timber::add_route('basingstoke-together/achievements/:year', function($params){
    $url = site_url('basingstoke-together/achievements/?timeline_year=' . $params['year']);
    wp_redirect( $url, 301 );
    exit;
});

Timber::add_route('/', function($params){
    $url = site_url('/in-basingstoke/home/');
    wp_redirect( $url, 301 );
    exit;
});

/**
 *  Webhook for Stripe
 */

Routes::map('webhook/stripe/', function($params){

    $payment_page = Timber::get_post([
        'post_type' => 'page',
        'meta_key' => '_wp_page_template',
        'meta_value' => 'page-templates/together-card.php',
    ]);

    $endpoint_secret = get_field('stripe_secret_webhook_key', $payment_page->id);
    $payload = @file_get_contents('php://input');

    $sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
    $event = null;

    try {
        $event = \Stripe\Webhook::constructEvent(
            $payload, $sig_header, $endpoint_secret
        );
    } catch(\UnexpectedValueException $e) {
        // Invalid payload
        http_response_code(400); // PHP 5.4 or greater
        exit();
    } catch(\Stripe\Error\SignatureVerification $e) {
        // Invalid signature
        http_response_code(400); // PHP 5.4 or greater
        exit();
    }

    if ($event->type == "payment_intent.succeeded") {
        try{
            $intent = $event->data->object;
            $email_data = [];

            $intent->metadata['stripe_transaction_id'] = $intent->charges->data[0]->id;
            $intent->metadata['payment_intent_id'] = $intent->id;
            if (!TogetherCard::process_to_form_submission($intent->metadata, $intent->id) ||
                !TogetherCard::send_emails($intent->metadata)){
                    http_response_code(400); // PHP 5.4 or greater
                    exit();
                }

            printf("Succeeded: %s", $intent->id);

        }
        catch (Exception $e){
            var_dump($e);
            http_response_code(400); // PHP 5.4 or greater
            exit();
        }
    }

    http_response_code(200);
    exit;
});


class BasingstokeBaseView extends TimberView {

    const rewards_api_key = '5A280CA0-FF4C-461F-86E4-47CA12EDE288';

    /**
     * Override this method in an extended class to add additional custom data to the context, or modify the context itself.
     */
    public function post_process(){

        $this->context['nonces'] = array(
            'newsletter_signup' => wp_nonce_field('newsletter_signup_action','newsletter_signup_nonce', true, false),
        );

        /**
         * Handy URLs to share throughout the site.
         */
        $this->context['urls'] = array(
            'home' => '/in-basingstoke/home/',
            'search' => '/search/',
            'contact' => '/in-basingstoke/contact-us/',
            'email' => 'info@basingstoketogether.co.uk',
            'admin_email' => get_option( 'admin_email' ),
            'initiatives' => '/basingstoke-together/business-initiatives/',
            'login' => '/basingstoke-together/login/',
            'logout' => '/basingstoke-together/logout/',
            'tncs' => '/terms-conditions/',
            'sections' => array(
                'home' => '/in-basingstoke/home/',
                'together_home' => '/basingstoke-together/home/',
            ),
            'section' => array(
                'in-basingstoke' => '/in-basingstoke/',
                'basingstoke-together' => '/basingstoke-together/',
            ),
            'footer' => array(
                'sitemap' => '/sitemap/',
                'cookie_policy' => '/cookie-policy/',
                'privacy_policy' => '/privacy-policy/',
                'hma' => 'http://www.hma.co.uk/',
            ),
            'social' => array(
                'facebook' => 'http://www.facebook.com/inbasingstoke',
                'twitter' => 'http://twitter.com/inbasingstoke',
                'youtube' => 'https://www.youtube.com/user/inbasingstoke',
                'instagram'=> 'https://www.instagram.com/inbasingstoke/',
            ),
        );

        /**
         * Get the 10 latest tweets (uses transients for caching/performance).
         */
        if (class_exists('DisplayTweets')) {
            $this->context['tweets'] = array();

            $tweets = DisplayTweets::get_instance()->get('inbasingstoke');

            foreach ($tweets as $idx => $tweet) {
                $this->context['tweets'][] = DisplayTweets::get_instance()->format_tweet( $tweet->text );
            }

            if ( !empty( $this->context['tweets'] ) ) {
                $this->context['latest_tweet'] = $this->context['tweets'][0];
            }
        }

        /**
         * Ensure that current section is properly set! (also setting the 'current' primary menu).
         */
        $this->context['site_section'] = 'inbasingstoke';
        $this->context['menu']['current'] = $this->context['menu']['inbasingstoke'];

        $businessPos = stripos( $_SERVER['REQUEST_URI'], '/basingstoke-together/' );

        if (  $businessPos !== false && intval($businessPos) == 0 ) {
            $this->context['site_section'] = 'basingstoketogether';
            $this->context['menu']['current'] = $this->context['menu']['basingstoketogether'];
        }

        /**
         * Right Column options used on numerous templates.
         */
        $showRelatedLinks = ( isset( $this->context['page']->show_related_links ) && $this->context['page']->show_related_links == 'yes' );
        if (array_key_exists('page', $this->context)) {
            $relatedLinks = get_field('related_links', $this->context['page']->ID );
        }

        $this->context['rightcol'] = array(
            'business' => ( isset( $this->context['page']->show_send_story ) && $this->context['page']->show_business == 'yes' ) ? true : false,
            'event' => ( isset( $this->context['page']->show_send_event ) && $this->context['page']->show_event == 'yes' ) ? true : false,
            'latest_tweet' => ( isset( $this->context['page']->show_latest_tweet ) && $this->context['page']->show_latest_tweet == 'yes' ) ? true : false,
            'related_links' => ( $showRelatedLinks && !empty( $relatedLinks ) ) ? $relatedLinks : false,
        );
    }

}

/**
 * Include BaseTheme class so that we can extend it.
 */
require( get_template_directory() . '/core/class-theme-base.php' );

class Theme extends BaseTheme {

    /**
     * Menu locations available to the theme. Slug/Label structure.
     */
    public $menus = array(
        'inbasingstoke' => 'In Basingstoke Menu',
        'basingstoketogether' => 'Basingstoke Together Menu',
        'inbasingstokefooter' => 'In Basingstoke Footer',
        'basingstoketogetherfooter' => 'Basingstoke Together Footer',
        'sitemap' => 'Sitemap List',
    );

    /**
     * Image Sizes to be used throughout the theme.
     *
     * Associative array of slug, width, height, and force cropping.
     */
    public $imageSizes = array(
        array(
            'slug' => 'contact-square-sml',
            'width' => 110,
            'height' => 110,
            'crop' => true,
        ),
        array(
            'slug' => 'business-list',
            'width' => 165,
            'height' => 135,
            'crop' => false,
        ),
        array(
            'slug' => 'contact',
            'width' => 230,
            'height' => 145,
            'crop' => true,
        ),
        array(
            'slug' => 'list-thumb',
            'width' => 470,
            'height' => 270,
            'crop' => true,
        ),
        array(
            'slug' => 'list-square',
            'width' => 470,
            'height' => 470,
            'crop' => true,
        ),
        array(
            'slug' => 'company',
            'width' => 170,
            'height' => 180,
            'crop' => true,
        ),
        array(
            'slug' => 'gallery',
            'width' => 960,
            'height' => 640,
            'crop' => true,
        ),
        array(
            'slug' => 'gallery-thumb',
            'width' => 95,
            'height' => 95,
            'crop' => true,
        ),
        array(
            'slug' => 'timeline-small',
            'width' => 180,
            'height' => 305,
            'crop' => true,
        ),
        array(
            'slug' => 'file-thumbnail',
            'width' => 90,
            'height' => 125,
            'crop' => false,
        ),
        array(
            'slug' => 'banner-logo',
            'width' => 325,
            'crop' => false,
        ),
        array(
            'slug' => 'together-card',
            'width' => 800,
            'height' => 500,
            'crop' => true,
        ),
    );

    public function init(){
        /**
         * Ensure we call BaseTheme's init method first!
         */
        parent::init();

        /**
         * Register the custm Youtube shortcode.
         */
        add_shortcode( 'youtube', array( $this, 'youtube_shortcode' ) );

        /**
         * Check for custom 'Business User' role, and create if null.
         */
        add_action( 'init', array( $this, 'setup_roles' ) );

        /**
         * Add sub-menu entry to show business users.
         */
        add_action( 'admin_menu', array( $this, 'edit_admin_menu' ) );
    }

    /**
     * Add/reorder menu items to admin nav.
     */
    public function edit_admin_menu() {

        global $menu;

        // get a reference to the media entry
        $mediaMenu = $menu[10];

        // unset 'posts', 'media', and 'links' menu entry (not used)
        unset($menu[5]);
        unset($menu[10]);
        unset($menu[15]);

        // add a menu separator before media
        $menu[15] = array(
            0 => '',
            1 => 'read',
            2 => 'separator-media',
            3 => '',
            4 => 'wp-menu-separator'
        );

        // reset the media item at position 15.
        $menu[15] = $mediaMenu;

        add_submenu_page( 'users.php', 'Business Users', 'Business Users', 'manage_options', 'users.php?role=business_user');
    }

    /**
     * Called on 'init'. Check for required user roles and construct if neccessary.
     */
    public function setup_roles(){

        global $wp_roles;

        $businessRole = get_role( 'business_user' );

        if ( !isset( $businessRole ) ) {
            /**
             * Business User role doesn't exist, so lets create it now.
             */
            add_role( 'business_user', 'Business User' );
        }
    }

    /**
     * Custom Youtube shortcode hook.
     * @param array $atts
     *   Associative array of shortcode attributes.
     *
     * @return string
     *   Youtube markup (if valid id provided)
     */
    public function youtube_shortcode( $atts ) {

        // Attributes
        extract( shortcode_atts(
            array(
                'id' => '',
                'width' => '584',
                'height' => '460',
            ), $atts )
        );

        $markup = '';

        if ( !empty( $id ) ) {
            $markup .= '<div class="video-container">';
            $markup .= '	<iframe src="//www.youtube.com/embed/' . $id . '?rel=0" width="' . $width . '" height="' . $height . '" frameBorder="0" seamless allowfullscreen></iframe>';
            $markup .= '</div>';
        }

        return $markup;
    }

    /**
     * Allow the binding of additional hooks in the extended theme class.
     *
     * Any extra site-wide hooks or filters should be added here.
     *
     */
    public function additional_hooks() {

        // custom filters to add additional attributes to search on
        add_filter( 'algolia_post_shared_attributes', [ $this, 'custom_algolia_attributes' ], 10, 2 );
        add_filter( 'algolia_searchable_post_shared_attributes', [ $this, 'custom_algolia_attributes' ], 10, 2 );

        add_filter( 'algolia_posts_index_settings', [ $this, 'custom_algolia_settings' ], 10, 2 );
    }

    public function custom_algolia_attributes( $attributes, $post ) {

        if ( 'page' === $post->post_type ) {

            $attributes['introduction'] = get_field( 'introduction', $post->ID );
            $attributes['main_content'] = wp_strip_all_tags( get_field( 'main_content', $post->ID ) );

            // remove 'standard' content attribute
            unset($attributes['content']);

            return $attributes;
        }

        if ( 'business' === $post->post_type ) {

            $attributes['description'] = get_field( 'description', $post->ID );

            // remove 'standard' content attribute
            unset($attributes['content']);

            return $attributes;
        }

        if ( 'event' === $post->post_type ) {

            $attributes['main_introduction'] = get_field( 'main_introduction', $post->ID );
            $attributes['main_content'] = wp_strip_all_tags( get_field( 'main_content', $post->ID ) );

            // remove 'standard' content attribute
            unset($attributes['content']);

            return $attributes;
        }

        if ( 'initiative' === $post->post_type ) {

            $attributes['introduction'] = get_field( 'introduction', $post->ID );
            $attributes['main_content'] = wp_strip_all_tags( get_field( 'main_content', $post->ID ) );

            // remove 'standard' content attribute
            unset($attributes['content']);

            return $attributes;
        }

        if ( 'job_vacancy' === $post->post_type ) {

            $attributes['main_content'] = wp_strip_all_tags( get_field( 'main_content', $post->ID ) );

            // remove 'standard' content attribute
            unset($attributes['content']);

            return $attributes;
        }

        if ( 'news_article' === $post->post_type ) {

            $attributes['main_introduction'] = get_field( 'main_introduction', $post->ID );
            $attributes['main_content'] = wp_strip_all_tags( get_field( 'main_content', $post->ID ) );

            // remove 'standard' content attribute
            unset($attributes['content']);

            return $attributes;
        }

        return $attributes;
    }

    public function custom_algolia_settings( $settings, $post_type ) {

        if ($post_type === 'page') {
            $settings['attributesToIndex'][] = 'unordered(introduction)';
            $settings['attributesToIndex'][] = 'unordered(main_content)';

            // Make Algolia return a pre-computed snippet of 50 chars as part of the result set.
            // @see https://www.algolia.com/doc/api-client/ruby/parameters#attributestohighlight
            $settings['attributesToSnippet'][] = 'introduction:50';
            $settings['attributesToSnippet'][] = 'main_content:200';
        }

        if ($post_type === 'business') {
            $settings['attributesToIndex'][] = 'unordered(description)';

            // Make Algolia return a pre-computed snippet of 50 chars as part of the result set.
            // @see https://www.algolia.com/doc/api-client/ruby/parameters#attributestohighlight
            $settings['attributesToSnippet'][] = 'description:200';
        }

        if ($post_type === 'event') {
            $settings['attributesToIndex'][] = 'unordered(main_introduction)';
            $settings['attributesToIndex'][] = 'unordered(main_content)';

            // Make Algolia return a pre-computed snippet of 50 chars as part of the result set.
            // @see https://www.algolia.com/doc/api-client/ruby/parameters#attributestohighlight
            $settings['attributesToSnippet'][] = 'main_introduction:50';
            $settings['attributesToSnippet'][] = 'main_content:200';
        }

        if ($post_type === 'initiative') {
            $settings['attributesToIndex'][] = 'unordered(introduction)';
            $settings['attributesToIndex'][] = 'unordered(main_content)';

            // Make Algolia return a pre-computed snippet of 50 chars as part of the result set.
            // @see https://www.algolia.com/doc/api-client/ruby/parameters#attributestohighlight
            $settings['attributesToSnippet'][] = 'introduction:50';
            $settings['attributesToSnippet'][] = 'main_content:200';
        }

        if ($post_type === 'job_vacancy') {
            $settings['attributesToIndex'][] = 'unordered(main_content)';

            // Make Algolia return a pre-computed snippet of 50 chars as part of the result set.
            // @see https://www.algolia.com/doc/api-client/ruby/parameters#attributestohighlight
            $settings['attributesToSnippet'][] = 'main_content:200';
        }

        if ($post_type === 'news_article') {

            $settings['attributesToIndex'][] = 'unordered(main_introduction)';
            $settings['attributesToIndex'][] = 'unordered(main_content)';

            // Make Algolia return a pre-computed snippet of 50 chars as part of the result set.
            // @see https://www.algolia.com/doc/api-client/ruby/parameters#attributestohighlight
            $settings['attributesToSnippet'][] = 'main_introduction:50';
            $settings['attributesToSnippet'][] = 'main_content:200';
        }

        return $settings;
    }
}

/**
 * Create an instance of our new theme.
 *
 * As the theme extends BaseTheme, there is no need to call init manually as it is automatically called when constructed.
 */
$theme = new Theme();

/**
 * Include all of our custom post types.
 */
require( get_template_directory() . '/custom-post-types/custom-post-types.php' );

/**
 * Include field groups (ACF).
 */
require( get_template_directory() . '/custom-field-groups/custom-field-groups.php' );

/**
 * Include Mandrill Emails class so that we can extend it.
 */
require( get_template_directory() . '/core/class-mandrill-emails.php' );

/**
 * Mailchimp integration
 */
require_once(get_template_directory() . '/core/inc/mailchimpv2.php');
require_once(get_template_directory() . '/core/inc/mailchimpv3.php');

/**
 * Adds ajax forms
 */
require_once(get_template_directory() . '/ajax-forms/forms.php');

/**
 * Process Together Card Submission
 */
require_once(get_template_directory() . '/stripe/together-card.php');
