<?php

class NewsletterSignupForm extends AjaxForm {

	public function __construct(){
		parent::__construct('newsletter_signup');
	}

	public function process(){

		parent::process();

        $first_name = ( array_key_exists('newsletter_signup_firstname', $_POST) ) ? sanitize_text_field( $_POST['newsletter_signup_firstname'] ) : '';
        $last_name = ( array_key_exists('newsletter_signup_lastname', $_POST) ) ? sanitize_text_field( $_POST['newsletter_signup_lastname'] ) : '';
        $email = ( array_key_exists('newsletter_signup_email', $_POST) ) ? sanitize_email( $_POST['newsletter_signup_email'] ) : '';



        if ( strlen( $first_name ) < 1 ) {
            $this->errors[] = 'Please enter your first name.';
        }

        if ( strlen( $last_name ) < 1 ) {
            $this->errors[] = 'Please enter your last name.';
        }

        if ( strlen( $email ) < 1 ) {
            $this->errors[] = 'Please enter your email address.';
        } else {
            if ( !is_email( $email ) ) {
                $this->errors[] = 'Please enter a valid email address.';
            }
        }
		

		if ( strlen( $email ) < 1 ) {
			$this->errors[] = 'Please enter your email address.';
		} else {
			if ( !is_email( $email ) ) {
				$this->errors[] = 'Please enter a valid email address.';
			}
		}

		if ( empty($this->errors) ) {

			$mcClient = new \HMA\MailChimpV2( '816ecd073776659f55dd94a73cad9810-us13' );

			$result = $mcClient->call('lists/subscribe', array(
                'id'                => 'd27158bc88',
                'email'             => array('email'=>$email),
                'merge_vars'        => array('FNAME'=>$first_name, 'LNAME'=>$last_name),
                'double_optin'      => false,
                'update_existing'   => true,
                'replace_interests' => false,
                'send_welcome'      => false,
            ));

			$this->success = ( is_array( $result ) && array_key_exists( 'euid', $result ) );

			// also store in db for belt and braces...
			/**
        	 * Save to database under form_submission post type.
        	 */
        	$post = array(
	            'post_title' => $first_name,
	            'post_status' => 'private',
	            'post_type'	=> 'form_submission',
	        );

	        $post_id = wp_insert_post($post);
			$dbsaved = ($post_id > 0);

			if ($dbsaved) {
				//store our extra fields
				add_post_meta($post_id, 'form_name',  'newsletter_signup', false);
                add_post_meta($post_id, 'email_address', $email, false);
				add_post_meta($post_id, 'mc_api_response', $result, false);
			}
			

			if ( true === $this->success ) {
				$this->message = 'Thank you for registering to receive the Basingstoke Together newsletter.';
			} else {
				$this->errors[] = 'An unexpected error has occurred. Please try again.';
			}

		}

		echo json_encode(array(
			'success' => $this->success,
			'message' => $this->message,
			'result' => $this->resultData,
			'errors' => $this->errors,
		));
			
		exit;
		
	}

}

new NewsletterSignupForm();
