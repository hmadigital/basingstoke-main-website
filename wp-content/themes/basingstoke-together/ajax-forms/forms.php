<?php

abstract class AjaxForm {

	private $action = '';
	public $success = false;
	public $message = '';
	public $errors = array();
	public $resultData = array();

    public function __construct( $action ) {
    	$this->action = $action;
        add_action( "wp_ajax_nopriv_$action", array ( $this, 'process' ) );
        add_action( "wp_ajax_$action", array ( $this, 'process' ) );
    }

    public function process() {

        header("Content-Type: application/json");

        if ( !isset( $_POST[$this->action . '_nonce'] ) || !wp_verify_nonce( $_POST[$this->action . '_nonce'], $this->action . '_action' ) ) {
		    $this->errors[] = 'Sorry, your nonce did not verify.';
		}

    }

}

require_once(get_template_directory() . '/ajax-forms/newsletter-signup.php');
