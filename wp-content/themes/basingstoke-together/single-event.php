<?php

class EventDetailView extends BasingstokeBaseView {

	public function post_process(){

		parent::post_process();

        $related_businesses = get_field('related_business', $this->context['page']->ID);

        if ( is_array($related_businesses) ) {

            $results = Timber::get_posts(array(
                'post_type' => 'business',
                'posts_per_page' => 1,
                'post__in' => $related_businesses,
            ) );

            if (is_array($results) && !empty($results)) {

                $this->context['related_business'] = $results[0];

            }

        }

		/*
		 * Always ensure 'Send an event' banner is shown.
		 */
		$this->context['rightcol']['event'] = true;
	}

}

$view = new EventDetailView( array( 'events-detail.twig' ) );
$view->render();