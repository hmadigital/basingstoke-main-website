<?php
/**
 * Template Name: Events List
 *
 * Description: Events List
 */

class EventMonthYearQuery {
	
	private $_query = '';

	function __construct(){

		global $wpdb;

		$this->_query = "
			SELECT 
				YEAR(metastart.meta_value) AS `year`, 
				MONTH(metastart.meta_value) AS `month`, 
				count(wposts.ID) as postcount 
			FROM $wpdb->posts wposts, $wpdb->postmeta metastart 
			WHERE wposts.post_type = 'event' 
			AND wposts.ID = metastart.post_id
			AND metastart.meta_key = 'start_date'
			AND wposts.post_status = 'publish'
			GROUP BY YEAR(metastart.meta_value), MONTH(metastart.meta_value) 
			ORDER BY metastart.meta_value ASC
		";

	}

	public function get($baseUrl){

		global $wpdb;

		// get time the posts were last changed
		$last_changed = wp_cache_get( 'last_changed', 'posts' );
		
		// build a unique cache key
		$key = md5( $this->_query );
		$key = "event_month_year_list:$key:$last_changed";

		//try and get our data from cache to start with
		if ( ! $results = wp_cache_get( $key, 'news_articles' ) ) {

			// if no luck from cache, lets query the db.
			$results = $wpdb->get_results( $this->_query );
			wp_cache_set( $key, $results, 'news_articles' );
		}
		if ( $results ) {
			
			$monthsYears = array();

			// tidy up the result :)
			foreach ( (array) $results as $result) {
				$monthsYears[] = array(
					'name' => $result->month . ' ' . $result->year,
					'month' => $result->month,
					'month_name' => date('F', strtotime('2012-' . $result->month . '-01')),
					'year' => $result->year,
					'slug' => strtolower($result->month . '-' . $result->year),
					'count' => $result->postcount,
					'link' => $baseUrl . $result->year .'/',
				);
			}

			return $monthsYears;
		} else {
			return array();
		}
	}
}

class EventListView extends BasingstokeBaseView {

	public function post_process(){	

		/**
		 * Ensure the base class post_process method gets called.
		 */
		parent::post_process();

		$link = $this->context['page']->link();

		/**
		 * Get cached reference to distinct month/years for news articles.
		 */
		$myQuery = new EventMonthYearQuery();
		$this->context['months_years'] = $myQuery->get($link);

		/**
		 * Get current page of news articles and ensure its available in the twig context.
		 */
		$currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$this->context['current_page'] = $currentPage;
		
		/**
		 * Parse the current story focus from the querystring if present, and add to the twig context.
		 */
		$eventFocus = ( array_key_exists('eventfocus', $_GET) && strlen($_GET['eventfocus']) > 0 ) ? strtolower( sanitize_text_field( $_GET['eventfocus'] ) ) : '';		

		/**
		 * Parse the current month/year from the querystring if present, and add to the twig context.
		 */
		$monthYear = ( array_key_exists('monthyear', $_GET) && strlen($_GET['monthyear']) > 0 ) ? explode( '-', ( sanitize_text_field( $_GET['monthyear'] ) ) ) : 'all';
		$currentMonth = ( is_array($monthYear) && !empty($monthYear) ) ? intval( $monthYear[0] ) : null;
		$currentYear = ( is_array($monthYear) && count($monthYear) > 1) ? intval( $monthYear[1] ) : null;
		
		$this->context['current_month'] = $currentMonth;
		$this->context['current_year'] = $currentYear;

		/**
		 * Get all 'Story Foci' taxonomy terms. Hide Empty terms, and only get top levels ones!
		 */
		$eventFoci = get_terms('event_focus', array(
			'hide_empty' => true,
			'hierarchical'  => false, 
			'parent' => 0,
		));
		
		if ( !empty( $eventFoci ) && !is_wp_error( $eventFoci ) ){
			$this->context['event_foci'] = $eventFoci;
		}

		/**
		 * Get 'Events' custom posts too.
		 */
		$yesterdayString = date('Ymd', time() - 60 * 60 * 24);
		$eventsArgs = array(
			'post_type' => 'event',
			'posts_per_page' => 12,
			'paged' => $currentPage,
			'meta_key' => 'start_date', 
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
			'meta_query' => [
				[
					'key' => 'event_section',
					'value' => $this->context['site_section'],
				],
				[
					'relation' => 'OR',
					[
						'key' => 'start_date',
						'value' => $yesterdayString,
						'compare' => '>',
					],
					[
						[
							'key' => 'end_date',
							'compare' => 'EXISTS',
						],
						[
							'key' => 'end_date',
							'value' => $yesterdayString,
							'compare' => '>',
						],
					],
				],
			],
		);

		if ( $monthYear !== 'all' && (int)$currentMonth > 0 && (int)$currentYear > 0 ) {

			$startMonth = new DateTime( "$currentYear-$currentMonth-01" );
			$lastDay = $startMonth->format('t');
			$endMonth = new DateTime( "$currentYear-$currentMonth-$lastDay" );

			$eventsArgs['meta_query'][] = [
				'key'		=> 'start_date',
				'compare'	=> 'BETWEEN',
				'value'		=> array( $startMonth->format('Ymd'), $endMonth->format('Ymd') ),
				'type'		=> 'DATE',
			];
		}

		if ( !empty( $eventFocus ) ) {
			$eventsArgs['tax_query'] = array(
				array(
					'taxonomy' => 'event_focus',
					'field' => 'slug',
					'terms' => $eventFocus,
				),
			);
		}

		$events = Timber::get_posts($eventsArgs, 'EventPost' );

		/* THESE LINES ARE CRUCIAL */
		/* in order for WordPress to know what to paginate */
		/* your args have to be the default query */
		query_posts($eventsArgs);

		$this->context['pagination'] = Timber::get_pagination();

		
		$this->context['events'] = $events;
	}

}

$view = new EventListView( array('events-list.twig') );
$view->render();