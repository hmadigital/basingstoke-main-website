<?php
/**
 * Template Name: Business Initiatives
 *
 * Description: Business Initiatives
 */

class BusinessInitiativesView extends BasingstokeBaseView {

	private function process_notification(){

		/**
		 * If these querystring items are present, it will be via a notification signup link.
		 */
		if (array_key_exists('action', $_GET) && array_key_exists('key', $_GET) && array_key_exists('otk', $_GET) ) {

			$key = strtolower( $_GET['key'] );
			$nonceName = 'notification_' . $key;

			/**
			 * Get a reference to the initiative post by key (post slug).
			 */
			$initiative = Timber::get_post( array(
			    'post_type' => 'initiative',
			    'name' => $key,
			), 'InitiativePost' );

			/**
			 * Check nonce is valid first for additional security.
			 */
			if ( $initiative && intval( wp_verify_nonce( $_GET['otk'], $nonceName ) ) > 0 ) {

				$result = $initiative->toggleSignup();

				$this->context['notification_message'] = $result['message'];
				if ($result['success'] == true) {
					$this->context['notification_success'] = true;
				} else {
					$this->context['notification_error'] = true;
				}

			}
		}

		/**
		 * If these querystring items are present, it will be via a notification signup link.
		 */
		if (array_key_exists('action', $_POST) && array_key_exists('key', $_POST) && array_key_exists('otk', $_POST) ) {

			$key = strtolower( $_POST['key'] );

			/**
			 * Get a reference to the initiative post by key (post slug).
			 */
			$initiative = Timber::get_post( array(
			    'post_type' => 'initiative',
			    'name' => $key,
			), 'InitiativePost' );

			/**
			 * Check nonce is valid first for additional security.
			 */
			if ( $initiative && intval( wp_verify_nonce( $_POST['otk'], $initiative->action_type() ) ) > 0 ) {

				$result = $initiative->processForm();

				$this->context['notification_message'] = $result['message'];
				if ($result['success'] == true) {
					$this->context['notification_success'] = true;
				} else {
					$this->context['notification_error'] = true;
				}

			}

		}

	}

	public function post_process(){

		parent::post_process();

		/**
		 * Auth check first. If not logged in - you shouldn't be here!
		 */
		// if ( !is_user_logged_in() ) {

		// 	$redir = $this->context['urls']['initiatives'];
		// 	$loginLink = site_url( $this->context['urls']['login'] );
		// 	$loginLink .= '?redir=' . urlencode($redir);

		// 	wp_redirect( $loginLink, 302 );
		// 	exit;
		// }

		/**
		 * Check if a notifcation link was the source and process accordingly.
		 */
		$this->process_notification();

		/**
		 * Get all initiatives, grouped by taxonomies (manually).
		 */
		$initiatives = array();

		$initiatives['promotion'] = Timber::get_posts( array(
		    'post_type' => 'initiative',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'initiative_type',
					'field' => 'slug',
					'terms' => 'promotion'
				)
			)
		), 'InitiativePost' );

		$initiatives['a_voice_for_business'] = Timber::get_posts( array(
		    'post_type' => 'initiative',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'initiative_type',
					'field' => 'slug',
					'terms' => 'a-voice-for-business'
				)
			)
		), 'InitiativePost' );

		$initiatives['a_place_for_people'] = Timber::get_posts( array(
		    'post_type' => 'initiative',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'initiative_type',
					'field' => 'slug',
					'terms' => 'a-place-for-people'
				)
			)
		), 'InitiativePost' );

		$initiatives['a_place_for_business'] = Timber::get_posts( array(
		    'post_type' => 'initiative',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'initiative_type',
					'field' => 'slug',
					'terms' => 'a-place-for-business'
				)
			)
		), 'InitiativePost' );

		$this->context['initiatives'] = $initiatives;
	}

}

$view = new BusinessInitiativesView( array('business-initiatives.twig') );
$view->render();