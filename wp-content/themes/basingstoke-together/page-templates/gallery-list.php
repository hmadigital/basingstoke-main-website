<?php

class GalleryListView extends BasingstokeBaseView {

	public function post_process(){	

		/**
		 * Ensure the base class post_process method gets called.
		 */
		parent::post_process();

		$link = $this->context['page']->link();

		
		/**
		 * Get 'Gallery Detail' custom posts too.
		 */
		$galleryArgs = array(
		    'post_type' => 'gallery',
		    'posts_per_page' => 8,
		    'rewrite' => array( 'slug' => 'make-it-ealing/galleries' ),
		    'has_archive' => false,
		    'hierarchical' => true
		);

		$galleries = Timber::get_posts($galleryArgs );

		/* THESE LINES ARE CRUCIAL */
	    /* in order for WordPress to know what to paginate */
	    /* your args have to be the default query */
		query_posts($galleryArgs);

		$this->context['pagination'] = Timber::get_pagination();

		
		$this->context['galleries'] = $galleries;
	}

}

$view = new GalleryListView( array('gallery-list.twig') );
$view->render();