<?php

class ForgotPasswordView extends BasingstokeBaseView {

	private $_nonceName = 'forgot_password_nonce';
	private $_nonceAction = 'ealing_business_forgot_password';

	/**
	 * Function to check if the forgot pass form was submitted and to process it accordingly.
	 */
	private function check_values(){

		if ( isset( $this->context['form_data']['form_name'] ) && ( !isset( $_POST[$this->_nonceName] ) || !wp_verify_nonce( $_POST[$this->_nonceName], $this->_nonceAction ) ) ) {
			$this->context['form_errors']['invalid_nonce'] = 'An invalid nonce value was provided. Please try again.';
			return;
		}

		// process an email reset link trigger
		if ( isset( $this->context['form_data']['form_name'] ) && $this->context['form_data']['form_name'] == 'forgot_password' ) {
			
			/**
			 * Check nonce is valid first!
			 */
			$sentKey = $this->generate_reset_key();

			if( is_wp_error( $sentKey ) ) {
				$this->context['form_errors'] = $sentKey->get_error_messages();
			} else {
				$this->context['form_data']['sent_key'] = true;
			}			
		}


		//process a user password reset trigger
		if ( isset( $this->context['form_data']['form_name'] ) && $this->context['form_data']['form_name'] == 'reset_password' && $this->has_valid_reset_key() ) {
			
			$errors = new WP_Error();

			$user = check_password_reset_key($_GET['key'], $_GET['login']);

			if ( !isset( $user ) ) {
				$errors->add( 'user_not_found', __( 'An error occurred verifying the user. Please try again.' ) );
			}
			
			if ( isset($_POST['pass1']) && $_POST['pass1'] != $_POST['pass2'] ) {
				$errors->add( 'password_reset_mismatch', __( 'The passwords do not match.' ) );
			}

			if ( !isset( $_POST['pass1'] ) || empty($_POST['pass1']) || strlen($_POST['pass1']) < 9 ) {
				$errors->add( 'password_reset_lenght', __( 'Your new password must be at least 8 chracters long.' ) );
			}

			/**
			 * Fires before the password reset procedure is validated.
			 *
			 * @since 3.5.0
			 *
			 * @param object           $errors WP Error object.
			 * @param WP_User|WP_Error $user   WP_User object if the login and reset key match. WP_Error object otherwise.
			 */
			do_action( 'validate_password_reset', $errors, $user );

			if ( !$errors->get_error_code() ) {
				
				reset_password($user, $_POST['pass1']);
				$this->context['form_data']['password_reset_success'] = true;
				
			} else {
				$this->context['form_errors'] = $errors->get_error_messages();
			}


		}
	}

	private function generate_reset_key(){

		global $wpdb, $wp_hasher;

		$errors = new WP_Error();

		if ( empty( $_POST['user_email'] ) ) {
			$errors->add('empty_username', __('Please enter a valid e-mail address.'));
		} else if ( strpos( $_POST['user_email'], '@' ) ) {
			$user_data = get_user_by( 'email', trim( $_POST['user_email'] ) );
			if ( empty( $user_data ) )
				$errors->add('invalid_email', __('There is no user registered with that email address.'));
		} 

		/**
		 * Fires before errors are returned from a password reset request.
		 *
		 * @since 2.1.0
		 */
		do_action( 'lostpassword_post' );

		if ( $errors->get_error_code() )
			return $errors;

		if ( !$user_data ) {
			$errors->add('invalidcombo', __('Error retrieving user information.'));
			return $errors;
		}

		// redefining user_login ensures we return the right case in the email
		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;

		/**
		 * Fires before a new password is retrieved.
		 *
		 * @since 1.5.0
		 * @deprecated 1.5.1 Misspelled. Use 'retrieve_password' hook instead.
		 *
		 * @param string $user_login The user login name.
		 */
		do_action( 'retreive_password', $user_login );
		/**
		 * Fires before a new password is retrieved.
		 *
		 * @since 1.5.1
		 *
		 * @param string $user_login The user login name.
		 */
		do_action( 'retrieve_password', $user_login );

		/**
		 * Filter whether to allow a password to be reset.
		 *
		 * @since 2.7.0
		 *
		 * @param bool true           Whether to allow the password to be reset. Default true.
		 * @param int  $user_data->ID The ID of the user attempting to reset a password.
		 */
		$allow = apply_filters( 'allow_password_reset', true, $user_data->ID );

		if ( ! $allow )
			return new WP_Error('no_password_reset', __('Password reset is not allowed for this user'));
		else if ( is_wp_error($allow) )
			return $allow;

		// Generate something random for a password reset key.
		$key = wp_generate_password( 20, false );

		/**
		 * Fires when a password reset key is generated.
		 *
		 * @since 2.5.0
		 *
		 * @param string $user_login The username for the user.
		 * @param string $key        The generated password reset key.
		 */
		do_action( 'retrieve_password_key', $user_login, $key );

		// Now insert the key, hashed, into the DB.
		if ( empty( $wp_hasher ) ) {
			require_once ABSPATH . 'wp-includes/class-phpass.php';
			$wp_hasher = new PasswordHash( 8, true );
		}
		$hashed = $wp_hasher->HashPassword( $key );
		$wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

		$message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
		$message .= network_home_url( '/' ) . "\r\n\r\n";
		$message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
		$message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
		$message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
		$message .= '<' . network_site_url("business/login/forgotten-password/?action=rp&key=$key&login=" . rawurlencode($user_login), 'http') . ">\r\n";
		
		if ( is_multisite() )
			$blogname = $GLOBALS['current_site']->site_name;
		else
			// The blogname option is escaped with esc_html on the way into the database in sanitize_option
			// we want to reverse this for the plain text arena of emails.
			$blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

		$title = sprintf( __('[%s] Password Reset'), $blogname );

		/**
		 * Filter the subject of the password reset email.
		 *
		 * @since 2.8.0
		 *
		 * @param string $title Default email title.
		 */
		$title = apply_filters( 'retrieve_password_title', $title );
		/**
		 * Filter the message body of the password reset mail.
		 *
		 * @since 2.8.0
		 *
		 * @param string $message Default mail message.
		 * @param string $key     The activation key.
		 */
		$message = apply_filters( 'retrieve_password_message', $message, $key );

		if ( $message && !wp_mail( $user_email, wp_specialchars_decode( $title ), $message ) )
			wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function.') );

		return true;
	}

	private function has_valid_reset_key(){

		if ($this->context['form_type'] == 'resetpassword') {
			
			$user = check_password_reset_key($_GET['key'], $_GET['login']);

			if ( is_wp_error($user) ) {
				if ( $user->get_error_code() === 'expired_key' ) {
					$this->context['key_error'] = 'This reset link has expired. Please try requesting a password reset again.';
				}
				else {
					$this->context['key_error'] = 'This reset link is invalid. Please double check the link entered.';
				}
			}
		}

		return !( isset( $this->context['key_error'] ) && strlen( $this->context['key_error'] ) > 0 );

	}

	public function post_process(){
		
		parent::post_process();

		/**
		 * Already logged in? If so why do you want to regnerate your password? Lets bail.
		 */
		if ( $this->context['user']['logged_in'] == true ) {
			wp_redirect( $this->get_redir(), 302 );
			exit;
		}

		/**
		 * Work out which form to render.
		 */
		$this->context['form_type'] = ( array_key_exists('action', $_GET) && array_key_exists('key', $_GET) && array_key_exists('login', $_GET) ) ? 'resetpassword' : 'sendemail';

		/**
		 * Work out which title to render.
		 */
		$this->context['title'] = ( $this->context['form_type'] == 'resetpassword' ) ? 'Reset Your Password' : 'Forgotten Password';

		/**
		 * If it is a reset password form, ensure that they keys are valid.
		 */
		$this->has_valid_reset_key();

		/**
		 * Get nonce markup as string to inject in tpl. (echos by default, which we don't want!).
		 */
		$this->context['nonce'] = wp_nonce_field($this->_nonceAction, $this->_nonceName, true, false);

		$this->check_values();
	}
}

$view = new ForgotPasswordView( array('forgot-password.twig') );
$view->render();