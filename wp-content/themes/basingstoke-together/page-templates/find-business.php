<?php
/**
 * Template Name: Find A Business
 *
 * Description: Find A Business
 */

class BusinessAlphaQuery {
	
	public static function get(){

		$terms = get_terms('business_letter');	
		
		// build up a full list of letters
		$alphas = range('A', 'Z');
		// ensure we add our numeric choice before the alpha options too.
		array_splice($alphas, 0, 0, '0-9');
		
		$tplLetters = array();

		// build an array of letters for use in template.
		// assume no matches by default
		foreach ($alphas as $letter) {
			$key = strtolower($letter);

			$tplLetters[$key] = array(
				'has_matches' => false,
				'letter' => $letter,
			);
		}

		if (!empty($terms))	{

			// iterate over terms and assume matches if exists
			foreach ( $terms as $term) {	
				$letter = strtolower( $term->slug );

				if ( isset( $tplLetters[$letter] )) {
					$tplLetters[$letter]['has_matches'] = true;
				}
			}

			return $tplLetters;
		} else {
			return array();
		}
	}
	
}

class BusinessListView extends BasingstokeBaseView {

	public function post_process(){	

		global $wpdb;

		/**
		 * Ensure the base class post_process method gets called.
		 */
		parent::post_process();

		/**
		 * Get a list of letters (with counts) for use in tpl.
		 */
		$this->context['letters'] = BusinessAlphaQuery::get();

		/**
		 * Get a current page ref.
		 */
		$currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$this->context['current_page'] = $currentPage;

		/**
		 * Get a current letter ref.
		 */
		$currentLetter = (get_query_var('by_letter')) ? strtolower( get_query_var('by_letter') ) : 'all';
		$this->context['current_letter'] = $currentLetter;

		/**
		 * Parse the business name from the querystring if present, and add to the twig context.
		 */
		$businessName = ( array_key_exists('business_name', $_GET) && strlen($_GET['business_name']) > 0 ) ? strtolower( sanitize_text_field( $_GET['business_name'] ) ) : '';		
		$this->context['business_name'] = $businessName;


		/**
		 * Parse the business name from the querystring if present, and add to the twig context.
		 */
		$businessType = ( array_key_exists('business_type', $_GET) && strlen($_GET['business_type']) > 0 ) ? strtolower( sanitize_text_field( $_GET['business_type'] ) ) : '';		

		/**
		 * Get all 'Buiness Category' taxonomy terms. Hide Empty terms, and only get top levels ones!
		 */
		$businessTypes = get_terms('business_category', array(
			'hide_empty' => true,
			'hierarchical'  => false, 
			'parent' => 0,
		));
 		
 		if ( !empty( $businessTypes ) && !is_wp_error( $businessTypes ) ){
 			$this->context['business_types'] = $businessTypes;
 		}

		/**
		 * Build up args for 'Business' custom posts too.
		 */
		$qryArgs = array(
		    'post_type' => 'business',
		    'order' => 'ASC',
		    'orderby' => 'title',
		    'posts_per_page' => 8,
		    'paged' => $currentPage,
		    'tax_query' => array(),
		);

		if ( strlen($businessName) > 0 ) {
			$qryArgs['s'] = $businessName;
		}

		if ($currentLetter !== 'all') {

			$qryArgs['tax_query'][] = array(
				'taxonomy' => 'business_letter',
				'field' => 'slug',
				'terms' => $currentLetter,
			);
		}

		if ( strlen($businessType) > 0 ) {
			$qryArgs['tax_query'][] = array(
				'taxonomy' => 'business_category',
				'field' => 'slug',
				'terms' => $businessType,
			);
		}

		$businesses = Timber::get_posts( $qryArgs, 'BusinessPost' );

		/* THESE LINES ARE CRUCIAL */
	    /* in order for WordPress to know what to paginate */
	    /* your args have to be the default query */
		query_posts($qryArgs);

		$this->context['pagination'] = Timber::get_pagination();		
		$this->context['businesses'] = $businesses;
	}
}

$view = new BusinessListView( array('find-business.twig') );
$view->render();