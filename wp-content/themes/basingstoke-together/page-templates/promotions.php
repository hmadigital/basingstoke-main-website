<?php
/**
 * Template Name: Promotions
 *
 * Description: Promotions
 */

class PromotionsCardView extends BasingstokeBaseView {

	private function process_notification(){

		/**
		 * If these querystring items are present, it will be via a notification signup link.
		 */
		if (array_key_exists('action', $_GET) && array_key_exists('key', $_GET) && array_key_exists('otk', $_GET) ) {

			$key = strtolower( $_GET['key'] );
			$nonceName = 'notification_' . $key;

			/**
			 * Get a reference to the promotion post by key (post slug).
			 */
			$promotion = Timber::get_post( array(
			    'post_type' => 'promotion',
			    'name' => $key,
			), 'InitiativePost' );

			/**
			 * Check nonce is valid first for additional security.
			 */
			if ( $promotion && intval( wp_verify_nonce( $_GET['otk'], $nonceName ) ) > 0 ) {

				$result = $promotion->toggleSignup();

				$this->context['notification_message'] = $result['message'];
				if ($result['success'] == true) {
					$this->context['notification_success'] = true;
				} else {
					$this->context['notification_error'] = true;
				}

			}
		}

		/**
		 * If these querystring items are present, it will be via a notification signup link.
		 */
		if (array_key_exists('action', $_POST) && array_key_exists('key', $_POST) && array_key_exists('otk', $_POST) ) {

			$key = strtolower( $_POST['key'] );

			/**
			 * Get a reference to the promotion post by key (post slug).
			 */
			$promotion = Timber::get_post( array(
			    'post_type' => 'promotion',
			    'name' => $key,
			), 'InitiativePost' );

			/**
			 * Check nonce is valid first for additional security.
			 */
			if ( $promotion && intval( wp_verify_nonce( $_POST['otk'], $promotion->action_type() ) ) > 0 ) {

				$result = $promotion->processForm();

				$this->context['notification_message'] = $result['message'];
				if ($result['success'] == true) {
					$this->context['notification_success'] = true;
				} else {
					$this->context['notification_error'] = true;
				}

			}

		}

	}

	public function post_process(){

		parent::post_process();

		/**
		 * Check if a notifcation link was the source and process accordingly.
		 */
		$this->process_notification();

		/**
		 * Get all promotions, grouped by taxonomies (manually).
		 */
		$promotions = array();

		$current_date = date( 'Ymd' );

		// Date metaquery array
		$metaquery_date = array(
			'relation' => 'OR',
			// If an end_date exists, check that it is upcoming.
			array(
				'key'        => 'end_date',
				'compare'    => '>=',
				'value'      => $current_date,
			),
			// OR!
			array(
				// A nested set of conditions for when the above condition is false.
				array(
					// We use another, nested set of conditions, for if the end_date
					// value is empty, OR if it is null/not set at all. 
					'relation' => 'OR',
					array(
						'key'        => 'end_date',
						'compare'    => '=',
						'value'      => '',
					),
					array(
						'key'        => 'end_date',
						'compare'    => 'NOT EXISTS',
					),
				),
			),
		);

		$promotions['shopping'] = Timber::get_posts( array(
		    'post_type' => 'promotion',
		    'nopaging' => true,
		    'orderby' => 'title',
			'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'promotion_type',
					'field' => 'slug',
					'terms' => 'shopping'
				)
			),
			'meta_key' => 'end_date',
			'meta_query' => $metaquery_date
		), 'PromotionPost' );


		$promotions['food_and_drink'] = Timber::get_posts( array(
		    'post_type' => 'promotion',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'promotion_type',
					'field' => 'slug',
					'terms' => 'food-and-drink'
				)
			),
			'meta_key' => 'end_date',
			'meta_query' => $metaquery_date
		), 'PromotionPost' );

		$promotions['entertainment'] = Timber::get_posts( array(
		    'post_type' => 'promotion',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'promotion_type',
					'field' => 'slug',
					'terms' => 'entertainment'
				)
			),
			'meta_key' => 'end_date',
			'meta_query' => $metaquery_date
		), 'PromotionPost' );

		$promotions['health_and_beauty'] = Timber::get_posts( array(
		    'post_type' => 'promotion',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'promotion_type',
					'field' => 'slug',
					'terms' => 'health-and-beauty'
				)
			),
			'meta_key' => 'end_date',
			'meta_query' => $metaquery_date
		), 'PromotionPost' );

		$promotions['professional_services'] = Timber::get_posts( array(
		    'post_type' => 'promotion',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'promotion_type',
					'field' => 'slug',
					'terms' => 'professional-services'
				)
			),
			'meta_key' => 'end_date',
			'meta_query' => $metaquery_date
		), 'PromotionPost' );

		$promotions['other'] = Timber::get_posts( array(
		    'post_type' => 'promotion',
		    'nopaging' => true,
		    'orderby' => 'title',
		    'order' => 'ASC',
		    'tax_query' => array(
				array(
					'taxonomy' => 'promotion_type',
					'field' => 'slug',
					'terms' => 'other'
				)
			),
			'meta_key' => 'end_date',
			'meta_query' => $metaquery_date
		), 'PromotionPost' );

		$this->context['promotions'] = $promotions;
	}
	
}

$view = new PromotionsCardView( array('promotions.twig') );
$view->render();