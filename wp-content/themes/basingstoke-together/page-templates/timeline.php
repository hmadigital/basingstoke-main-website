<?php

class TimelineView extends BasingstokeBaseView {

    public function post_process(){

        parent::post_process();

        $months = get_field( 'timeline_months', $this->context['page']->ID );

        if (isset($_GET['timeline_year']) && is_numeric($_GET['timeline_year']) && $_GET['timeline_year'] >= 2000 && $_GET['timeline_year'] <= 2100) {

            $year = $_GET['timeline_year'];

        } else {

            $year = date('Y');

        }

        $valid_months = array();

        $valid_years = array();

        foreach($months as &$month) {

            $valid_years[$month['year']] = true;

            if ( $month['year'] == $year ) {

                $month['image'] = new TimberImage($month['image']);

                $valid_months[] = $month;

            }

        }

        $timeline_buttons = array();

        if ( isset($valid_years[ $year - 1 ]) ) {
            $timeline_buttons['prev'] = $year - 1;
            $timeline_buttons['current'] = $year;
        }

        if ( isset($valid_years[ $year + 1 ]) ) {
            $timeline_buttons['next'] = $year + 1;
            $timeline_buttons['current'] = $year;
        }

        $this->context['months'] = $valid_months;
        $this->context['timeline_buttons'] = $timeline_buttons;

    }
}

$view = new TimelineView( array('timeline.twig') );
$view->render();
