<?php
/**
 * Template Name: Search
 *
 * Description: Search
 */

class SearchResultsView extends BasingstokeBaseView {

    public function post_process(){

        parent::post_process();

        $currentPage = (isset($_GET['sp']) && strlen($_GET['sp']) > 0) ? (int)$_GET['sp'] : 1;
        $searchQuery = (isset($_GET['searchquery']) && strlen($_GET['searchquery']) > 0) ? sanitize_text_field($_GET['searchquery']) : '';

        $pageSize = 8;
        $recordCount = 0;
        $pageCount = 1;
        $pageLinks = [];
        $start = (($currentPage * $pageSize) - ($pageSize - 1));
        $resultList = [];
        /***
         * All possible CSE params for reference 
         ***/
        // $cseUrl = 'https://www.googleapis.com/customsearch/v1
        //  ?q={searchTerms}
        //  &num={count?}
        //  &start={startIndex?}
        //  &lr={language?}
        //  &safe={safe?}
        //  &cx={cx?}
        //  &cref={cref?}
        //  &sort={sort?}
        //  &filter={filter?}
        //  &gl={gl?}
        //  &cr={cr?}
        //  &googlehost={googleHost?}
        //  &c2coff={disableCnTwTranslation?}
        //  &hq={hq?}
        //  &hl={hl?}
        //  &siteSearch={siteSearch?}
        //  &siteSearchFilter={siteSearchFilter?}
        //  &exactTerms={exactTerms?}
        //  &excludeTerms={excludeTerms?}
        //  &linkSite={linkSite?}
        //  &orTerms={orTerms?}
        //  &relatedSite={relatedSite?}
        //  &dateRestrict={dateRestrict?}
        //  &lowRange={lowRange?}
        //  &highRange={highRange?}
        //  &searchType={searchType}
        //  &fileType={fileType?}
        //  &rights={rights?}
        //  &imgSize={imgSize?}
        //  &imgType={imgType?}
        //  &imgColorType={imgColorType?}
        //  &imgDominantColor={imgDominantColor?}
        //  &alt=json';

        $searchURLBase = 'https://www.googleapis.com/customsearch/v1?key=%s&cx=%s&q=%s&num=%d&start=%d&filter=1';

        $api_key = get_field('google_search_api_key');
        $site_key = get_field('google_search_site_key');
        $searchURL = sprintf($searchURLBase, $api_key, $site_key, $searchQuery, $pageSize, $start);

        $search = [
            'term' => $searchQuery,
            'pagelinks' => [],
        ];

        if (strlen($searchQuery) > 0) {
        
            $http_results = wp_remote_get($searchURL, [ 'timeout' => 120, 'httpversion' => '1.1' ]);
                
            if ( $http_results && $http_results['response']['code'] == 200) {
                $results = json_decode($http_results['body']);

                if (!empty($results->searchInformation) && (int)($results->searchInformation->totalResults) == 0) {
                    $search['no_matches'] = true;
                }
                else if ( is_object($results) && isset($results->items) && is_array($results->items) && !empty($results->items) ) {
                    $search['results'] = $resultList = $results->items;

                    //calculate page information
                    $search['count'] = $recordCount = ( isset($results->searchInformation->totalResults) ) ? intval($results->searchInformation->totalResults) : 0;
                    $search['no_matches'] = ($recordCount == 0);
                    $search['page_count'] = ( $recordCount > 0 ) ? intval( ( $recordCount + $pageSize - 1) / $pageSize ) : 1;
                    
                    $search['prev'] = ($currentPage > 1) ? [
                        'link' => '/search/?searchquery=' . $searchQuery . '&amp;sp=' . ($currentPage - 1),
                    ] : null;

                    $search['next'] = ($currentPage < $search['page_count']) ? [
                        'link' => '/search/?searchquery=' . $searchQuery . '&amp;sp=' . ($currentPage + 1),
                    ] : null;
                    

                    if ($search['page_count'] > 1) {
                        
                        $pageIdx = 1;
                        
                        while ($pageIdx <= $search['page_count'] && $pageIdx < 11) {
                            
                            $search['pagelinks'][] = [
                                'num' => $pageIdx,
                                'link' => '/search/?searchquery=' . $searchQuery . '&amp;sp=' . $pageIdx,
                                'class' => ( $currentPage == $pageIdx ) ? 'active' : '',
                            ];

                            $pageIdx++;
                        }
                    }
                } else {
                    $search['error'] = true;
                }
            } else {
                $search['error'] = true;
            }

            $this->context['search'] = $search;
        
        }

    }
}


$view = new SearchResultsView( array('search.twig') );
$view->render();