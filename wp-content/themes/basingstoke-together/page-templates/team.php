<?php
/**
 * Template Name: Team
 *
 * Description: Team
 */

class TeamView extends BasingstokeBaseView {

	/*
	 * Override this method in an extended class to add additional custom data to the context, or modify the context itself.
	 */
	public function post_process(){
		parent::post_process();

		/**
		 * Get team member data for ease of reference.
		 */
		$teamMemberRows = get_field('team_members', $this->context['page']->ID);

		if ( is_array( $teamMemberRows ) ) {
			$teamMembers = array_map( function( $a ){ return $a['team_member']; }, $teamMemberRows);
			$this->context['team_members'] = new TimberPostsCollection($teamMembers);
		}

	}

}

$view = new TeamView( array('team.twig') );
$view->render();
