<?php
/**
 * Template Name: Jobs List
 *
 * Description: Jobs List
 */

class JobsListView extends BasingstokeBaseView {

	public function post_process(){
		
		parent::post_process();

		/*
		 * Always ensure 'Send a story' banner and latest tweet are shown.
		 */
		$this->context['rightcol']['job_listing'] = true;

		/**
		 * Get all 'Job Vacancy Category' taxonomy terms. Hide Empty terms, and only get top levels ones!
		 */
		$this->context['categories'] = get_terms('vacancy_category', array(
			'hide_empty' => true,
			'hierarchical'  => false, 
			'parent' => 0,
		));

		/**
		 * Get a current category ref.
		 */
		$currentCategory = (get_query_var('by_category')) ? strtolower( get_query_var('by_category') ) : 'all';
		$this->context['current_category'] = $currentCategory;

		/**
		 * Get a current page ref.
		 */
		$currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$this->context['current_page'] = $currentPage;

		$qryArgs = array(
			'post_type' => 'job_vacancy',
			'posts_per_page' => 6,
			'paged' => $currentPage,
			'tax_query' => [],
			'meta_query' => [
				[
					'key' => 'application_date',
					'value' => date('Ymd'),
					'compare' => '>=',
				]
			],
		);

		if ($currentCategory !== 'all') {

			$qryArgs['tax_query'][] = array(
				'taxonomy' => 'vacancy_category',
				'field' => 'slug',
				'terms' => $currentCategory,
			);
		}

		$vacancies = Timber::get_posts($qryArgs, 'JobVacancyPost' );

		/* THESE LINES ARE CRUCIAL */
	    /* in order for WordPress to know what to paginate */
	    /* your args have to be the default query */
		query_posts($qryArgs);

		$this->context['pagination'] = Timber::get_pagination();		
		$this->context['vacancies'] = $vacancies;
	}

}

$view = new JobsListView( array('jobs-list.twig') );
$view->render();