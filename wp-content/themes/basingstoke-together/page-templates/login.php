<?php

class LoginView extends BasingstokeBaseView {

	private $_redir;
	private $_nonceName = 'login_nonce';
	private $_nonceAction = 'ealing_business_login';

	private function get_redir(){

		if (!$this->_redir){

			$redir = site_url('business/home/');

			if ( array_key_exists('redir', $_GET) && strlen($_GET['redir']) > 0 ) {
				$redir = urldecode( $_GET['redir'] );
			}

			$this->_redir = $redir;
		}
		return $this->_redir;
	}

	/**
	 * Function to check if the login form was submitted and to process it accordingly.
	 */
	private function check_login(){
		if ( isset( $this->context['form_data']['form_name'] ) && $this->context['form_data']['form_name'] == 'login' ) {
			
			/**
			 * Check nonce is valid first!
			 */
			if ( !isset( $_POST[$this->_nonceName] ) || !wp_verify_nonce( $_POST[$this->_nonceName], $this->_nonceAction ) ) {
				$this->context['form_errors']['invalid_nonce'] = 'An invalid nonce value was provided. Please try again.';
			}

			if ( !is_email( $this->context['form_data']['login_email'] ) ) {
				$this->context['form_errors']['login_email'] = 'Please enter a valid email address.';
			}
			
			if ( !isset( $this->context['form_data']['login_password'] ) || strlen( $this->context['form_data']['login_password'] ) < 1 ) {
				$this->context['form_errors']['login_password'] = 'Please enter your password.';
			}

			if ( empty($this->context['form_errors']) ) {

				$email = $this->context['form_data']['login_email'];
				$password = $this->context['form_data']['login_password'];
				$user = get_user_by( 'email', $email );
				$suspended = false;


				if ( $user && wp_check_password( $password, $user->data->user_pass, $user->ID) ) {

					// We have a valid user. Let's check if it is a Business User, and if so, whether it is 'suspended' or not.
					if ( in_array('business_user', $user->roles) ) {
						$accountStatus = get_user_meta( $user->ID, 'account_status', true );
						$suspended = ( $accountStatus == 'suspended' );
					}

					if ($suspended == true) {
						$this->context['form_errors']['invalid_credentials'] = 'This account has been suspended. Please contact us.';
					} else {
						wp_clear_auth_cookie();
					    wp_set_current_user( $user->ID );
					    wp_set_auth_cookie( $user->ID );

					    wp_redirect( $this->get_redir(), 302 );
						exit;
					}

				} else {
					$this->context['form_errors']['invalid_credentials'] = 'Invalid login details provided. Please double check the email address and password entered.';
				}

			}
			
		}
	}

	public function post_process(){
		
		parent::post_process();

		/**
		 * Already logged in? If so lets just send you to your destination (or business home if not set).
		 */
		if ( $this->context['user']['logged_in'] == true ) {
			wp_redirect( $this->get_redir(), 302 );
			exit;
		}

		/**
		 * Get nonce markup as string to inject in tpl. (echos by default, which we don't want!).
		 */
		$this->context['nonce'] = wp_nonce_field($this->_nonceAction, $this->_nonceName, true, false);

		$this->context['action'] = $this->context['page']->link() . '?redir=' . urlencode( $this->get_redir() );

		$this->check_login();
	}

}

$view = new LoginView( array('login.twig') );
$view->render();