<?php
/**
 * Template Name: Contact Us
 *
 * Description: Contact Us
 */

class ContactUsView extends BasingstokeBaseView {

	/*
	 * Override this method in an extended class to add additional custom data to the context, or modify the context itself.
	 */
	public function post_process(){
		parent::post_process();

		/**
		 * Get team member data for ease of reference.
		 */
		$teamMemberRows = get_field('team_members', $this->context['page']->ID);

		if ( is_array( $teamMemberRows ) ) {
			$teamMembers = array_map( function( $a ){ return $a['team_member']; }, $teamMemberRows);
			$this->context['team_members'] = new TimberPostsCollection($teamMembers);
		}

		/**
		 * Try and process the form (if neccessary).
		 */
		$this->check_submission();
	}

	/**
	 * Function to check if the contact form was submitted and to process it accordingly.
	 */
	private function check_submission(){

		if ( isset( $this->context['form_data']['form_name'] ) && $this->context['form_data']['form_name'] == 'contact' ) {

			/**
			 * Check recaptcha.
			 */
			if ( empty( $this->context['form_data']['g-recaptcha-response'] ) || !$this->check_recaptcha( $this->context['form_data']['g-recaptcha-response'] ) ) {
                $this->context['form_errors']['submit_content_recaptcha'] = 'Please verify you\'re not a robot.';
            }

			/**
			 * Ensure name is provided.
			 */
			if ( ! $this->required_input('contact_name', 2) ) {
				$this->context['form_errors']['contact_name'] = 'Please enter your name.';
			}

			/**
			 * Ensure an email address is supplied. If it is, ensure it is a real email address too!
			 */
			if ( ! $this->required_input('contact_email', 3) ) {
				$this->context['form_errors']['contact_email'] = 'Please enter your email address.';
			} else if ( !is_email( $this->context['form_data']['contact_email'] ) ) {
				$this->context['form_errors']['contact_email'] = 'Please enter a valid email address.';
			}

			/**
			 * Ensure query is provided.
			 */
			if ( ! $this->required_input('contact_query', 5) ) {
				$this->context['form_errors']['contact_query'] = 'Please enter your query.';
			}

			if ( empty($this->context['form_errors']) ) {
				/**
				 * No errors so lets check no other user's have the same email before continuing.
				 */

				$name = $this->context['form_data']['contact_name'];
				$telephone = ( strlen($this->context['form_data']['contact_telephone']) > 0) ? $this->context['form_data']['contact_telephone'] : 'Not specfied' ;
				$email = $this->context['form_data']['contact_email'];
				$query = nl2br($this->context['form_data']['contact_query']);

				/**
				 * Email headers can be re-used for multiple emails.
				 */
            	$headers[] = 'From: Basingstoke Together <no-reply@basingstoketogether.co.uk>' . "\r\n";

				/**
				 * Send notifcation to Basingstoke Together admin.
				 */
            	$message = '<p>There has been a contact submission via the Basingstoke Together website. Details are as follows:</p>';
            	$message .= '<p>Name: ' . $name . '</p>';
            	$message .= '<p>Email Address: ' . $email . '</p>';
            	$message .= '<p>Telephone: ' . $telephone . '</p>';
            	$message .= '<p>Query: ' . $query . '</p>';

            	wp_mail( get_option( 'admin_email' ), 'Basingstoke Together - Contact Submission', $message, $headers );

            	$this->context['form_success'] = true;
			}

		}
	}

	/**
	 * Check a form field meets length critera.
	 * @param string $field
	 *   The field name to check.
	 *
	 * @param string $minLength
	 *   The minimum length to enforce (defaults to 1).
	 *
	 * @return bool
	 *   Whether the fields meets the min-length requirement.
	 */
	private function required_input($field, $minLength = 1){
		return ( isset( $this->context['form_data'][$field] ) && strlen( $this->context['form_data'][$field] ) >= $minLength );
	}

	/**
     * Check whether the recaptcha entered was valid
     * @param  String $recaptcha_response     A recaptcha response
     * @return Boolean                        Whether or not it was valid
     */
    public function check_recaptcha( $recaptcha_response ) {

        $url_string = vsprintf(
            'https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s',
            array(
                RECAPTCHA_SECRET,
                $recaptcha_response,
                $_SERVER['REMOTE_ADDR'],
            )
        );

        $response = json_decode( file_get_contents( $url_string ) );

        return $response->success;

    }

}

$view = new ContactUsView( array('contact.twig') );
$view->render();
