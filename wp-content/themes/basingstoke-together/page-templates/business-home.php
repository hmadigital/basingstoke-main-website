<?php
/**
 * Template Name: Basingstoke Together Home
 *
 * Description: Basingstoke Together Home
 */

class BasingstokeTogetherHomeView extends BasingstokeBaseView {

	public function post_process(){	

		parent::post_process();

		/**
		 * Get associated 'Banners' custom posts.
		 */
		if ( isset( $this->context['page']->rotating_banners ) && !empty( $this->context['page']->rotating_banners ) ) {

			$this->context['banners'] = Timber::get_posts( array(
			    'post_type' => 'homepage_banner',
			    'nopaging' => true,
			    'orderby' => 'post__in', 
			    'post__in' => $this->context['page']->rotating_banners,
			), 'HomepageBannerPost' );
			
		}

		/**
		 * Get latest news article.
		 */
		$newsArticles = Timber::get_posts( array(
		    'post_type' => 'news_article',
		    'posts_per_page' => 1,
		    'paged' => 1,
		), 'NewsArticlePost' );

		if ( is_array( $newsArticles ) && !empty($newsArticles) ) {
			$this->context['latest_news'] = $newsArticles[0];
		}

		        /**
         * Get latest event.
         */
        $today = new DateTime();
        $events = Timber::get_posts( array(
            'post_type' => 'event',
            'posts_per_page' => 1,
            'paged' => 1,
            'meta_query' => [
                [
                    'key' => 'event_section',
                    'value' => $this->context['site_section'],
                ],
                [
                    'key' => 'start_date',
                    'value' => $today->format('Ymd'),
                    'compare' => '>='
                ],
            ],
            'meta_key' => 'start_date',
            'orderby' => 'meta_value_num',
            'order' => 'ASC',
        ), 'EventPost' );

		if ( is_array( $events ) && !empty($events) ) {
			$this->context['latest_event'] = $events[0];
		}

		
	}

}

$view = new BasingstokeTogetherHomeView( array('business-home.twig') );
$view->render();