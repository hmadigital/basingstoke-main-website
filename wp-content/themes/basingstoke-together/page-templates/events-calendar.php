<?php
/**
 * Template Name: Events Calendar
 *
 * Description: Events Calendar
 */

class EventsCalendarView extends BasingstokeBaseView {

    /**
     * Returns the last day in the current month
     * @param  Num      $month
     * @param  Num      $year
     * @return String   Nuber of days in month
     */
    public function get_number_of_days_in_month($month, $year) {
        // Using first day of the month, it doesn't really matter
        $date = $year . '-' . $month . '-1';
        return date('t', strtotime($date));
    }

    public function post_process(){

        /**
         * Ensure the base class post_process method gets called.
         */
        parent::post_process();

        /**
         * Get all 'Story Foci' taxonomy terms. Hide Empty terms, and only get top levels ones!
         */
        $eventFoci = get_terms('event_focus', array(
            'hide_empty' => false,
            'hierarchical'  => false,
            'parent' => 0,
        ));

        $event_legend = array();

        if ( !empty( $eventFoci ) && !is_wp_error( $eventFoci ) ){

            foreach ( $eventFoci as $event => $foci ) {

                $colour = get_field( 'colour', 'event_focus_' . $foci->term_id );

                if ( !empty($colour) ) {

                     $event_legend[$foci->term_id] = array(
                        'name' => $foci->name,
                        'colour' => $colour
                    );

                }

            }

        }

        if ( !empty($event_legend) && count($event_legend) ) {

            $this->context['event_legend'] = array_chunk($event_legend, 4);

        }

        /**
         *
         * This is the Calendar.
         *
         * First we set up a daterange to iterate over, day by day,
         * either based on valid eventmonth and eventyear get variables,
         * or falling back to current month and year. We need first the first and last dates
         * of the months we have chosen, then set up the date range, then iterate over the dates.
         * Whilst iterating over the dates, if there is a valid event for that date
         * we will add it to a results array.
         *
         * Once this is done, we then pad the beginning of the results array so it always
         * starts on a Monday. The we chunk the array into blocks of , representing weeks,
         * and pad the end of the last array so it ends on a Saturday, giving us either 4, 5, or
         * possibly(?) 6 full arrays of 7 days to iterate over in twig to make a lovely calendar.
         *
         * Last thing to do is work out the previous month and next month buttons with a little logic.
         *
         * Boom.
         *
         */

        $begin = 0;
        $end = 0;

        // try and set up $begin and $end
        if (isset($_GET['eventmonth']) && is_numeric($_GET['eventmonth']) && isset($_GET['eventyear']) && is_numeric($_GET['eventyear'])) {

            $month = $_GET['eventmonth'];
            $year = $_GET['eventyear'];

            $daysInMonth = $this->get_number_of_days_in_month($month, $year);

            $begin = new DateTime( $year . '-' . $month . '-01' );
            $end = new DateTime( $year . '-' . $month . '-' . $daysInMonth . ' 23:59:59' );
        }

        if (!($begin instanceof DateTime) || !($end instanceof DateTime)) {

            $month = date('m');
            $year = date('Y');

            $daysInMonth = $this->get_number_of_days_in_month($month, $year);

            $begin = new DateTime( $year . '-' . $month . '-01' );
            $end = new DateTime( $year . '-' . $month . '-' . $daysInMonth . ' 23:59:59' );
        }

        /**
         * DateRange
         */
        $interval = new DateInterval('P1D');
        $daterange = new DatePeriod($begin, $interval, $end);

        /**
         * Get 'Events' custom posts too.
         */
        $eventsArgs = array(
            'post_type' => 'event',
            'nopaging' => true,
            'meta_key' => 'start_date',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
            'meta_query' => [
                'relation' => 'AND',
                [
                    'key' => 'event_section',
                    'value' => $this->context['site_section'],
                ],
                [
                    'key' => 'start_date',
                    'value' => $begin->format('Ymd'),
                    'compare' => '>='
                ],
                [
                    'relation' => 'OR',
                    [
                        'key' => 'end_date',
                        'value' => $end->format('Ymd'),
                        'compare' => '<='
                    ],
                    [
                        'key' => 'end_date',
                        'compare' => 'NOT EXISTS'
                    ],
                ],
            ],
        );

        $events = Timber::get_posts($eventsArgs, 'EventPost' );

        $repeatEventsArgs = array(
            'post_type' => 'event',
            'nopaging' => true,
            'meta_key' => 'start_date',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
            'meta_query' => [
                'relation' => 'AND',
                [
                    'key' => 'event_section',
                    'value' => $this->context['site_section'],
                ],
                [
                    'key' => 'day_repeater',
                    'value' => '',
                    'compare' => '!=',
                ],
            ],
        );

        $repeatEvents = Timber::get_posts( $repeatEventsArgs, 'EventPost' );

        $events = array_unique( array_merge( $events, $repeatEvents ) );

        /**
         * results array to pish to
         */
        $results = array();

        $today = new DateTime();

        /**
         * Iterate over daterange, day by day, and push any events to the results array as long as they're in the future
         */
        foreach ($daterange as $date) {

            $day = array(
                'date' => $date,
                'events' => array(),
            );

            $day_of_the_week = $date->format('D');

            // only add events to $day if they're in the future
            if ( $date >= $today ) {
    
                foreach ($events as &$event) {

                    $startDate = get_field('start_date', $event->ID);
                    $endDate = get_field('end_date', $event->ID);

                    $eventStart = DateTime::createFromFormat('Ymd H:i:s', $startDate . ' 00:00:01', new DateTimeZone('UTC'));
                    $eventEnd = DateTime::createFromFormat('Ymd H:i:s', $endDate . ' 00:00:01', new DateTimeZone('UTC'));

                    // set valid day of the week to true if either a) day repeater isn't set for that event, or b) the event occurs on that day
                    $valid_day_of_the_week = ( !is_array($event->day_repeater) || in_array($day_of_the_week, array_values( $event->day_repeater ) ) ) ? true : false;

                    // if the day is the start date or between the start and the end dates and a valid date
                    if ( ( $date->format('Ymd') === $eventStart->format('Ymd') || ( $date >= $eventStart && $date <= $eventEnd ) ) && $valid_day_of_the_week ) {

                        $event_legend_item = array_key_exists($event->main_focus, $event_legend ) ? $event_legend[$event->main_focus] : null;

                        $event->bgcolour = is_array($event_legend_item) ? $event_legend_item['colour'] : '';
                        $day['events'][] = $event;

                    }

                }

            }

            $results[] = $day;

        }

        /**
         * Get date as a number between 0 and 6 (Sunday to Saturday)
         */
        $firstDay = $results[0]['date']->format('w');

        /*

        If we want to start on Monday instead of Sunday, use this!

        // put Sunday to the end, then take away 1 because we want a Monday to Sunday calendar
        if ($firstDay == 0) {

            $firstDay = 7;

        }

        $firstDay = $firstDay - 1;

        */

        /**
         * For every day after monday, add 1 empty item to the beginning of the array to pad it for the calendar
         */
        for ($i = 1; $i <= $firstDay; $i++) {

            array_unshift($results, '');

        }

        foreach ($results as &$result) {

            if (isset($result['date'])) {

                $result['day'] = $result['date']->format('d');

            }

        }

        /**
         * Chunk array into sets of 7 (for 7 day weeks)
         */
        $weeks = array_chunk($results, 7);

        /**
         * Get last week index
         */
        $weeksLength = count($weeks) - 1;

        /**
         * work out if the last week has a full 7 days in it -- if it doesn't, pad it out to 7
         */

        $lastWeekLength = count($weeks[$weeksLength]);

        if ($lastWeekLength < 7) {

            for ($i = 0; $i < (7 - $lastWeekLength); $i++) {

                $weeks[$weeksLength][] = '';

            }

        }

        $this->context['weeks'] = $weeks;


        /**
         * Now that the calendar is set up, we need to set up current month info,
         * as well as next and prev month buttons.
         */

        $currentMonth = array(
            'month_string' => date('F', strtotime(date('Y-' . $month . '-01'))),
            'year' => $year,
        );

        $this->context['current_month'] = $currentMonth;



        $prevMonth = array(
            'month' => $month - 1,
            'year' => $year,
        );

        $nextMonth = array(
            'month' => $month + 1,
            'year' => $year,
        );

        if ($prevMonth['month'] == 0) {
            $prevMonth['month'] = 12;
            $prevMonth['year']--;
        }

        if ($nextMonth['month'] == 13) {
            $nextMonth['month'] = 1;
            $nextMonth['year']++;
        }

        $this->context['prev_month'] = $prevMonth;
        $this->context['next_month'] = $nextMonth;

        $this->context['calendar_url'] = get_permalink( $this->context['page']->ID );


        /**
         * Days of the week
         * Like this in case we decide to change between starting on Monday or on Sunday: makes it simpler
         */

        $this->context['days_of_the_week'] = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');

    }
}

$view = new EventsCalendarView( array('events-calendar.twig') );
$view->render();
