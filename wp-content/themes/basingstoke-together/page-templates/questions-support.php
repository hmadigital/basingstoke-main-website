<?php

class QuestionsSupportView extends BasingstokeBaseView {

	public function post_process(){	

		/**
		 * Ensure the base class post_process method gets called.
		 */
		parent::post_process();

		$this->context['groups'] = get_field( 'question_groups', $this->context['page']->ID );
	}
}

$view = new QuestionsSupportView( array('questions-support.twig') );
$view->render();