<?php

class DownloadGroups {

	public static function getGroups($pageID = 0, $archived = false) {

		if ($pageID > 0) {

			// get time the posts were last changed
			$last_changed = wp_cache_get( 'last_changed', 'posts' );

			// build a unique cache key
			$key = md5( 'download_groups_page_' . $pageID . '_' . ( ($archived) ? 'archived' : 'not_archived') );
			$key = "download_groups:$key:$last_changed";

			//try and get our data from cache to start with
			if ( ! $results = wp_cache_get( $key, 'download_groups' ) ) {

				/**
				 * What will be our final, 'friendly' group array for use in the twig tpl.
				 */
				$groups = array();

				/**
				 * We'll query all download_groups CPTs by this array of IDs.
				 */
				$groupIDs = array();

				// if no luck from cache, lets query the db.
				$groupFieldKey = ($archived) ? 'archived_download_groups' : 'current_download_groups';
				$groupRows = get_field( $groupFieldKey, $pageID );

				foreach ($groupRows as $key => $row) {
					$newItemID = ( array_key_exists( 'group' , $row ) ) ? $row['group']->ID : 0;
					$newItemTitle = ( array_key_exists( 'group' , $row ) ) ? $row['group']->post_title : '';
					$newItemDate = ( $archived && array_key_exists( 'archive_date', $row ) ) ? $row['archive_date'] : null;

					if ($newItemID > 0) {

						array_push( $groupIDs , $newItemID );

						$groups[] = array(
							'id' => $newItemID,
							'title' => $newItemTitle,
							'archived' => $archived,
							'archivedate' => $newItemDate,
							'files' => array(),
						);
					}
				}

				/**
				 * Query for all download_groups matching previous group IDs.
				 */
				$cptGroups = Timber::get_posts(array(
					'post_type' => 'download_group',
					'no_paging' => true,
					'post__in' => $groupIDs,
				) );

				/**
				 * Populate the file array for each group now we have CPTs.
				 */
				foreach ($groups as $idx => $group) {

					$cptMatch = null;

					// Get the matching CPT item first.
					foreach ($cptGroups as $cptGroup) {
						if ( $cptGroup->ID == $group['id'] ) {
							$cptMatch = $cptGroup;
						}
					}

					if ($cptMatch) {

						$files = array();
						foreach ( get_field('files', $cptMatch->ID) as $key => $value ) {
							$file = $value['file'];
							$file['thumbnail'] = $value['thumbnail'];
							$files[] = $file;
						}

						//loop over each file to set size and extension
						foreach ($files as $fileIdx => $file) {

							// work out file extension
							$ext = substr(strrchr( $file['url'],'.'),1);

							$fileRef = get_attached_file( $file['id'] );

							// work out file size!
							$size = (file_exists($fileRef)) ? filesize( $fileRef ) : 0;

							//set our keys for access in twig!
							$files[$fileIdx]['extension'] = $ext;
							$files[$fileIdx]['size'] = $size;
							$files[$fileIdx]['size_formatted'] = size_format($size, 1);
							$files[$fileIdx]['thumbnail'] = (!empty($files[$fileIdx]['thumbnail'])) ? new TimberImage($files[$fileIdx]['thumbnail']) : null;
						}

						$groups[$idx]['files'] = $files;
					}

				}

				$results = $groups;


				wp_cache_set( $key, $results, 'download_groups' );
			}

			if ( $results ) {
				return $results;
			}

		} else {
			return array();
		}



	}

}

class DownloadsView extends BasingstokeBaseView {

	public function post_process(){

		/**
		 * Ensure the base class post_process method gets called.
		 */
		parent::post_process();

		/**
		 * Check if we're in 'archive' mode.
		 */
		$archived = ( array_key_exists('archived', $_GET) && strlen($_GET['archived']) > 0 ) ? true : false;
		$this->context['archived'] = $archived;

		/**
		 * Get page specific file groups.
		 */
		$this->context['groups'] = DownloadGroups::getGroups( $this->context['page']->ID, $archived );
	}
}

$view = new DownloadsView( array('downloads.twig') );
$view->render();