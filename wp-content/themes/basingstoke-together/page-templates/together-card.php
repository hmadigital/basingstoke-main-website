<?php
/**
 * Template Name: Together Card
 *
 * Description: Together Card
 */

class TogetherCardView extends BasingstokeBaseView {

    public function post_process(){

        parent::post_process();

        $this->get_businesses_list();
        $this->get_mailchimp_interests();
        $this->process_form_submission();
    }


    private function get_businesses_list() {

        $this->context['businesses'] = TimberHelper::transient('businesses', function(){

            $qryArgs = array(
                'post_type' => 'business',
                'order' => 'ASC',
                'orderby' => 'title',
                'posts_per_page' => -1,
            );

            $business_posts = Timber::get_posts( $qryArgs, 'BusinessPost' );

            $businesses = array_map(function($item) {
                return [
                    'id' => $item->id,
                    'name' => $item->post_title,
                ];
            }, $business_posts);

            return $businesses;

        }, 2 * HOUR_IN_SECONDS);

    }

    private function get_mailchimp_interests() {

        $this->context['interests'] = TimberHelper::transient('interests', function(){

            // API CALL TO GET INTERESTS
            $mcClient = new \HMA\MailChimpV3( '66db84ad1a3d77f18fb8a267777a1b6c-us18' );
            $interests_data = $mcClient->get('lists/bb672c6603/interest-categories/635ea60d99/interests');
            $interests = [];

            if (is_array($interests_data) && array_key_exists('interests', $interests_data)) {
                $interests = array_map(function($item) {
                    return [
                        'id' => $item['id'],
                        'label' => $item['name'],
                    ];
                }, $interests_data['interests']);

            }

            $this->context['interests'] = $interests;

            return $interests;

        }, 2 * HOUR_IN_SECONDS);
    }

    private function process_form_submission() {

        \Stripe\Stripe::setApiKey( $this->context['page']->stripe_secret_key );

        $this->context['stripe'] = [
            'pubkey' => $this->context['page']->stripe_public_key
        ];

        $user_interests = [];
        $user_interests_as_string = "";

        if (array_key_exists('promo_interest', $this->context['form_data']) && is_array($this->context['form_data']['promo_interest']) && !empty($this->context['form_data']['promo_interest'])){
            $user_interests = $this->context['form_data']['promo_interest'];
        }

        if (!empty($user_interests)){

            $interest_values = [];

            foreach($user_interests as $item) {
                foreach ($this->context['interests'] as $interest_item){
                    if ($interest_item['id'] == $item){
                        $interest_values[] = $interest_item['label'];
                    }
                }
            }

            $user_interests_as_string = implode(", ", $interest_values);
            $this->context['form_data']['user_interests'] = $user_interests_as_string;
        }

        if ( empty( $this->context['form_errors'] ) ) {

            if (isset( $this->context['form_data']['form_name'] ) &&
                $this->context['form_data']['form_name'] == 'payment' &&
                isset( $this->context['form_data']['payment_intent_id'])) {

                $this->context['form_success'] = TogetherCard::mark_card_as_unconfirmed($this->context['form_data']['payment_intent_id']);
            }
            else if ( isset( $this->context['form_data']['form_name'] ) && $this->context['form_data']['form_name'] == 'together_card' ) {
                $payment_required = $this->context['form_data']['user_type'] == "user_public";

                // Form Submit
                // If already has a payment_intent_id process form as an edit
                if (!empty($this->context['form_data']['payment_intent_id']) && $payment_required){
                    $this->context['show_card_details'] = $this->validate_form() && $this->handle_edit_intent();
                }
                // Else treat it as a brand new submission
                else if ($payment_required){
                    $this->context['show_card_details'] = $this->validate_form() && $this->handle_new_intent();
                }
                else{
                    $this->context['form_success'] = $this->validate_form() && $this->handle_new_card();
                }

            } else if (( isset( $this->context['form_data']['form_name'] ) && $this->context['form_data']['form_name'] == 'together_card_edit' ) &&
                         !empty($this->context['form_data']['payment_intent_id'])) {

                // Load the intent details from stripe
                $this->handle_load_intent_form($this->context['form_data']['payment_intent_id']);
            }
        }

    }

    /**
     * Retrieve and repopulate the form data with the stripe intent
     */
    private function handle_load_intent_form ($intent_id) {
        if (empty($intent_id)) return false;
        try{
            $intent = \Stripe\PaymentIntent::retrieve($intent_id);
            $this->context['form_data']['payment_intent_id'] =  $intent_id;
            $this->context['form_data']['first_name']        =  $intent->metadata->first_name;
            $this->context['form_data']['last_name']         =  $intent->metadata->last_name;
            $this->context['form_data']['email']             =  $intent->metadata->email;
            $this->context['form_data']['user_type']         =  $intent->metadata->user_type;
            $this->context['form_data']['user_interests']    =  $intent->metadata->user_interests;
            $this->context['form_data']['address_line_1']    =  $intent->metadata->address_line_1;
            $this->context['form_data']['address_line_2']    =  $intent->metadata->address_line_2;
            $this->context['form_data']['address_town']      =  $intent->metadata->address_town;
            $this->context['form_data']['address_postcode']  =  $intent->metadata->address_postcode;
            $this->context['form_data']['town_city']         =  $intent->metadata->town_city;

        } catch ( Exception $e ) {
            $this->handle_stripe_exception($e);
        }
    }

    /**
     * Handle process form for a new card no payment needed.
     *
     * @return void
     */
    private function handle_new_card() {
        if (TogetherCard::process_to_form_submission($this->context['form_data']) &&
            TogetherCard::send_emails($this->context['form_data'])) {
            return true;
        }
        return false;
    }

    /**
     * Handle the process of generating a new stripe
     *
     * @return void
     */
    private function handle_new_intent() {
        try {
            $intent = \Stripe\PaymentIntent::create([
                'amount' => 200,
                'currency' => 'gbp',
                'description' => implode( ' ', [
                    'Personal Together Card',
                    $this->context['form_data']['email'],
                ] ),
                'metadata' => $this->process_meta_data(),
            ]);

            //send intent back to payment screen
            if ( $intent != null ){
                $this->context['stripe_intent_secret'] = $intent->client_secret;
                $this->context['stripe_intent_id'] = $intent->id;
                $this->context['stripe_intent'] = $intent;
                $this->context['form_data']['payment_intent_id'] = $intent->id;
                TogetherCard::process_to_form_submission($this->context['form_data']);
                return true;
            }
        }
        catch(Exception $e) {
            $this->handle_stripe_exception($e);
        }
    }

    private function handle_edit_intent () {

        try {
            $intent = \Stripe\PaymentIntent::update(
                $this->context['form_data']['payment_intent_id'],
                [
                'description' => implode( ' ', [
                    'Personal Together Card',
                    $this->context['form_data']['email'],
                ] ),
                'metadata' => $this->process_meta_data(),
            ]);

            //send intent back to payment screen
            if ( $intent != null ){
                $this->context['stripe_intent_secret'] = $intent->client_secret;
                $this->context['stripe_intent_id'] = $intent->id;
                $this->context['stripe_intent'] = $intent;

                return true;
            }
        }
        catch(Exception $e) {
            $this->handle_stripe_exception($e);
        }
    }

    /**
     * Process Meta ready for Stripe Payment Intent
     *
     * @return Array
     */
    private function process_meta_data () {
        return [
            'first_name'       =>  $this->context['form_data']['first_name'],
            'last_name'        =>  $this->context['form_data']['last_name'],
            'email'            =>  $this->context['form_data']['email'],
            'email_subscribe'  =>  $this->context['form_data']['email_subscribe'],
            'user_type'        =>  $this->context['form_data']['user_type'],
            'user_interests'   =>  $this->context['form_data']['user_interests'],
            'address_line_1'   =>  $this->context['form_data']['address_line_1'],
            'address_line_2'   =>  $this->context['form_data']['address_line_2'],
            'address_town'     =>  $this->context['form_data']['address_town'],
            'address_postcode' =>  $this->context['form_data']['address_postcode'],
            'town_city'        =>  $this->context['form_data']['town_city'],
        ];
    }


    private function validate_form() {
        $email = array_key_exists('email', $this->context['form_data']) ? strtolower(trim($this->context['form_data']['email'])) : '';

        if ( empty( $this->context['form_data']['g-recaptcha-response'] ) || !$this->check_recaptcha( $this->context['form_data']['g-recaptcha-response'] ) ) {
            $this->context['form_errors']['submit_content_recaptcha'] = 'Please verify you\'re not a robot.';
        }

        /**
         * Ensure name is provided.
         */
        if ( !$this->required_input('first_name', 2) ) {
            $this->context['form_errors']['first_name'] = 'Please enter your first name.';
        }
        if ( !$this->required_input('last_name', 2) ) {
            $this->context['form_errors']['last_name'] = 'Please enter your last name.';
        }

        /**
         * Ensure an email address is supplied. If it is, ensure it is a real email address too!
         */
        if ( !$this->required_input('email', 3) ) {
            $this->context['form_errors']['email'] = 'Please enter your email address.';
        } else if ( !is_email( $email ) ) {
            $this->context['form_errors']['email'] = 'Please enter a valid email address.';
        } else {
            $existing_submission_for_email = Timber::get_post( array(
                'post_type' => 'form_submission',
                'post_status' => 'any',
                'meta_query' => [
                    'relation' => 'AND',
                    [
                        'key' => 'form_name',
                        'value' => 'together_card'
                    ],
                    [
                        'key' => 'email',
                        'value' => $email
                    ]
                    // And NOT PENDING
                ],
            ));

            if (!empty($existing_submission_for_email)) {
                $sage_pay_status = get_post_meta($existing_submission_for_email->id, 'stripe_status', true);
                $user_type = get_post_meta($existing_submission_for_email->id, 'user_type', true);

                if ((!empty($sage_pay_status) && $sage_pay_status != 'PENDING') ||
                    (!empty($user_type) && $user_type == 'user_business')){
                    $this->context['form_errors']['email'] = 'This email address has already been used. Please use a different email.';
                }
            }
        }

        if ( $this->context['form_data']['user_type'] == "user_business"){

            if ( !$this->required_input('business_name', 2) ) {
                $this->context['form_errors']['business_name'] = 'Please enter your business name.';
            }

        }

        if ( $this->context['form_data']['user_type'] == "user_public"){

            if ( !$this->required_input('address_line_1', 2) ) {
                $this->context['form_errors']['address_line_1'] = 'Please enter your full address.';
            }

            if ( !$this->required_input('address_town', 2) ) {
                $this->context['form_errors']['address_town'] = 'Please enter your full address.';
            }

            if ( !$this->required_input('address_postcode', 2) ) {
                $this->context['form_errors']['address_postcode'] = 'Please enter your postcode.';
            }

        }

        if ( !empty( $this->context['form_errors'] ) ) {
            return false;
        }

        return true;
    }

    /**
     * Check a form field meets length critera.
     * @param string $field
     *   The field name to check.
     *
     * @param string $minLength
     *   The minimum length to enforce (defaults to 1).
     *
     * @return bool
     *   Whether the fields meets the min-length requirement.
     */
    private function required_input($field, $minLength = 1){
        return ( isset( $this->context['form_data'][$field] ) && strlen( $this->context['form_data'][$field] ) >= $minLength );
    }

      /**
     * Check whether the recaptcha entered was valid
     * @param  String $recaptcha_response     A recaptcha response
     * @return Boolean                        Whether or not it was valid
     */
    public function check_recaptcha( $recaptcha_response ) {

        $url_string = vsprintf(
            'https://www.google.com/recaptcha/api/siteverify?secret=%s&response=%s&remoteip=%s',
            array(
                RECAPTCHA_SECRET,
                $recaptcha_response,
                $_SERVER['REMOTE_ADDR'],
            )
        );

        $response = json_decode( file_get_contents( $url_string ) );

        return $response->success;

    }

    /**
     * Capture Stripe Payment Intent API exceptions and display the correct error
     *
     * @param Exception $e
     * @return void
     */
    private function handle_stripe_exception($e) {
        if ($e instanceof \Stripe\Error\RateLimit ) {
            $this->context['form_errors']['error_message'] = 'Too many requests made to the payment gateway too quickly';
        } else if ($e instanceof \Stripe\Error\InvalidRequest) {
            $this->context['form_errors']['error_message'] = 'Invalid parameters were supplied payment gateway';
        } else if ( $e instanceof \Stripe\Error\Authentication) {
            $this->context['form_errors']['error_message'] = 'Authentication with payment gateway failed';
        } else if ( $e instanceof \Stripe\Error\ApiConnection) {
            $this->context['form_errors']['error_message'] = 'Network communication with payment gateway failed';
        } else if ( $e instanceof \Stripe\Error\Base) {
            $this->context['form_errors']['error_message'] = 'Display a very generic error to the user, and maybe send yourself an email';
        } else {
            $this->context['form_errors']['error_message'] = 'An unknown error occured';
        }
    }

}

$view = new TogetherCardView( array('together-card.twig') );
$view->render();
