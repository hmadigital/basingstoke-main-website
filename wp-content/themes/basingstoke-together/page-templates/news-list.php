<?php
/**
 * Template Name: News List
 *
 * Description: News List
 */

class NewsMonthYearQuery {
	
	private $_querySrc = '';

	function __construct(){
		$this->_query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month`, count(ID) as postcount FROM wp_posts WHERE post_type = 'news_article' AND post_status = 'publish' GROUP BY YEAR(post_date), MONTH(post_date) ORDER BY post_date DESC";
	}

	public function get($baseUrl){

		global $wpdb;

		// get time the posts were last changed
		$last_changed = wp_cache_get( 'last_changed', 'posts' );
		
		// build a unique cache key
		$key = md5( $this->_query );
		$key = "month_year_list:$key:$last_changed";

		//try and get our data from cache to start with
		if ( ! $results = wp_cache_get( $key, 'news_articles' ) ) {

			// if no luck from cache, lets query the db.
			$results = $wpdb->get_results( $this->_query );
			wp_cache_set( $key, $results, 'news_articles' );
		}
		if ( $results ) {
			
			$monthsYears = array();

			// tidy up the result :)
			foreach ( (array) $results as $result) {				
				$monthsYears[] = array(
					'name' => $result->month . ' ' . $result->year,
					'month' => $result->month,
					'month_name' => date('F', strtotime('2012-' . $result->month . '-01')),
					'year' => $result->year,
					'slug' => strtolower($result->month . '-' . $result->year),
					'count' => $result->postcount,
					'link' => $baseUrl . $result->year .'/',
				);
			}

			return $monthsYears;
		} else {
			return array();
		}
	}
}

class NewsListView extends BasingstokeBaseView {

	public function post_process(){	

		/**
		 * Ensure the base class post_process method gets called.
		 */
		parent::post_process();

		$link = $this->context['page']->link();

		/**
		 * Get cached reference to distinct month/years for news articles.
		 */
		$myQuery = new NewsMonthYearQuery();
		$this->context['months_years'] = $myQuery->get($link);

		/**
		 * Get current page of news articles and ensure its available in the twig context.
		 */
		$currentPage = (get_query_var('paged')) ? get_query_var('paged') : 1;
		$this->context['current_page'] = $currentPage;
		
		/**
		 * Parse the current story focus from the querystring if present, and add to the twig context.
		 */
		$storyFocus = ( array_key_exists('storyfocus', $_GET) && strlen($_GET['storyfocus']) > 0 ) ? strtolower( sanitize_text_field( $_GET['storyfocus'] ) ) : '';		

		/**
		 * Parse the current month/year from the querystring if present, and add to the twig context.
		 */
		$monthYear = ( array_key_exists('monthyear', $_GET) && strlen($_GET['monthyear']) > 0 ) ? explode( '-', ( sanitize_text_field( $_GET['monthyear'] ) ) ) : 'all';
		$currentMonth = ( is_array($monthYear) && !empty($monthYear) ) ? intval( $monthYear[0] ) : null;
		$currentYear = ( is_array($monthYear) && count($monthYear) > 1) ? intval( $monthYear[1] ) : null;
		
		$this->context['current_month'] = $currentMonth;
		$this->context['current_year'] = $currentYear;

		/**
		 * Get all 'Story Foci' taxonomy terms. Hide Empty terms, and only get top levels ones!
		 */
		$storyFoci = get_terms('story_focus', array(
			'hide_empty' => true,
			'hierarchical'  => false, 
			'parent' => 0,
		));
 		
 		if ( !empty( $storyFoci ) && !is_wp_error( $storyFoci ) ){
 			$this->context['story_foci'] = $storyFoci;
 		}

		/**
		 * Get 'News Articles' custom posts too.
		 */
		$newsArgs = array(
		    'post_type' => 'news_article',
		    'posts_per_page' => 8,
		    'paged' => $currentPage,
		);

		if ( $monthYear !== 'all' && (int)$currentMonth > 0 && (int)$currentYear > 0 ) {
			$newsArgs['monthnum'] = $currentMonth;
			$newsArgs['year'] = $currentYear;
		}

		if ( !empty( $storyFocus ) ) {
			$newsArgs['tax_query'] = array(
				array(
					'taxonomy' => 'story_focus',
					'field' => 'slug',
					'terms' => $storyFocus,
				),
			);
		}

		$articles = Timber::get_posts($newsArgs, 'NewsArticlePost' );

		/* THESE LINES ARE CRUCIAL */
	    /* in order for WordPress to know what to paginate */
	    /* your args have to be the default query */
		query_posts($newsArgs);

		$this->context['pagination'] = Timber::get_pagination();

		
		$this->context['articles'] = $articles;
	}

}

$view = new NewsListView( array('news-list.twig') );
$view->render();