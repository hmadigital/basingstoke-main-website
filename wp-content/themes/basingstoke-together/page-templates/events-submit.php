<?php

class EventsSubmitView extends BasingstokeBaseView {

    private $created_event_id;

    private $test_list = array(
        'string' => '/.+/',
        'email' => '/^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*\.(([0-9]{1,3})|([a-zA-Z]{2,3})|(aero|coop|info|museum|name))$/',
        'date' => '/^(((0[1-9]|[12]\\d|3[01])\\/(0[13578]|1[02])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|[12]\\d|30)\\/(0[13456789]|1[012])\\/((19|[2-9]\\d)\\d{2}))|((0[1-9]|1\\d|2[0-8])\\/02\\/((19|[2-9]\\d)\\d{2}))|(29\\/02\\/((1[6-9]|[2-9]\\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/',
    );

    private $valid_fields = array(
        'event_person' => array(
            'validation' => 'string',
            'error_message' => 'Your name',
        ),
        'event_email' => array(
            'validation' => 'email',
            'error_message' => 'Your email address',
        ),
        'event_phone' => array(
            'validation' => 'string',
            'error_message' => 'Your phone number',
        ),
        'event_name' => array(
            'validation' => 'string',
            'error_message' => 'The name of your event',
        ),
        'event_date_from' => array(
            'validation' => 'date',
            'error_message' => 'When your event starts',
        ),
        'event_date_to' => array(
            'validation' => 'date',
        ),
        'event_address' => array(
            'validation' => 'string',
            'error_message' => 'Your event address',
        ),
        'event_details' => array(
            'validation' => 'string',
            'error_message' => 'Any additional details about your event',
        ),
    );

    private $errors = array();

    private function format_date_for_insert($date_string) {

        $input_format = 'd/m/Y';
        $output_format = 'Ymd';
        $timezone = new DateTimeZone('UTC');

        $date = DateTime::createFromFormat( $input_format, $date_string, $timezone );

        return $date->format($output_format);

    }

    private function validate_form() {

        // valid fields plus one extra for file upload
        $error_count = count($this->valid_fields) + 1;

        foreach ($this->context['form_data'] as $key => $value) {

            if (array_key_exists($key, $this->valid_fields)) {

                $test = $this->test_list[ $this->valid_fields[$key]['validation'] ];

                if (!is_array($test)) {

                    preg_match($test, $value, $matches);

                    if (($value == '' && empty($this->valid_fields[$key]['error_message']) ) || count($matches)) {

                        $error_count--;

                    } else {

                        array_push($this->errors, $this->valid_fields[$key]['error_message']);

                    }

                } else if (in_array($value, $test)) {

                    $error_count--;

                } else {

                    array_push($this->errors, $this->valid_fields[$key]['error_message']);

                }

            }

        }

        if ($_FILES && isset($_FILES['event_thumbnail']) && !$_FILES['event_thumbnail']['error']) {

            $error_count--;

        } else {

            array_push($this->errors, 'Please include a thumbnail image for your event');

        }

        if ($error_count === 0) {

            return true;

        } else {

            return false;

        }

    }

    public function add_event_record() {

        $post = array(
            'post_title'    => $this->context['form_data']['event_name'],
            'post_content'  => $this->context['form_data']['event_details'],
            'post_status'   => 'draft',
            'post_author'   => 5,
            'post_type'     => 'event',
        );

        $this->created_event_id = wp_insert_post( $post );

        if ( $this->created_event_id ) {

            // make sure the thumbnail works

            foreach ($_FILES as $file => $array) {

                // These files need to be included as dependencies when on the front end.
                require_once( ABSPATH . 'wp-admin/includes/image.php' );
                require_once( ABSPATH . 'wp-admin/includes/file.php' );
                require_once( ABSPATH . 'wp-admin/includes/media.php' );

                if ($_FILES[$file]['error'] !== UPLOAD_ERR_OK) {

                    array_push($this->errors, 'upload error : ' . $_FILES[$file]['error']);

                    return false;

                }

                $attach_id = media_handle_upload( $file, $this->created_event_id );

                if (is_numeric($attach_id) && $attach_id > 0) {

                    update_post_meta($this->created_event_id, 'list_image', $attach_id);

                } else {

                    array_push($this->errors, 'Please upload a valid image');

                }

                break;
            }

            if (count($this->errors) > 0) {

                return false;

            }


            // basic info

            $update_id = update_post_meta( $this->created_event_id, 'list_text', $this->context['form_data']['event_name'] );

            update_post_meta( $this->created_event_id, 'main_content', $this->context['form_data']['event_details'] );

            // dates from and til

            $start_date = $this->format_date_for_insert( $this->context['form_data']['event_date_from'] );

            update_post_meta( $this->created_event_id, 'start_date', $start_date );

            if ( isset($this->context['form_data']['event_date_to']) && $this->context['form_data']['event_date_to']) {

                $end_date = $this->format_date_for_insert( $this->context['form_data']['event_date_to'] );

                if (update_post_meta( $this->created_event_id, 'end_date', $end_date ) ) {

                    update_post_meta( $this->created_event_id, 'multiday_event', 'yes' );

                    if ( isset($this->context['form_data']['event_repeater']) && is_array( $this->context['form_data']['event_repeater'] ) ) {

                        update_post_meta( $this->created_event_id, 'day_repeater', $this->context['form_data']['event_repeater'] );

                    } else {

                        array_push($this->errors, 'You must specify the day(s) of the week that your multi-day event occurs on');

                        return false;

                    }

                }

            }

            // event type

            if ( isset($_POST['event_type']) && is_array($_POST['event_type']) ) {

                $event_types = get_terms('event_focus');

                $event_types_array = array();

                foreach ( $event_types as $event_type ) {
                    $event_types_array[] = $event_type->slug;
                }

                $event_type_ids = array();

                foreach ( $this->context['form_data']['event_type'] as $event_type ) {

                    if ( in_array($event_type, $event_types_array) ) {

                        $event_type_ids[] = $event_type;

                    }

                }

                wp_set_object_terms( $this->created_event_id, $event_type_ids, 'event_focus');

            }

            return true;

        }

    }

    public function send_submission_successful_emails() {


        // admin
        $message = '<p>There has been an event submission via the Basingstoke Together website. Details can be found in the Events Pages in the CMS, with the status set as pending.</p>';
        $message .= '<br>';
        $message .= '<p>Details of the person who submit the event information are as follows:' . '</p>';
        $message .= '<br>';
        $message .= '<p>Name: ' . $this->context['form_data']['event_person'] . '</p>';
        $message .= '<br>';
        $message .= '<p>Email Address: ' . $this->context['form_data']['event_email'] . '</p>';
        $message .= '<br>';
        $message .= '<p>Telephone: ' . $this->context['form_data']['event_phone'] . '</p>';
        $message .= '<br>';
        $message .= '<p>Event Title: ' . $this->context['form_data']['event_name'] . '</p>';
        $message .= '<br>';

        wp_mail( get_option('admin_email'), 'Event submission on Basingstoke Together site', $message );


        // event maker
        $message = '<p>Thank you for submitting your event details for the Basingstoke Together website! The details have been sent to the Basingstoke Together team for approval.</p>';

        wp_mail( $this->context['form_data']['event_email'], 'Your event submission to Basingstoke Together', $message );


    }

    public function post_process() {

        parent::post_process();

        if (isset($this->context['form_data']['form_name']) && $this->context['form_data']['form_name'] === 'submit_event') {

            // if the form failed to validate
            if (!$this->validate_form() || !$this->add_event_record()) {

                // get error(s)
                $this->context['errors'] = $this->errors;


                // get previous values for the form
                $previous_values = array();

                foreach ($this->valid_fields as $key => $value) {

                    if (!empty($this->context['form_data'][$key])) {

                        $previous_values[$key] = $this->context['form_data'][$key];

                    }

                }

                $this->context['previous_values'] = $previous_values;


                // delete an intermediate post if it was created during the process
                if ($this->created_event_id) {

                    wp_delete_post($this->created_event_id, true);

                }

            } else {

                $this->send_submission_successful_emails();

                $this->context['record_added'] = true;

            }

        }

        $this->context['event_types'] = array_chunk(get_terms('event_focus'), 2);

        $this->context['days_of_the_week'] = array_chunk(array( 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat' ), 2);

    }
}

$view = new EventsSubmitView( array('events-submit.twig') );
$view->render();
