<?php
/**
 * The template for displaying 404 pages (Not Found).
 */

$view = new BasingstokeBaseView( array('404.twig') );
$view->render();