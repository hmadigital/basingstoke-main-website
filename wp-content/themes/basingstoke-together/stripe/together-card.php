<?php

/**
 * Functions for the Together Cards
 */
class TogetherCard {

    /**
     * Save data to a Together Card Form submission
     *
     * @param Object $submissionData
     * @return bool
     */
    public static function  process_to_form_submission($submissionData, $intent_id = null) {
        $user_type = $submissionData['user_type'] == 'user_business' ? 'user_business' : 'user_public';
        $stripe_transaction_id = 'N/A';

        $fields = [
            'first_name',
            'last_name',
            'email',
            'business_name',
            'user_type',
            'user_interests',
            'stripe_transaction_id',
            'payment_intent_id',
            'address',
        ];

        if (empty($intent_id)){
            $post = [
                'post_title'    =>  $submissionData['first_name'] . ' ' . $submissionData['last_name'],
                'post_content'  => '',
                'post_status'   => 'draft',
                'post_author'   => 5,
                'post_type'     => 'form_submission',
            ];

            $post_id = wp_insert_post( $post );

            if ($user_type != 'user_business') {
                update_post_meta( $post_id, 'stripe_status', 'PENDING' );
            }

            update_post_meta( $post_id, 'email_opt_in', $submissionData['email_subscribe'] == "true" ? 'Yes' : 'No');
        }
        else {
            $post = Timber::get_post([
                'post_type'     => 'form_submission',
                'post_status'   => 'draft',
                'meta_key'      => 'payment_intent_id',
                'meta_value'    => $intent_id
            ]);

            if (empty($post)){
                return false;
            }

            $post_id = $post->id;
            $stripe_transaction_id = $submissionData['stripe_transaction_id'];

            update_post_meta( $post_id, 'stripe_status', 'CONFIRMED' );
        }

        if ($post_id) {

            update_post_meta( $post_id, 'form_name', 'together_card' );
            foreach ($fields as $field) {
                switch ($field) {
                    case 'stripe_transaction_id':
                        update_post_meta( $post_id, 'stripe_transaction_id', $stripe_transaction_id );
                    case 'user_type':
                        update_post_meta( $post_id, 'user_type', $user_type );
                    case 'address':
                        $address_keys = [
                            'address_line_1',
                            'address_line_2',
                            'address_town',
                            'address_postcode',
                        ];
                        $address_array = [];

                        foreach ( $address_keys as $address_key ) {
                            if ( !empty( $submissionData[$address_key] ) ) {
                                $address_array[] = $submissionData[$address_key];
                            }
                        }

                        $address_string = implode(", ", $address_array);
                        update_post_meta( $post_id, 'address', $address_string);
                        break;
                    case 'user_interests':
                        if (!empty($submissionData['user_interests'])){
                            update_post_meta( $post_id, 'interests', $submissionData['user_interests']);
                        }
                        break;
                    default:
                        update_post_meta( $post_id, $field, isset($submissionData[$field]) ? $submissionData[$field] : '' );
                        break;
                }
            }
            return true;
        }

        return false;
    }

    public static function mark_card_as_unconfirmed($payment_intent_id) {
        $post = Timber::get_post([
            'post_type'     => 'form_submission',
            'post_status'   => 'draft',
            'meta_key'      => 'payment_intent_id',
            'meta_value'    => $payment_intent_id
        ]);

        if (empty($post)){
            return false;
        }

        update_post_meta( $post->id, 'stripe_status', 'UNCONFIRMED' );
        return true;
    }

    public static function send_emails($submissionData) {

        $payment_page = Timber::get_post([
            'post_type' => 'page',
            'meta_key' => '_wp_page_template',
            'meta_value' => 'page-templates/together-card.php',
        ]);

        $email_opt_in = $submissionData['email_subscribe'] == "true";
        $user_type = $submissionData['user_type'] == 'user_business' ? 'user_business' : 'user_public';

        /**
         * Email headers can be re-used for multiple emails.
         */
        $headers[] = 'From: Basingstoke Together <no-reply@basingstoketogether.co.uk>' . "\r\n";

        /**
         * Send email notifcation to Basingstoke Together admin.
         */
        $user_type_text = $user_type == 'user_business' ? 'Business owner/employee' : 'Member of the general public';

        $message = '<p>There has been a Rewards Card submission via the Basingstoke Together website. Details are as follows:</p><br/>';
        $message .= '<p>User Type: ' . $user_type_text . '</p>';
        $message .= '<p>First Name: ' . $submissionData['first_name'] . '</p>';
        $message .= '<p>Last Name: ' . $submissionData['last_name'] . '</p>';
        $message .= '<p>Email Address: ' . $submissionData['email'] . '</p>';
        if ($submissionData['user_type'] == 'user_business'){
            $message .= '<p>Business Name: ' . $submissionData['business_name'] . '</p>';
        }
        if ($submissionData['user_type'] == 'user_public'){
            $message .= '<p>Address Line One: ' . $submissionData['address_line_1'] . '</p>';
            $message .= '<p>Address Line Two: ' . $submissionData['address_line_2'] . '</p>';
            $message .= '<p>Town: ' . $submissionData['address_town'] . '</p>';
            $message .= '<p>Postcode: ' . $submissionData['address_postcode'] . '</p>';
        }
        if (!empty($submissionData['user_interests']) && $email_opt_in == true){
            $message .= '<p>Interests: ' . $submissionData['user_interests'] . '</p>';
        }


        // wp_mail( 'rewards@basingstoketogether.co.uk', 'Basingstoke Together - Basingstoke Rewards Card', $message, $headers );
        wp_mail( get_field('admin_notification_email', $payment_page->id), 'Basingstoke Together - Basingstoke Rewards Card', $message, $headers );


        /**
         * Send email notifcation to client.
         */
        $address_text = $submissionData['user_type'] == 'user_business' ? 'You will receive your card within 2 weeks.' : 'Your card will be processed & sent to you within 5 – 7 working days.';

        $message = '<p>Dear ' . $submissionData['first_name'] . ',</p><br/>';
        $message .= '<p>Thank you for signing up for a Basingstoke Rewards card. ' . $address_text . '</p>';
        $message .= '<p>To keep up to date with the latest promotions find us on social media @inbasingstoke or visit:</p>';
        $message .= '<p><a href="https://www.basingstoketogether.co.uk/in-basingstoke/home/promotions/">https://www.basingstoketogether.co.uk/in-basingstoke/home/promotions/</a></p>';
        $message .= '<br/><p>Many thanks,</p>';
        $message .= '<p>Basingtoke Together</p>';

        wp_mail( $submissionData['email'], 'Basingstoke Together - Together Card', $message, $headers );

        if ($email_opt_in == true) {
            /**
             * Add user to Mailchimp list.
             * This assumes that the user's email hasn't been added before - checked above
             * If a user already exists on the mailchimp list this will fail
             */
            $mcClient = new \HMA\MailChimpV3( '66db84ad1a3d77f18fb8a267777a1b6c-us18' );

            // API CALL TO GET INTERESTS
            $interests_data = $mcClient->get('lists/bb672c6603/interest-categories/635ea60d99/interests');
            $mcInterests = [];

            if (is_array($interests_data) && array_key_exists('interests', $interests_data)) {
                $mcInterests = array_map(function($item) {
                    return [
                        'id' => $item['id'],
                        'label' => $item['name'],
                    ];
                }, $interests_data['interests']);

            }

            // $list_id = 'bb672c6603';
            $list_id = get_field('mailchimp_contact_list_id', $payment_page->id);

            if (!empty($submissionData['user_interests'])){
                $interests = explode(', ',$submissionData['user_interests']);
                $interest_values = array_reduce($mcInterests, function($result, $item) use ($interests) {

                    $id = $item['id'];
                    $label = $item['label'];
                    $selected = in_array($label, $interests);
                    $result[$id] = $selected;

                    return $result;
                }, []);
            } else {
                $interest_values = [];
            }

            // Set the user type interest depending on user type
            if ($user_type == 'user_business') {
                $interest_values['ab9de758fb'] = true;
                $interest_values['2f591cd325'] = false;
            } else {
                $interest_values['ab9de758fb'] = false;
                $interest_values['2f591cd325'] = true;
            }



            if (isset($list_id)){

                $result = $mcClient->post("lists/$list_id/members", [
                    'email_address' => $submissionData['email'],
                    'status'        => 'subscribed',
                    'merge_fields'  => [
                        'FNAME' => $submissionData['first_name'],
                        'LNAME'=> $submissionData['last_name'],
                    ],
                    'interests' => (object) $interest_values,
                ]);
            }
        }

        return true;
    }

}
