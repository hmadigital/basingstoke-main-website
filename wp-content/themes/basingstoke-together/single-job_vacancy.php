<?php

class JobVacancyDetailView extends BasingstokeBaseView {

	public function post_process(){
		
		parent::post_process();

		/*
		 * Always ensure 'Send an event' banner is shown.
		 */
		$this->context['rightcol']['job_listing'] = true;
	}

}

$view = new JobVacancyDetailView( array( 'jobs-detail.twig' ) );
$view->render();