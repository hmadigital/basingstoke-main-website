var gulp = require('gulp'),
	jshint = require('gulp-jshint'),
	prefix = require('gulp-autoprefixer'),
	sass = require('gulp-sass'),
	concat = require('gulp-concat'),
	minifycss = require('gulp-minify-css'),
	uglify = require('gulp-uglify'),
	imagemin = require('gulp-imagemin'),
	plumber = require('gulp-plumber'),
	header = require('gulp-header'),
	gutil = require('gulp-util'),
	sourcemaps = require('gulp-sourcemaps'),

paths = {
	scripts: [
		'js/*.js',
		'js/vendor/*.js'
	],
	images: [
		'img/*'
	],
	css: ['css/sass.css'],
	sass: [
		'css/scss/vendor/*.css',
		'css/./scss/*.scss',
		'css/scss/helpers/*.scss',
		'css/scss/base/*.scss',
		'css/scss/important/*.scss',
		'css/scss/layout/*.scss',
		'css/scss/module/*.scss',
		'css/scss/state/*.scss',
		'css/scss/theme/*.scss'
	],
	fonts: ['fonts/**/*.otf', 'fonts/**/*.eot', 'fonts/**/*.ttf', 'fonts/**/*.svg', 'fonts/**/*.woff'],
},

theme = require('./theme.json'),
	themeBanner = [
		'/*',
		'Theme Name: <%= theme.name %>',
		  'Theme URI: <%= theme.uri %>',
		'Author: <%= theme.author %>',
		'Description: <%= theme.description %>',
		'Version: <%= theme.version %>',
		'License: <%= theme.license %>',
		'Build Timestamp: <%= timestamp %>',
		'*/',
		'',
		'',
	].join('\n');

// Error handling
onError = function (err) {
	gutil.beep();
	console.log(err);
};

// Sass
gulp.task('sass', function(){
	var ts = Math.round(new Date().getTime() / 1000);
	return gulp.src(paths.sass)
		.pipe(sourcemaps.init())
		.pipe(plumber({ errorHandler: onError }))
		.pipe(sass.sync({errLogToConsole: true}))
		.pipe(prefix({
			browsers: ['last 2 version', '> 1%', 'ie 8'],
			cascade: true
		}))
		.pipe(minifycss({
			compatibility: 'ie8' // This ensures minifycss doesn't strip out CSS that is needed for IE8
		}))
		.pipe(header(themeBanner, { theme : theme, timestamp: ts } ))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest('../'))
});

// CSS
gulp.task('css', function () {
	var ts = Math.round(new Date().getTime() / 1000);
	return gulp.src(paths.css)
		.pipe(plumber({
			errorHandler: onError
		}))
		.pipe(concat('style.css'))
		.pipe(minifycss({noAdvanced:true}))
		.pipe(header(themeBanner, { theme : theme, timestamp: ts } ))
		.pipe(prefix({
			browsers: ['last 2 version', '> 1%', 'ie 8'],
			cascade: true
		}))
		.pipe(gulp.dest('../'))
});

// JavaScript
gulp.task('js', function () {
	return gulp.src(paths.scripts)
		.pipe(jshint())
		.pipe(plumber({ errorHandler: onError }))
		.pipe(uglify({mangle: true}))
		.pipe(concat('base.min.js'))
		.pipe(gulp.dest('../js'));
});

// Image Minify
gulp.task('img', function () {
	return gulp.src(paths.images)
		.pipe(imagemin({ progressive: true, }))
		.pipe(gulp.dest('./img'))
});

// Watch Files
gulp.task('watch', function () {
	gulp.watch(paths.sass, ['sass']);
	gulp.watch(paths.scripts, ['js']);
	gulp.watch(paths.images, ['img']);
});

// Default
gulp.task('default', ['sass', 'js', 'watch']);
































// var gulp = require('gulp'),
// jshint = require('gulp-jshint'),
// prefix = require('gulp-autoprefixer'),
// sass = require('gulp-ruby-sass'),
// concat = require('gulp-concat'),
// minifycss = require('gulp-minify-css'),
// uglify = require('gulp-uglify'),
// imagemin = require('gulp-imagemin'),
// flatten = require('gulp-flatten'),
// bowerFiles = require("gulp-bower-files"),
// gutil = require('gulp-util'),
// plumber = require('gulp-plumber'),
// header = require('gulp-header'),
// notify = require('gulp-notify'),
// paths = {
// 	vendor: [
// 		'js/vendor/visibility-1.2.1.min.js',
// 		'js/vendor/visibility.fallback-1.2.1.min.js',
// 		'js/vendor/jquery.min.js',
// 		'js/vendor/jquery.cookie.js',
// 		'js/vendor/jquery.meanmenu.js',
// 		'js/vendor/jquery.placeholder.js',
// 		'js/vendor/imagesloaded.pkgd.js',
// 		'js/vendor/jquery.velocity.js',
// 		'js/vendor/jquery.velocity.ui.js',
// 		'js/vendor/jquery.matchHeight.js',
// 		'js/vendor/jquery.carouFredSel.js',
// 		'js/vendor/jquery-ui.js',
// 		'js/vendor/masonry.pkgd.min.js',
// 	],
// 	scripts: [
// 		'js/base.js',
// 		'js/promotions.js',
// 		'js/business-initiatives.js',
// 		'js/gallery.js',
// 		'js/billboard-map.js',
// 		'js/timeline.js',
// 		'js/newsletter-signup.js',
// 	],
// 	images: 'img/*',
// 	css: ['css/sass.css'],
// 	sass: [
// 			'css/scss/vendor/*.css',
// 			'css/./scss/*.scss',
// 			'css/scss/helpers/*.scss',
// 			'css/scss/base/*.scss',
// 			'css/scss/important/*.scss',
// 			'css/scss/layout/*.scss',
// 			'css/scss/module/*.scss',
// 			'css/scss/state/*.scss',
// 			'css/scss/theme/*.scss'],
// 	fonts: ['fonts/**/*.otf', 'fonts/**/*.eot', 'fonts/**/*.ttf', 'fonts/**/*.svg', 'fonts/**/*.woff']
// },
// theme = require('./theme.json'),
// themeBanner = [
// 	'/*',
// 	'Theme Name: <%= theme.name %>',
// 	  'Theme URI: <%= theme.uri %>',
// 	'Author: <%= theme.author %>',
// 	'Description: <%= theme.description %>',
// 	'Version: <%= theme.version %>',
// 	'License: <%= theme.license %>',
// 	'Build Timestamp: <%= timestamp %>',
// 	'*/',
// 	'',
// 	'',
// ].join('\n'),
// onError = function (err) {
// 	gutil.beep();
// 	console.log(err);
// };

// // Minify Base
// gulp.task('jsbase', function () {
// return gulp.src(paths.scripts)
// 	.pipe(jshint())
// 	  .pipe(jshint.reporter('jshint-stylish'))
// 	.pipe(plumber({
// 		  errorHandler: onError
// 	}))
// 	//.pipe(uglify({mangle: false}))
// 	.pipe(concat('base.min.js'))
// 	.pipe(gulp.dest('../js'));
// });

// // Minify Vendor
// gulp.task('js', function () {
// return gulp.src(paths.vendor)
// 	//.pipe(jshint())
// 	  //.pipe(jshint.reporter('default'))
// 	.pipe(plumber({
// 		  errorHandler: onError
// 	}))
// 	//.pipe(uglify({mangle: false}))
// 	.pipe(concat('vendor.min.js'))
// 	.pipe(gulp.dest('../js'));
// });

// // Move Bower
// gulp.task('bowerFiles', function(){
// bowerFiles().pipe(gulp.dest("../build"));
// });

// // Image minify
// gulp.task('img', function () {
// return gulp.src(paths.images)
// 	.pipe(imagemin({optimizationLevel: 5}))
// 	.pipe(gulp.dest('img'))
// 	.pipe(notify({ message: 'Image tasks complete' }));
// });

// // Fonts
// gulp.task('fonts', function () {
// return gulp.src(paths.fonts)
// 	.pipe(flatten())
// 	.pipe(gulp.dest('fonts'));
// });

// // Sass
// gulp.task('sass', function () {
// return gulp.src(paths.sass)
// 	.pipe(plumber({
// 		  errorHandler: onError
// 	}))
// 	.pipe(sass({style: 'compact', noCache: true}))
// 	.pipe(concat('sass.css'))
// 	.pipe(gulp.dest('../build/css'));
// });

// // CSS
// gulp.task('css', function () {

// var ts = Math.round(new Date().getTime() / 1000);

// return gulp.src(paths.css)
// 	.pipe(plumber({
// 		  errorHandler: onError
// 	}))
// 	.pipe(concat('style.css'))
// 	.pipe(minifycss({noAdvanced:true}))
// 	.pipe(header(themeBanner, { theme : theme, timestamp: ts } ))
// 	.pipe(prefix('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
// 	.pipe(gulp.dest('../'))
// 	.pipe(notify({ message: 'CSS Compiled' }));
// });

// // Rerun Tasks
// gulp.task('watch', function () {
// gulp.watch(paths.sass, ['sass', 'css']);
// gulp.watch(paths.vendor, ['js']);
// gulp.watch(paths.scripts, ['jsbase']);
// gulp.watch(paths.images, ['img']);
// gulp.watch(paths.fonts, ['fonts']);
// gulp.watch(paths.css, ['css']);
// });

// // Default Task
// gulp.task('default', ['sass', 'js', 'jsbase', 'fonts', 'css', 'watch']);