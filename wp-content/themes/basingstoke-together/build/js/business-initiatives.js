window.Basingstoke = window.Basingstoke || {};

Basingstoke.businessInitiatives = (function($) {

    "use strict";

        //elements
    var _win,
        _body,
        _smallBoxes,
        _detailBlocks,

        //functions
        init,
        urlUnserialize,
        closeOpenDetail,
        calculateHeights,

        // scalar
        windowWidth = 980,
        openId = '';

    urlUnserialize = function(p){
        var ret = {},
        seg = p.replace(/^\?/,'').split('&'),
        len = seg.length, i = 0, s;

        for (;i<len;i++) {
            if (!seg[i]) { continue; }
            s = seg[i].split('=');
            ret[s[0]] = s[1];
        }
        return ret;
    };

    closeOpenDetail = function(){

        if (openId && openId.length > 0) {
            $('#' + openId).velocity('transition.slideUpBigOut', {
                duration: 250,
                complete: function(){
                    openId = '';
                }
            });
        }

    };

    calculateHeights = function() {

        windowWidth = _win.width();

        _detailBlocks.css({
            'position': 'static',
            'display': 'block',
            'visibility': 'hidden'
        });

        _detailBlocks.each(function(){
            var _detail = $(this),
                smallId = '#' + _detail.attr('id').replace('detail', 'small'),
                _small = $(smallId),
                offset = _small.offset(),
                smallHeight = _small.outerHeight(),
                height = _detail.outerHeight(),
                currentlyVisible = (openId == _detail.attr('id'));

            _detail
                .attr('data-height', height)
                .removeAttr('style')
                .css({
                    'position': 'absolute',
                    'display': (currentlyVisible) ? 'block' : 'none',
                    'top': (offset.top + smallHeight),
                    'left': (windowWidth > 979) ? ( (windowWidth - 920) / 2 ) : 0,
                    'margin': (windowWidth > 979) ? '0 auto' : '0 4%'
                });
        });



    };

    init = function() {

        _win = $(window);
        _body = $('body');

        if (!_body.hasClass('page-template-page-templatesbusiness-initiatives-php')) {
            return;
        }

        $('.initiative-details-wrapper').appendTo(_body);

        _smallBoxes = $('.col-box');
        _detailBlocks = $('.col-detail');

        _detailBlocks
            .removeClass('has-js-hide')
            .find('a.col-detail--close').on('click tap', function(e){
                e.preventDefault();
                closeOpenDetail();
            });

        // 'normal' notifcation handling
        _detailBlocks.find('a.action-type-notification').on('click tap', function(e){
            e.preventDefault();

            var _link = $(this),
                _smallBoxChk = $('#' + _link.data('small-col') ).find('a.small-chk'),
                _spinner = $('<span class="loading"><i class="fa fa-cog fa-spin"></i> Loading, please wait...</span>').css('display', 'none').appendTo(_link.parent()),
                href = _link.attr('href'),
                data = urlUnserialize(href.substr(href.lastIndexOf('?')));

            _link.velocity('transition.slideRightOut', {
                duration: 250,
                display: 'none',
            });

            _spinner.velocity('transition.slideLeftIn', {
                duration: 150,
                delay: 150
            });

            var posting = $.post(Basingstoke.ajaxURL, data);

            posting.done(function(data){

                setTimeout(function(){

                    if(data && data.success && data.success === true){

                        var checked = (data.method == 'add');

                        if (checked === true) {
                            _link.addClass('col-detail__aside__link_active');
                            _smallBoxChk.addClass('col-box--check');
                            _smallBoxChk.removeClass('col-box--uncheck');
                        } else {
                            _link.removeClass('col-detail__aside__link_active');
                            _smallBoxChk.addClass('col-box--uncheck');
                            _smallBoxChk.removeClass('col-box--check');
                        }

                        if (data.button_text && data.button_text.length > 0) {
                            _link.text(data.button_text);
                        }

                    }

                     _link.velocity('transition.slideRightIn', {
                        display: 'block',
                        duration: 150,
                        delay: 100
                    });

                    _spinner.velocity('transition.slideLeftOut', {
                        duration: 250
                    });


                }, 500);

             });
        });

        // form (twitter or business) ajax handling
        _detailBlocks.find('form').on('submit', function(e){
             e.preventDefault();

             var _form = $(this),
                formData = _form.serialize(),
                _link = _form.find('button.col-detail__aside__link'),
                _smallBoxChk = $('#' + _link.data('small-col') ).find('a.small-chk'),
                _spinner = $('<span class="loading"><i class="fa fa-cog fa-spin"></i> Loading, please wait...</span>').css('display', 'none').appendTo(_link.parent());

            _link.velocity('transition.slideRightOut', {
                duration: 250,
                display: 'none',
            });

            _spinner.velocity('transition.slideLeftIn', {
                duration: 150,
                delay: 150
            });

            var posting = $.post(Basingstoke.ajaxURL, formData);

            posting.done(function(data){

                setTimeout(function(){

                    if(data && data.success && data.success === true){

                        var checked = (data.method == 'add');

                        if (checked === true) {
                            _link.addClass('col-detail__aside__link_active');
                            _smallBoxChk.addClass('col-box--check');
                            _smallBoxChk.removeClass('col-box--uncheck');
                        } else {
                            _link.removeClass('col-detail__aside__link_active');
                            _smallBoxChk.addClass('col-box--uncheck');
                            _smallBoxChk.removeClass('col-box--check');
                        }

                        if (data.button_text && data.button_text.length > 0) {
                            _link.text(data.button_text);
                        }

                    }

                     _link.velocity('transition.slideRightIn', {
                        display: 'block',
                        duration: 150,
                        delay: 100
                    });

                    _spinner.velocity('transition.slideLeftOut', {
                        duration: 250
                    });


                }, 500);

             });


        });

        _win.on('resize', calculateHeights);

        Basingstoke.base.preloadImages($('.col img'), calculateHeights);

        _smallBoxes.find('a').on('click tap', function(e){
            e.preventDefault();

            var _link = $(this),
                newId = _link.attr('href').replace('#', ''),
                _target = $(_link.attr('href'));

            if (newId == openId) {
                return;
            }

            closeOpenDetail();

            _target.velocity('transition.slideDownBigIn', {
                duration: 350,
                complete: function(){
                    openId = _target.attr('id');
                }
            });
        });

    };

    return {
        init: init
    };

}(jQuery));

jQuery(function(){
    if (!$('#initiatives_wrapper').length) {
        $('.js-initiatives').find('.small-chk').hide();
        return false;
    }
    Basingstoke.businessInitiatives.init();
});