window.Basingstoke = window.Basingstoke || {};

Basingstoke.togetherCard = (function($) {

    "use strict";

    var init;

    init = function () {

        var isPaymentForm = document.querySelector('#together-payment-form');

        if (isPaymentForm) {

            const formWithStripe = document.querySelector('#together-payment-form');
            const cardElement = document.querySelector('#card-element');

            if (!cardElement) return;

            const publicKey = cardElement.getAttribute('data-public-key');
            const intentSecret = cardElement.getAttribute('data-intent-secret');
            const cardErrors = document.getElementById('card-errors');
            const billingName = cardElement.getAttribute('data-billing-name');
            const billingEmail = cardElement.getAttribute('data-billing-email');

            if (formWithStripe && publicKey && intentSecret) {

                const formButton = formWithStripe.querySelector('#payment-button');
                const initialButtonText = formButton.textContent;

                const stripe = Stripe(publicKey);
                const elements = stripe.elements();

                const elementsStyle = {
                    base: {
                        color: "#333333",
                        fontFamily: '"omnes-pro", "Helvetica Neue", Helvetica, Arial, sans-serif',
                        fontSmoothing: 'antialiased',
                        fontSize: '16px',
                        '::placeholder': {
                            color: '#8a8989'
                        },
                    },
                    invalid: {
                        color: '#FF7375',
                        iconColor: '#FF7375'
                    }
                };

                const stripeCardElement = elements.create('card', { style: elementsStyle });

                stripeCardElement.mount(cardElement);

                // Handle real-time validation errors from the card Element.
                stripeCardElement.addEventListener('change', function (event) {

                    if (event.error) {
                        cardErrors.textContent = event.error.message;
                    } else {
                        cardErrors.textContent = '';
                    }
                });

                formWithStripe.addEventListener('submit', function(event) {

                    event.preventDefault();

                    formButton.setAttribute('disabled', "");
                    formButton.innerText = 'Processing, please wait...';
                    cardErrors.textContent = '';

                    stripe
                        .handleCardPayment(
                            intentSecret,
                            stripeCardElement,
                            {
                                payment_method_data: {
                                    billing_details: {
                                        name: billingName,
                                        email: billingEmail,
                                    },
                                },
                            }
                        )
                        .then( function(result) {

                            if (result.error) {
                                // Display error.message in your UI.
                                cardErrors.textContent = result.error.message;
                                formButton.removeAttribute('disabled');
                                formButton.innerText = initialButtonText;
                            } else {
                                // The payment has succeeded. Display a success message.
                                cardErrors.textContent = '';
                                // document.querySelector('#payment-success-message').removeAttribute('hidden');
                                // document.querySelector('#together-payment-form').setAttribute('hidden', true);
                                formWithStripe.submit();
                            }
                        })
                });
            }

        } else {

            console.log('running"')

            var userTypeRadioButtons = $('.promo__radio-item>input');
            var emailSubscribeRadioButtons = $('.email-radio-subscribe-input');
            var publicOnlyFields = $('#together-details-form [data-for="public"]');
            var businessOnlyFields = $('#together-details-form [data-for="business"]');
            var publicRequiredFields = publicOnlyFields.find('input[required]');
            var businessRequiredFields = businessOnlyFields.find('input[required], select[required]');

            // When page is loaded, check if there is a hash fragment matching 'public'. If there is, check the public radio, and remove the hash.
            var hash = window.location.hash;

            var publicRadioInput = $('#radio_user_public');

            // If the hash is 'public' check public radio input if it isn't checked
            if (hash == "#public") {
                if (!publicRadioInput.is(':checked')){
                    publicRadioInput.prop("checked", true);
                }
                // Remove hash from URL to allow changing radio
                history.pushState("", document.title, window.location.pathname
                + window.location.search);
            }

            function checkUserTypeRadioButtons(){
                var checkedValue = $('input[name=user_type]:checked', '#together-details-form').val()

                if (checkedValue === "user_public") {
                    // REMOVE REQUIRED attribute from input fields and hide fields related to business only
                    publicRequiredFields.prop('required', true);
                    publicOnlyFields.slideDown();
                    businessRequiredFields.prop('required', null);
                    businessOnlyFields.slideUp();

                    // Ensure button text is correct
                    $('#promo-submit-button').text('GO TO PAYMENT');

                } else if ( checkedValue == "user_business") {
                    // REMOVE REQUIRED attribute from input fields and hide fields related to public only
                    businessRequiredFields.prop('required', true);
                    businessOnlyFields.slideDown();
                    publicRequiredFields.prop('required', null);
                    publicOnlyFields.slideUp();

                    // Ensure button text is correct
                    $('#promo-submit-button').text('JOIN NOW');
                }
            }

            function checkEmailSubscribeRadioButtons(){
                var checkedSubscribeValue = $('input[name=email_subscribe]:checked', '#together-details-form').val();

                if (checkedSubscribeValue === "true") {
                    // Show interests checkboxes
                    $('#promo_interests').slideDown();

                } else if ( checkedSubscribeValue == "false") {
                    // Hide interests checkboxes
                    $('#promo_interests').slideUp();
                }
            }

            userTypeRadioButtons.on('change', function(ev){
                checkUserTypeRadioButtons();
            })
            emailSubscribeRadioButtons.on('change', function(ev){
                checkEmailSubscribeRadioButtons();
            })

            checkUserTypeRadioButtons();
            checkEmailSubscribeRadioButtons();
        }
    };


    return {
        init: init
    };

}(jQuery));

$(document).ready(function () {

    if (!document.getElementById('together-card-sign-up')) {
        return false;
    }

    Basingstoke.togetherCard.init();

});
