window.Basingstoke = window.Basingstoke || {};

Basingstoke.Promotions = (function($) {

    "use strict";

        //elements
    var _win,
        _body,
        _smallBoxes,
        _detailBlocks,

        //functions
        init,
        urlUnserialize,
        closeOpenDetail,
        calculateHeights,

        // scalar
        windowWidth = 980,
        openId = '';

    urlUnserialize = function(p){
        var ret = {},
        seg = p.replace(/^\?/,'').split('&'),
        len = seg.length, i = 0, s;

        for (;i<len;i++) {
            if (!seg[i]) { continue; }
            s = seg[i].split('=');
            ret[s[0]] = s[1];
        }
        return ret;
    };

    closeOpenDetail = function(){

        if (openId && openId.length > 0) {
            $('#' + openId).velocity('transition.slideUpBigOut', {
                duration: 250,
                complete: function(){
                    openId = '';
                }
            });
        }

    };

    calculateHeights = function() {

        windowWidth = _win.width();

        _detailBlocks.css({
            'position': 'static',
            'display': 'block',
            'visibility': 'hidden'
        });

        _detailBlocks.each(function(){
            var _detail = $(this),
                smallId = '#' + _detail.attr('id').replace('detail', 'small'),
                _small = $(smallId),
                offset = _small.offset(),
                smallHeight = _small.outerHeight(),
                height = _detail.outerHeight(),
                currentlyVisible = (openId == _detail.attr('id'));

            _detail
                .attr('data-height', height)
                .removeAttr('style')
                .css({
                    'position': 'absolute',
                    'display': (currentlyVisible) ? 'block' : 'none',
                    'top': (offset.top + smallHeight),
                    'left': (windowWidth > 979) ? ( (windowWidth - 920) / 2 ) : 0,
                    'margin': (windowWidth > 979) ? '0 auto' : '0 4%'
                });
        });

    };

    init = function() {

        _win = $(window);
        _body = $('body');

        if (!_body.hasClass('page-template-page-templatespromotions-php')) {
            return;
        }

        // Tabbed Layout

        var clickedTab = $('.category-tabs > .category-tabs--active'),
            tabWrapper = $('.category-tabs__content'),
            activeTab = tabWrapper != null ? tabWrapper.find('.category-tabs--active') : null,
            activeTabHeight = activeTab.outerHeight();

        // Show tab on page load
        if (activeTab != null) {
            activeTab.css({
                'display': 'flex'
            });
        }
        setTimeout(function(){
            activeTabHeight = activeTab.outerHeight();

            // Set height of wrapper on page load
            if (tabWrapper != null) {
                tabWrapper.height(activeTabHeight + 80);
            }
        }, 80)        

        $('.category-tabs > li').on('click', function() {

            closeOpenDetail();

            $('.category-tabs > li').removeClass('category-tabs--active');

            $(this).addClass('category-tabs--active');

            clickedTab = $('.category-tabs .category-tabs--active');

            // Fade out active tab
            activeTab.fadeOut(250, function() {

                // Remove active class all tabs
                $('.category-tabs__content > li').removeClass('category-tabs--active');

                // Get index of clicked tab
                var clickedTabIndex = clickedTab.index();

                // Add class active to corresponding tab
                $('.category-tabs__content > li').eq(clickedTabIndex).addClass('category-tabs--active');

                // update new active tab
                activeTab = $('.category-tabs__content > .category-tabs--active');

                // Update variable
                activeTabHeight = activeTab.outerHeight();

                // Animate height of wrapper to new tab height
                tabWrapper.stop().delay(50).animate({
                    height: activeTabHeight
                }, 250, function() {

                    // Fade in active tab
                    activeTab.delay(50).css({
                        'display': 'flex'
                    });

                });

            });

        });

        $('.initiative-details-wrapper').appendTo(_body);

        _smallBoxes = $('.col-box');
        _detailBlocks = $('.col-detail');

        _detailBlocks
            .removeClass('has-js-hide')
            .find('a.col-detail--close').on('click tap', function(e){
                e.preventDefault();
                closeOpenDetail();
            });

        _win.on('resize', calculateHeights);

        Basingstoke.base.preloadImages($('.col img'), calculateHeights);

        _smallBoxes.find('a').on('click tap', function(e){
            e.preventDefault();

            calculateHeights();

            var _link = $(this),
                newId = _link.attr('href').replace('#', ''),
                _target = $(_link.attr('href'));

            if (newId == openId) {
                return;
            }

            closeOpenDetail();

            _target.velocity('transition.slideDownBigIn', {
                duration: 350,
                complete: function(){
                    openId = _target.attr('id');
                }
            });

        });

    };

    return {
        init: init
    };

}(jQuery));

jQuery(function(){
    if (!$('#initiatives_wrapper').length) {
        $('.js-initiatives').find('.small-chk').hide();
        return false;
    }
    Basingstoke.Promotions.init();
});

// $(window).on('load', function() {
//     tabWrapper.height(activeTabHeight);
// });
