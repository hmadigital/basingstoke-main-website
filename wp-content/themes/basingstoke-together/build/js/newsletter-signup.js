window.Basingstoke = window.Basingstoke || {};

Basingstoke.newsletterSignup = (function ($) {

    'use strict';

    var init,
        submitFormWithAJAX,
        handleFormSubmit,
        setupBinds,
        _errorList;

    /**
     * Submit the form with AJAX, and handle the success/fail responses
     * @param  {Object} data The data to send
     */
    submitFormWithAJAX = function (data) {

        $.ajax({
            url:      Basingstoke.ajaxURL,
            type:     'post',
            dataType: 'json',
            data:     data,
        }).done(function (data) {

            if ( data.errors && data.errors.length > 0 ) {

                console.log(_errorList, data.errors);

                _errorList.empty();

                $.each( data.errors, function(idx, error){
                    $('<li></li>').text(error).appendTo(_errorList);
                });

                _errorList.parent().show();

                return false;

            }

            $('#newsletter_signup_form').html('<h2>' + data.message + '</h2>');

        }).fail(function (jqXHR, textStatus, errorThrown) {

            _errorList.empty();

            $('<li></li>').text(textStatus).appendTo(_errorList);

            _errorList.parent().show();

        });

    };

    /**
     * Event handler
     */
    handleFormSubmit = function (event) {

        var data;

        event.preventDefault();

        _errorList.parent().hide();

        data = $(this).serialize();

        submitFormWithAJAX(data);

    };

    /**
     * Setup the click and submit binds for the page
     */
    setupBinds = function () {

        $('#newsletter_signup_form form').on('submit', handleFormSubmit);

    };

    /**
     * Initialisation function
     */
    init = function () {

        _errorList = $('#newsletter_signup_form .error--message ul');

        setupBinds();

    };

    return {
        init: init
    };

}(jQuery));

$(document).on('ready', Basingstoke.newsletterSignup.init);
