window.Basingstoke = window.Basingstoke || {};

Basingstoke.gallery = (function($) {

    "use strict";

    var init,
        setupGallery,
        setupGalleryButtons,
        setupThumbsSlider,

        windowWidth,

        updateImageButtonsVisibility,

        currentPosition,
        nextImage,
        prevImage,

        sliderPosition,
        sliderLeft,
        sliderRight,

        handleWindowResize,
        handleNextClick,
        HandlePrevClick,
        handleSliderButtonClick,
        handleThumbClick,

        $gallery;

    $.fn.fadeInOrShow = function (type) {
        if (windowWidth > 1024) {
            $(this).fadeIn();
        } else {
            $(this).show();
        }
        return this;
    };

    $.fn.fadeOutOrHide = function (type) {
        if (windowWidth > 1024) {
            $(this).fadeOut();
        } else {
            $(this).hide();
        }
        return this;
    };

    updateImageButtonsVisibility = function () {

        var currentSelected;

        currentSelected = $gallery.container.find('.js-selected');

        if (!$gallery.images.index(currentSelected)) {
            $gallery.images.buttons.left.attr('disabled', 'disabled');
        } else {
            $gallery.images.buttons.left.removeAttr('disabled');
        }

        if ($gallery.images.index(currentSelected) === $gallery.images.length - 1) {
            $gallery.images.buttons.right.attr('disabled', 'disabled');
        } else {
            $gallery.images.buttons.right.removeAttr('disabled');
        }
    };

    nextImage = function (event) {
        $gallery
            .container
            .find('.js-selected')
            .fadeOutOrHide()
            .removeClass('js-selected')
            .next('.Basingstoke-gallery-images__image')
            .addClass('js-selected')
            .fadeInOrShow();

        updateImageButtonsVisibility();
    };

    prevImage = function (event) {
        $gallery
            .container
            .find('.js-selected')
            .fadeOutOrHide()
            .removeClass('js-selected')
            .prev('.Basingstoke-gallery-images__image')
            .addClass('js-selected')
            .fadeInOrShow();

        updateImageButtonsVisibility();
    };

    sliderLeft = function (event) {

        sliderPosition--;
        $($gallery.thumbs[sliderPosition]).show();

        $gallery.thumbs.buttons.right.removeAttr('disabled');

        if (!sliderPosition) {
            $gallery.thumbs.buttons.left.attr('disabled', 'disabled');
        }

    };

    sliderRight = function (event) {

        $($gallery.thumbs[sliderPosition]).hide();
        sliderPosition++;

        $gallery.thumbs.buttons.left.removeAttr('disabled');

        if (sliderPosition > ($gallery.thumbs.length - 10)) {
            $gallery.thumbs.buttons.right.attr('disabled', 'disabled');
        }

    };

    handleThumbClick = function (event) {

        var target;

        event.preventDefault();

        target = $(this).attr('href');

        $(target).fadeInOrShow().addClass('js-selected').siblings('.Basingstoke-gallery-images__image').fadeOutOrHide().removeClass('js-selected');

        updateImageButtonsVisibility();

    };

    handleWindowResize = function () {
        windowWidth = $(window).width();
    };

    setupThumbsSlider = function() {

        sliderPosition = 0;

        $gallery.thumbs.buttons = {
            left:  $gallery.container.find('.Basingstoke-gallery-thumbs__button--left'),
            right: $gallery.container.find('.Basingstoke-gallery-thumbs__button--right')
        };

        $gallery.thumbs.buttons.right.removeAttr('disabled');

        $gallery.thumbs.buttons.left.on('click tap', sliderLeft);
        $gallery.thumbs.buttons.right.on('click tap', sliderRight);

    };

    setupGalleryButtons = function () {
        $gallery.images.buttons = {
            left:  $gallery.container.find('.Basingstoke-gallery-images__button--left'),
            right: $gallery.container.find('.Basingstoke-gallery-images__button--right')
        };

        $gallery.images.buttons.right.removeAttr('disabled');

        $gallery.images.buttons.left.on('click tap', prevImage);
        $gallery.images.buttons.right.on('click tap', nextImage);

    };

    setupGallery = function () {

        var container;

        container = $('#Basingstoke_gallery');

        $gallery = {
            container: container,
            images:    container.find('.Basingstoke-gallery-images__image'),
            thumbs:    container.find('.js-Basingstoke_gallery_thumbs').find('a')
        };

        $gallery.images.hide().first().addClass('js-selected').show();

    };

    init = function () {

        $(window).resize(handleWindowResize);

        handleWindowResize();

        if (!document.getElementById('Basingstoke_gallery')) {
            return false;
        }

        setupGallery();

        if ($gallery.images.length > 1) {
            setupGalleryButtons();
        }

        $gallery.thumbs.on('click tap', handleThumbClick);

        if ($gallery.thumbs.length > 8) {
            setupThumbsSlider();
        }

    };

    return {
        init: init
    };

}(jQuery));

jQuery(function(){
    Basingstoke.gallery.init();
});