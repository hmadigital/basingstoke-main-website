window.Basingstoke = window.Basingstoke || {};

Basingstoke.base = (function($) {

	"use strict";

		//elements
	var _html,
		_win,
		_body,
		_slider,

		_pagination,
		_loadNext,
		_nextPage,
		_fallback,
		_tempWrapper,
		_placeholderNext,

		//functions
		init,
		getPageData,
		setupPagination,
		checkCookies,
		preloadImages,
		onResize,

		//scalar values (ints, strings etc)
		currentWidth = 980,
		nextSelector = '.tool-pagination a.next',
		loadingPage = false,
		currentPage;

	checkCookies = function(){
		var accepted = ($.cookie('policy-accepted') == 'yes');

		if (!accepted) {
			var _prompt = $('<div/>').addClass('cookie-policy').appendTo($('body'));
			_prompt.html('<p>Our cookies help us provide you with the best possible online experience.<br/>By continuing to use our site we will assume you are happy to receive these cookies. <a href="/cookie-policy/" title="Find out more about our cookies.">View our cookie policy.</a> <a href="#" class="close">Close this message</a></p>');

			_prompt.find('a.close').on('click tap', function(e){
				e.preventDefault();
				_prompt.remove();
			});

			$.cookie('policy-accepted', 'yes', { expires: 14, path: '/' });
		}
	};

	preloadImages = function(sources, callback) {
		if(sources.length) {
			var preloaderDiv = $('<div style="display: none;"></div>').prependTo(document.body);

			$.each(sources, function(i,source) {
				$("<img/>").attr("src", source.src).appendTo(preloaderDiv);

				if(i == (sources.length-1)) {
					$(preloaderDiv).imagesLoaded(function() {
						$(this).remove();
						if(callback) { callback(); }
					});
				}
			});
		} else {
			if(callback) { callback(); }
		}
	};

	getPageData = function(_link, _destination){

		if (loadingPage) { return; }

		_loadNext.velocity({
			'opacity': 0
		}, 250);

		loadingPage = true;

		var url = _link.attr('href'),
			xhr = $.ajax(url, { dataType: 'html' }).done(function(data){
				setTimeout(function(){

					_tempWrapper.html(data);

					var _articles = _tempWrapper.find('div.article-wrapper'),
						_newPagination = _tempWrapper.find('div.tool-pagination');

					_nextPage = _newPagination.find('a.next');
					_articles.css('display', 'none');
					_articles.appendTo(_destination).velocity('transition.slideUpIn', 250);

					loadingPage = false;

					if (_nextPage.length > 0) {
						_loadNext.velocity({
							'opacity': 1
						}, 250);
					} else {
						_pagination.css('display', 'none');
					}

					_tempWrapper.empty();


				}, 250);
			});

	};

	setupPagination = function(){

		_pagination = $('.tool-pagination');
		_fallback = _pagination.find('nav.pagination');
		_loadNext = _pagination.find('a.btn-load-next');

		_placeholderNext = $('.data-placeholder-next');
		_nextPage = $(nextSelector);

		if (_pagination.length > 0) {

			_tempWrapper = $('<div class="tmp-wrap"></div>').css('display', 'none').appendTo($('body')).empty();

			//check for a next or previous page link
			if (_nextPage.length > 0) {
				_fallback.css('display', 'none');


				if (_nextPage.length > 0) {
					_loadNext.on('click tap', function(e){
						e.preventDefault();

						getPageData(_nextPage, _placeholderNext);
					});
				}


			} else {
				//no links so no more pages, just hide stuff!
				_pagination.css('display', 'none');
			}

		}

	};

	onResize = function () {
		currentWidth = _win.width();
	};

	init = function() {
		_html = $('html');
		_win = $(window);
		_body = $('body');
		_slider = $('div.slider');

		_html.removeClass('no-js').addClass('has-js');

		$(window).on('resize', onResize);

		onResize();

		_body.on('click tap', 'a.external', function(e){
			e.preventDefault();
			window.open(this, '_blank');
		});

		checkCookies();

		setupPagination();

		// Mobile Menu
		$('nav.site-nav').meanmenu({
			meanMenuClose: '<div class="lines"><span /><span /><span /></div> <em>Menu</em>', // Open
			meanMenuOpen: '<div class="lines"><span /><span /><span /></div> <em>Menu</em>', // Closed
			meanRevealPosition: 'left',
			meanExpand: ">", // single character you want to represent the expand for ULs
			meanContract: "v", // single character you want to represent the contract for ULs
			meanScreenWidth: 980
		});


		$('.collapsible').each(function(idx, collapsible){

			var _collapsible = $(this),
				animating = false,
				_title = _collapsible.find('h3.collapsible__title'),
				_content = _collapsible.find('.collapsible__content');

			_content.css('display', 'none');

			_title.on('click tap', function(e){

				if (animating) { return; }

				animating = true;

				if (_title.hasClass('collapsible__title--open')) {

					_content.removeClass('collapsible__content--open');
					_title.removeClass('collapsible__title--open');
					_content.slideUp(250, function(){
					   animating = false;
					});
				} else {
					_title.addClass('collapsible__title--open');
					 _content.addClass('collapsible__content--open');
					_content.slideDown(250, function(){
						animating = false;
					});
				}
			});
		});

		// Homepage sliders setup
		if (_slider.length > 0) {
			var currentSlide,
				lastSlide,

				_slides,
				_prevButton,
				_nextButton,

				handleSliderNavClick,
				updateSlider;

			_slides = _slider.find('.slides > li');
			currentSlide = 0;
			lastSlide = _slides.length - 1;

			_prevButton = _slider.find('.direction-nav__prev');
			_nextButton = _slider.find('.direction-nav__next');

			if (lastSlide === 0) {
				_prevButton.hide();
				_nextButton.hide();
				return;
			}

			updateSlider = function () {

				_prevButton.show();
				_nextButton.show();

				if (currentSlide === 0) {
					_prevButton.hide();
				} else if (currentSlide === lastSlide) {
					_nextButton.hide();
				}

				if (currentWidth > 640) {
					_slides.eq(currentSlide).fadeIn(750).siblings().fadeOut(750);
				} else {
					_slides.eq(currentSlide).show().siblings().hide();
				}

			};

			handleSliderNavClick = function (event) {
				var direction;

				event.preventDefault();

				direction = $(this).attr('data-direction');

				switch(direction) {
					case 'prev':
						currentSlide--;
						break;
					case 'next':
						currentSlide++;
						break;
				}
				updateSlider();

			};

			_slider.find('.direction-nav').on('click tap', '.direction-nav__link', handleSliderNavClick);

			updateSlider();
		}

		$('.img-banner').matchHeight();

		// Equal Height
		$('body .row').each(function() {
			$(this).children('.banner').matchHeight();
			$(this).children('.even-grid').matchHeight();
			$(this).children('.grid-4').matchHeight();
			$(this).children('.col-box').matchHeight();
		});

		// Floated Labels
		var onClass = "label_on";
		var showClass = "label_show";
		$("input, textarea").bind("checkval",function(){
		var label = $(this).prev("label");
		if($(this).val() !== ""){
			label.addClass(showClass);
		} else {
		label.removeClass(showClass);
		}
		}).on("keyup",function(){
			$(this).trigger("checkval");
		}).on("focus",function(){
			$(this).prev("label").addClass(onClass);
		}).on("blur",function(){
			$(this).prev("label").removeClass(onClass);
		}).trigger("checkval");

		// Placeholder Text
		$('.theme-form input, .theme-form textarea').placeholder();

		$('#gallery_business_detail').carouFredSel({
			items       : 1,
			responsive	: true,
			scroll		: {
				fx		: "crossfade"
			},
			auto: false,
	        pagination  : {
		        container       : "#gallery_business_detail_thumbs",
		        anchorBuilder   : function( nr ) {
		            var src = $("img", this).attr( "src" );
		            return '<li class="gallery__item"><img src="' + src + '" /></li>';
		        }
		    }

		});

		$('#together-card-carousel').carouFredSel({
			responsive	: true,

		});

        $('.file-replacement').click(function(){
            //This will make the element with class file_input_replacement launch the select file dialog.
            var assocInput = $(this).siblings("input[type=file]");
            assocInput.click();
        });
        $('.file-replaced').change(function(){
            //This portion can be used to trigger actions once the file was selected or changed. In this case, if the element triggering the select file dialog is an input, it fills it with the filename
            var thisInput = $(this);
            var assocInput = thisInput.siblings(".file-replacement");
            if (assocInput.length > 0) {
              var filename = (thisInput.val()).replace(/^.*[\\\/]/, '');
              assocInput.val(filename);
            }
        });

        $('#business_tabs').on('click', '.js-tab', function (event) {

        	var id;

        	event.preventDefault();

        	$(this).addClass('site-tabs--active').siblings().removeClass('site-tabs--active');

        	id = $(this).attr('href');

        	$(id).show().siblings().hide();

        	$('.grid-4').matchHeight();

        });

    	$('#offers_and_rewards').hide();

	};

	return {
		init: init,
		preloadImages: preloadImages
	};

}(jQuery));

jQuery(function(){
	Basingstoke.base.init();
});