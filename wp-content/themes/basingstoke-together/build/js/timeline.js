window.Basingstoke = window.Basingstoke || {};

Basingstoke.timeline = (function($) {

    "use strict";

        // functions
    var init,
        setupMasonry,

        // elements
        container;

    setupMasonry = function () {

        container.masonry({
            columnWidth: '.timeline__item',
            itemSelector: '.item',
            transitionDuration: 0
        }).masonry();

    };

    init = function () {

        var images;

        container = $('#timeline');

        images = container.find('img');

        Basingstoke.base.preloadImages(images, setupMasonry);

    };

    return {
        init: init
    };

}(jQuery));

$(document).ready(function () {

    if (!document.getElementById('timeline')) {
        return false;
    }

    Basingstoke.timeline.init();

});
