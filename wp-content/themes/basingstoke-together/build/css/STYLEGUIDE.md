# Styling guidelines

The cascade follows the [SMACSS](http://smacss.com) approach and makes heavy
use of OOCSS. You must follow a common commenting, syntax and naming throughout.
DO NOT alter class names or introduce new elements unless you fully understand
what the implications will be and what other components you are altering.

* We make heavy use of BEM (Block, Element, Modifier) notation.
* DO NOT introduce IDs to CSS. Style using only classes.
* DO NOT change or alter class names.
* Avoid nesting more than 2 levels deep.
* Avoid using !important to hide specificity issues.
* Avoid styling JS Hooks. Prepend with .js-
* Avoid magic numbers.
* Selectors should be kept short.
* Avoid over-qualifing selectors.
* Avoid adding widths to components.
* Avoid adding heights to elements.
* Avoid styling according to location.
* Font Sizes should be set in rems with a pixel fallback.
* Line Heights should be unitless.