<?php

class BusinessDetailView extends BasingstokeBaseView {

	public function post_process(){

		/**
		 * Ensure the base class post_process method gets called.
		 */
		parent::post_process();

		/**
		 * Work out whether the business has regular opening hours, or get the opening times
		 */
		$regular_opening_hours = get_field( 'has_regular_opening_hours', $this->context['page']->ID);

		if ( !$regular_opening_hours ) {

			$this->context['regular_opening_hours'] = true;

		} else {

			$this->context['opening_hours'] = get_field('opening_hours', $this->context['page']->ID);

		}

		/**
		 * Get any 'related' events.
		 */
		$imageIDs = get_field('images', $this->context['page']->ID);

		if ( is_array($imageIDs) && !empty($imageIDs)) {
			$imageIDs = array_map( function($a){ return intval($a['image']); }, $imageIDs);

			$images = array();

			foreach ($imageIDs as $imageID) {
				if ($imageID > 0) {
					$images[] = new TimberImage($imageID);
				}
			}

			$this->context['business_images'] = $images;
		}

		/**
		 * Get any 'related' events.
		 */
		$events = Timber::get_posts(array(
			'post_type' => 'event',
			'posts_per_page' => 1,
			'meta_key' => 'start_date',
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
			'meta_query' => array(
				array(
					'key' => 'related_business',
					'value' => '"' . $this->context['page']->ID . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
					'compare' => 'LIKE'
				)
			)
		), 'EventPost');

		if ( is_array($events) && !empty($events)) {
			$this->context['related_event'] = $events[0];
		}

		/**
		 * Get any 'related' job vacancies.
		 */
		$jobs = Timber::get_posts(array(
			'post_type' => 'job_vacancy',
			'posts_per_page' => 1,
			'meta_key' => 'application_date',
			'orderby' => 'meta_value_num',
			'order' => 'ASC',
			'meta_query' => array(
				[
					'key' => 'related_business',
					'value' => '"' . $this->context['page']->ID . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
					'compare' => 'LIKE'
				],
				[
					'key' => 'application_date',
					'value' => date('Ymd'),
					'compare' => '>=',
				]
				
			)
		), 'JobVacancyPost');

		if ( is_array($jobs) && !empty($jobs) ) {

			foreach($jobs as $job) {

                $job->image = new TimberImage($job->company_logo);

            }
			$this->context['related_job'] = $jobs[0];
		}

		/**
		 * Get any 'related' events.
		 */
		$news = Timber::get_posts(array(
			'post_type' => 'news_article',
			'posts_per_page' => 1,
			'meta_query' => array(
				array(
					'key' => 'related_business',
					'value' => '"' . $this->context['page']->ID . '"', // matches exaclty "123", not just 123. This prevents a match for "1234"
					'compare' => 'LIKE'
				)
			)
		), 'NewsArticlePost');

		if ( is_array($news) && !empty($news)) {
			$this->context['related_news'] = $news[0];
		}


		// /**
		//  * Get business offers
		//  */

		// $offers_and_rewards = $this->get_offers_and_rewards();

		// $this->context['offers'] = $offers_and_rewards['offers'];
		// $this->context['rewards'] = $offers_and_rewards['rewards'];

	}
}

$view = new BusinessDetailView( array('find-business-detail.twig') );
$view->render();