<?php

class GalleryDetailView extends BasingstokeBaseView {

    public function post_process(){

        /**
         * Ensure the base class post_process method gets called.
         */
        parent::post_process();

        $images = array();
        $imageData = $this->context['page']->get_field('image_list');

        if (!empty($imageData)) {

            foreach($imageData as $imageRow) {

                $images[] = array(
                    'image' => new TimberImage($imageRow['image']),
                    'description' => $imageRow['image_description'],
                    'link' => $imageRow['auction_link'],
                );

            }

        }

        $this->context['images'] = $images;

    }
}

$view = new GalleryDetailView( array('gallery-detail.twig') );
$view->render();