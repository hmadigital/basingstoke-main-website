
If you get a database error, check that you have a local wp-config.php file.
If not, create the wp-config.php file and paste in the following snippet


<?php 
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {
    include dirname( __FILE__ ) . '/wp-config-local.php';
}

## ACF Plugin Standard Installed

This project does not use ACF Pro

## Plugin update for ACF disabled

So, you may be wondering why the acf plugin is currently unable to update.

Basically, old ACF doesn't play well with google maps API; it fails to pass in an API key. so we had to update
wp-content/plugins/advanced-custom-fields/core/fields/google-map.php
line 293 to include 'key' => 'AIzaSyB4L4QybNh3Di47HgGU9hMuV9Wccd182Bc'

If you NEED to update ACF, then comment out the line in functions.php, update the plugin, then add in the key above and then please for the love of glob uncomment the line in function.php again!
