<?php

define('WP_DEBUG', true);

/**
 * Database credentials.
 */
/**
 * Database credentials.
 */
define( 'DB_NAME', 'wp_basingstoke' );
define( 'DB_USER', 'root' );
define( 'DB_PASSWORD', '' );
define( 'DB_HOST', 'localhost' );

/**
 * Custom constants used to build the local development URL.
 * Only change the HMA_DEV_PREFIX constant, based on the client and project.
 */
define( 'HMA_DEV_URL', 'http://basingstoke-main-website.hmalocal/' );
define( 'WP_SITEURL', HMA_DEV_URL);
define( 'WP_HOME', HMA_DEV_URL );



define( 'HMA_DEV_REWRITE_UPLOADS', true);
define( 'HMA_DEV_PRODUCTION_URL', 'http://basingstoketogether.co.uk/' );
define( 'HMA_DEV_LOCAL_UPLOADS', WP_SITEURL . 'wp-content/uploads' );
define( 'HMA_DEV_PRODUCTION_UPLOADS', HMA_DEV_PRODUCTION_URL . 'wp-content/uploads' );

if (HMA_DEV_REWRITE_UPLOADS === true) {
	ob_start( function( $pageContent ) {
	    return str_replace( array( HMA_DEV_LOCAL_UPLOADS ), HMA_DEV_PRODUCTION_UPLOADS, $pageContent );
	} );
}


define('AUTH_KEY',         '2*y*?v27j`9rz#w_GI#NmZ?5Ezn+-_LJw3s]||}nO-`o$QzwuGg8ui@/1[tIMD6?');
define('SECURE_AUTH_KEY',  'v7FzC8+qJ$C+D0>od`+WKZB4-Y<15TR5KK91KGI<:ggxmv?/Y{W3+u1l3db*4N%H');
define('LOGGED_IN_KEY',    '}U3t1SK-g|y3]h[-P,-:s:VP#?;0+UzxW2.lU{sf=2JgMC,^NS(MX,K,l,eC+iKn');
define('NONCE_KEY',        'n$ {Zy1X4.<]m>wUp@[|P6roz&Bd&K??<w}x]WNqgLD#txBX>+t6]w!Z>8u_|pni');
define('AUTH_SALT',        '-W7n:I9p@LQXZ1q0N+32oM>8@B7CSAxr-Rls%[`zTC8)EZ/*J%_bbNg%}?2`F<|F');
define('SECURE_AUTH_SALT', ',qK#4+Bh|rCOTO^{HioBgP#uSx)6-rbEt!T;ncFs!o1%ky)fVAQ=CM2Ea^f>2qC`');
define('LOGGED_IN_SALT',   '+:JwKl@HCAPv%bN_U4_maVL(+h[s)F#+n|bs&3MOPtcE+3kK$^V01)nwBrE?{n-i');
define('NONCE_SALT',       '>v~5<HIULApl]oWphhBpWxwWfi&?3]sIaik$m>`Dqy7T>.8nqqb.7pK il9rU$JV');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

//rewrite uploads URL to live!



/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');